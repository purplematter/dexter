# Dexter

Dexter is a Tezos on-chain decentralized exchange. It is designed for ease of use.

## V1 Features

- Trade Tezzies for on-chain tokens
- Trade on-chain tokens for on-chain tokens
- Trade on-chain tokens for Tezzies
- Liquidity pools for small fees on transactions, liquidity providers earn fees for providing liquidity
- No other fees
- Unit-tested and formally verified smart contracts in Morley
- Simple in-browser trading with Tezbox wallet
- Expose a Haskell library for building programs on top of Dexter
- Expose an OCaml and a BuckleScript/ReasonML library for building programs on top of Dexter, partially automated with a Haskell to OCaml tool
- Provide a JSON-RPC for building on top of Dexter (with languages other than Haskell, OCaml, or BuckleScript/ReasonML)

## Dependencies

- Morley
- tezos-servant-client (we are developing this)

## Details

One exchange per tezos token contract.
Factory as a public registry of tokens exchange contracts. The store value from the factory contract contains all of the exchange addresses.
Provide analytics in Dexter web page.

## Michelson Limitations

Contract interaction is difficult. There is not an easy way to get store information from another contract on-chain. To achieve this, both contracts must know each others' type signature or you have to make an intermediary contract to work with a contract whose type signature you do not have control over. Then you create a series of callbacks to send the data in the parameter.


## Smart Contracts

- Factory, create and manage exchanges for tezos tokens
- Exchange, exchange tezzies to token, token to token, token to tezzies
- ExchangeToTokenIntermediary, token for exchange to get data from 
- ExternalToExchangeIntermediarySkeleton, Morley code that can be extended to allow 3rd party contracts to interact with Exchange (because of challenges of 
- ERC20, a toy contract to help development of Factory, Exchange and ExchangeToTokenIntermediary

## References
https://docs.uniswap.io/
https://news.ycombinator.com/item?id=20042355

## V2 Features

- Support offchain trades with non-Tezos platforms
- Possibly expand to 0x style contracts
- Connect to Cosmos via a peg-zone
- Support private exchanges, useful if you need whitelist/blacklist
