# dexter-store-manager

dexter-store-manager is responsible for querying dexter contract data from tezos-cli at different periods of time (different block hashes). dexter-store-manager builds up a history of transactions, account balances and other data from the exchange contract, the token contract and the account balances. This data is available for querying via the dexter-servant-server and can be provided to the front-end.
