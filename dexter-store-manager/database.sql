create table exchange_contract (
  id                          SERIAL  PRIMARY KEY,
  name                        TEXT    NOT NULL,
  exchange_contract_id        TEXT    NOT NULL UNIQUE,
  exchange_initial_block_hash TEXT    NOT NULL,
  token_contract_id           TEXT    NOT NULL UNIQUE  
);

CREATE UNIQUE INDEX exchange_id_index
on exchange_contract (id);

CREATE UNIQUE INDEX exchange_contract_id_index
on exchange_contract (exchange_contract_id);

CREATE UNIQUE INDEX token_contract_id_index
on exchange_contract (token_contract_id);




create table exchange_transaction (
  id            SERIAL  PRIMARY KEY,
  block_hash    TEXT    NOT NULL,
  level         BIGINT  NOT NULL,

  source        TEXT    NOT NULL,
  fee           BIGINT  NOT NULL,
  counter       BIGINT  NOT NULL,
  gas_limit     BIGINT  NOT NULL,
  storage_limit BIGINT  NOT NULL,
  amount        BIGINT  NOT NULL,
  parameters    JSON,

  exchange_contract_id   INTEGER NOT NULL,
  FOREIGN KEY (exchange_contract_id) REFERENCES exchange_contract (id)
);

CREATE UNIQUE INDEX exchange_transaction_id_index
on exchange_transaction (id);

CREATE INDEX exchange_transaction_block_hash_index
on exchange_transaction (block_hash);

CREATE INDEX exchange_transaction_source_index
ON exchange_transaction (source);

CREATE INDEX exchange_transaction_exchange_contract_id_index
ON exchange_transaction (exchange_contract_id);




create table exchange_balance (
  id            SERIAL  PRIMARY KEY,
  block_hash    TEXT    NOT NULL,
  level         BIGINT NOT NULL,
  liquidity     BIGINT NOT NULL,
  mutez         BIGINT NOT NULL,
  token         BIGINT NOT NULL,

  exchange_contract_id   INTEGER NOT NULL,
  FOREIGN KEY (exchange_contract_id) REFERENCES exchange_contract (id)
);

CREATE INDEX exchange_balance_block_hash_index
on exchange_balance (block_hash);

CREATE INDEX exchange_balance_exchange_contract_id_index
ON exchange_balance (exchange_contract_id);




create table account (
  id      SERIAL  PRIMARY KEY,
  address TEXT    NOT NULL UNIQUE
);

CREATE UNIQUE INDEX account_address_index
on account (address);




create table account_balance (
  id            SERIAL  PRIMARY KEY,
  block_hash    TEXT    NOT NULL,
  level         BIGINT NOT NULL,
  liquidity     BIGINT NOT NULL,
  mutez         BIGINT NOT NULL,
  token         BIGINT NOT NULL,

  account_id    INTEGER NOT NULL,
  FOREIGN KEY (account_id) REFERENCES account (id),

  exchange_contract_id   INTEGER NOT NULL,
  FOREIGN KEY (exchange_contract_id) REFERENCES exchange_contract (id)
);

CREATE INDEX account_balance_block_hash_index
ON account_balance (block_hash);

CREATE INDEX account_balance_account_id_index
ON account_balance (account_id);

CREATE INDEX account_balance_exchange_contract_id_index
ON account_balance (exchange_contract_id);

/*
drop table exchange_balance;
drop table account_balance;
drop table account;
drop table exchange_transaction;
drop table exchange_contract;
*/
