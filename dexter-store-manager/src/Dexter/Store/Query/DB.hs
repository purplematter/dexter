{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications  #-}

module Dexter.Store.Query.DB where

import           Control.Exception (try, SomeException)
import           Control.Lens  ((^.))
import           Control.Monad (forM_, join)

import qualified Data.Generics.Product as P
import           Data.Int (Int64)

import           Database.PostgreSQL.Simple (Connection, Only(..), execute, query)

import           Dexter.Store.Types
  (Account, AccountBalance, AccountBalanceId, AccountId, ExchangeBalance,
   ExchangeBalanceId, ExchangeContract, ExchangeContractId,
   ExchangeTransaction(..), ExchangeTransactionId, IsSqlKey(..),
   SqlEntity(..), exchangeContractId, toSqlKey)

import           Safe (headMay)

import           Tezos.Client.Types
  (BlockHash, ContractId(..), OperationTransaction)

-- =============================================================================
-- ExchangeContract queries
-- =============================================================================

insertExchangeContract :: Connection -> ExchangeContract -> IO ExchangeContractId
insertExchangeContract conn exchangeContract =
  join $ catchInsertError "insertExchangeContract" exchangeContract <$>
  (try (execute conn "insert into exchange_contract (name, exchange_contract_id, exchange_initial_block_hash, token_contract_id) values (?,?,?,?)" exchangeContract))

getExchangeContract :: Connection -> ContractId -> IO (Maybe (SqlEntity ExchangeContractId ExchangeContract))
getExchangeContract conn contractId =
  join $ catchSingleQueryError "getExchangeContract" <$> (try (query conn "select id, name, exchange_contract_id, exchange_initial_block_hash, token_contract_id from exchange_contract where exchange_contract_id = ?" (Only contractId)))

getOrInsertExchangeContract :: Connection -> ExchangeContract -> IO (SqlEntity ExchangeContractId ExchangeContract)
getOrInsertExchangeContract conn exchangeContract = do
  mExchangeContract <- getExchangeContract conn (exchangeContractId exchangeContract)
  maybe
     (do    
         exchangeContractId' <- insertExchangeContract conn exchangeContract
         pure $ SqlEntity exchangeContractId' exchangeContract
     )
     pure mExchangeContract

-- =============================================================================
-- ExchangeTransaction queries
-- =============================================================================

getExchangeTransaction :: Connection -> ExchangeTransactionId -> IO (Maybe (SqlEntity ExchangeTransactionId ExchangeTransaction))
getExchangeTransaction conn psqlId =
  join $ catchSingleQueryError "getExchangeTransaction" <$>
  (try $ query conn "select id, block_hash, level, source, fee, counter, gas_limit, storage_limit, amount, parameters, exchange_contract_id from exchange_transaction where id = (?)" (Only psqlId))

getExchangeTransactionByContractId :: Connection -> ContractId -> IO (Maybe (SqlEntity ExchangeTransactionId ExchangeTransaction))
getExchangeTransactionByContractId conn blockHash =
  join $ catchSingleQueryError "getExchangeTransactionByContractId" <$>
  (try $ query conn "select id, block_hash, level, source, fee, counter, gas_limit, storage_limit, amount, parameters, exchange_contract_id from exchange_transaction where block_hash = (?)" (Only blockHash))

getLatestExchangeTransaction :: Connection -> ExchangeContractId -> IO (Maybe (SqlEntity ExchangeTransactionId ExchangeTransaction))
getLatestExchangeTransaction conn psqlId =
  join $ catchSingleQueryError "getLatestExchangeTransaction" <$>
  (try $ query conn "select id, block_hash, source, level, fee, counter, gas_limit, storage_limit, amount, parameters, exchange_contract_id from exchange_transaction where exchange_contract_id = (?) ORDER BY level DESC LIMIT 1" (Only psqlId))

insertExchangeTransaction :: Connection -> ExchangeTransaction -> IO ExchangeTransactionId
insertExchangeTransaction conn transactionRow = do
  join $ catchInsertError "insertExchangeTransaction" transactionRow <$> (try $ execute conn
    ("insert into exchange_transaction " <>
     "(block_hash, source, level, fee, counter, gas_limit, storage_limit, amount, parameters, exchange_contract_id) " <>
     "values (?,?,?,?,?,?,?,?,?,?)")
    transactionRow)

-- =============================================================================
-- ExchangeBalance queries
-- =============================================================================

catchSingleQueryError :: String -> (Either SomeException [SqlEntity a b])-> IO (Maybe (SqlEntity a b))
catchSingleQueryError functionName result =
  case result of
    Left error' -> fail $ functionName ++ ": " ++ show error'
    Right ok    -> pure . headMay $ ok

catchInsertError :: (Show a, IsSqlKey b) => String -> a -> Either SomeException Int64 -> IO b
catchInsertError functionName value result =
  case result of
    Left error' -> fail $ functionName ++ ": \n" ++ show value ++ "\n" ++ show error'
    Right ok    -> pure . keyFromSqlKey . toSqlKey $ ok

  
getExchangeBalance :: Connection -> BlockHash -> ContractId -> IO (Maybe (SqlEntity ExchangeBalanceId ExchangeBalance))
getExchangeBalance conn blockHash exchangeContractId' = do
  join $ catchSingleQueryError "getExchangeBalance" <$> (try $ query conn "SELECT id, block_hash, level, liquidity, mutez, token, exchange_contract_id FROM exchange_balance WHERE block_hash = (?) AND exchange_contract_id = (?)" (blockHash, exchangeContractId'))

insertExchangeBalance :: Connection -> ExchangeBalance -> IO ExchangeBalanceId
insertExchangeBalance conn exchangeBalanceRow = do
  join $ catchInsertError "insertExchangeBalance" exchangeBalanceRow <$> (try $ execute conn
    ("insert into exchange_balance " <>
     "(block_hash, level, liquidity, mutez, token, exchange_contract_id) " <>
     "values (?,?,?,?,?,?)")
    exchangeBalanceRow)

-- =============================================================================
-- Account queries
-- =============================================================================

getAccount :: Connection -> ContractId -> IO (Maybe (SqlEntity AccountId Account))
getAccount conn address =
  join $ catchSingleQueryError "getAccount" <$>
  (try $ query conn "select id, address from account where address = (?)" (Only address))

insertAccount :: Connection -> Account -> IO AccountId
insertAccount conn accountRow =
  join $ catchInsertError "insertAccount" accountRow <$> (try $ execute conn
    ("insert into account " <>
     "(address) " <>
     "values (?)")
    accountRow)

-- =============================================================================
-- AccountBalance queries
-- =============================================================================

getAccountBalance
  :: Connection
  -> AccountId
  -> ExchangeContractId
  -> BlockHash
  -> IO (Maybe (SqlEntity AccountBalanceId AccountBalance))
getAccountBalance conn accountId exchangeContractId' blockHash =
  join $ catchSingleQueryError "getAccountBalance" <$> (try $ query conn "SELECT id, block_hash, level, liquidity, mutez, token, account_id, exchange_contract_id FROM account_balance WHERE account_id = (?) AND exchange_contract_id = (?) AND block_hash = (?)" (accountId, exchangeContractId', blockHash))

insertAccountBalance :: Connection -> AccountBalance -> IO AccountBalanceId
insertAccountBalance conn accountBalanceRow =
  join $ catchInsertError "insertAccountBalance" accountBalanceRow <$> (try $ execute conn
    ("insert into account_balance " <>
     "(block_hash, level, liquidity, mutez, token, account_id, exchange_contract_id) " <>
     "values (?,?,?,?,?,?,?)")
    accountBalanceRow)

-- =============================================================================
-- Per pair of ContractId and BlockHash query
-- =============================================================================

-- | For a particular ContractId and BlockHash/BlockId pair
-- insert all of the transactions received from tezos and store them in the db.
insertBlockTransactions :: Connection -> ExchangeContractId -> BlockHash -> Int64 -> [OperationTransaction] -> IO ()
insertBlockTransactions conn exchangeContractKey blockHash level' operations =  
  forM_ operations $ \operation -> do
    let et =
          ExchangeTransaction
          blockHash
          (operation ^. P.field @"source")
          level'
          (operation ^. P.field @"fee")
          (operation ^. P.field @"counter")
          (operation ^. P.field @"gasLimit")
          (operation ^. P.field @"storageLimit")
          (operation ^. P.field @"amount")
          (operation ^. P.field @"parameters")
          exchangeContractKey

    insertExchangeTransaction conn et
