{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TypeApplications      #-}

module Dexter.Store.Query.Tezos where

import           Control.Monad (void)
import           Control.Monad.STM (atomically)
import           Control.Concurrent.STM.TVar (TVar, readTVarIO, writeTVar)
import           Control.Lens  ((^.))
import           Control.Monad (join, forM_)

import           Data.Either    (rights)
import qualified Data.Generics.Product as P (field)
import           Data.Int       (Int64)
import           Data.Map       (Map)
import qualified Data.Map as Map
import           Data.Maybe     (catMaybes)
import           Data.Proxy     (Proxy(Proxy))
import           Data.Set       (Set)
import qualified Data.Set as Set

import           Database.PostgreSQL.Simple (Connection)

import           Dexter.Store.Query.DB
  (getAccount, getLatestExchangeTransaction, insertAccount,
   insertAccountBalance, insertBlockTransactions)

import           Dexter.Store.Types
  (Account(..), AccountBalance(..), AccountId,
   ExchangeContract, ExchangeContractId, SqlEntity(..), accountAddress,
   exchangeContractId, exchangeInitialBlockHash, entityKey, entityVal,
   tokenContractId, trBlockHash)

import           Safe (headMay)

import           Servant.Client
  (ClientEnv, ClientM, ServantError, client, runClientM)

import           Tezos.Client.Protocol.Alpha (GetBlock, GetBlockHeader, GetBlockOperations)
import           Tezos.Client.Protocol.Alpha.Context
  (GetContractStorage, PostContractBigMap, GetBlockContextContractBalance)

import           Tezos.Client.Shell (GetBlockchainHeads)

import           Tezos.Client.Types
  (Bignum(..), Block, BlockHash, BlockId(..), BlockHeader, BigMap(..),
   ChainId(..), ContractId(..), Mutez, Operation(..), OperationTransaction(..),
   OperationContents(..), OperationContentsAndResult(..), Timestamp,
   Expression(..), PositiveBignum(..), PrimitiveType(..), Primitives(..))

-- =============================================================================
-- dexter storage functions
-- =============================================================================

getHeadBlockHash :: ClientEnv -> IO (Maybe BlockHash)
getHeadBlockHash clientEnv = do
  blockHashes <- runClientM (getBlockchainHeads ChainIdMain Nothing Nothing Nothing) clientEnv
  -- get the first element of the first list
  pure $ either (const Nothing) (join . fmap headMay . headMay) $ blockHashes

getHeadBlockHeader :: ClientEnv -> IO (Either ServantError BlockHeader)
getHeadBlockHeader clientEnv = runClientM (getBlockHeader ChainIdMain BlockIdHead) clientEnv

-- support a limit

getBlockHashesInRange :: ClientEnv -> Int -> Maybe BlockHash -> BlockHash -> IO (Either ServantError (Set BlockHash))
getBlockHashesInRange clientEnv' searchSize' start' end' =
  getBlockHashesInRange' clientEnv' searchSize' start' end' []
  where
    getBlockHashesInRange' :: ClientEnv -> Int -> Maybe BlockHash -> BlockHash -> [BlockHash] -> IO (Either ServantError (Set BlockHash))
    getBlockHashesInRange' clientEnv searchSize start end previouslyFoundBlockHashes = do
      eBlockHashes <- runClientM (getBlockchainHeads ChainIdMain (Just searchSize) start Nothing) clientEnv
      case eBlockHashes of
        Left error' -> pure $ Left error'
        Right blockHashes -> do
          let (found, filteredBlockHashes) =
                foldl
                (\(found', blockHashes') blockHash ->
                   if found'
                   then (found', blockHashes')
                   else
                     let isFound = blockHash == end in
                       (isFound, blockHashes' ++ [blockHash])
                ) (False, []) (join blockHashes)
          let newBlockHashes = previouslyFoundBlockHashes ++ filteredBlockHashes
          if found
            then pure $ Right $ Set.fromList newBlockHashes
            else getBlockHashesInRange' clientEnv searchSize (Just $ Prelude.last filteredBlockHashes) end newBlockHashes


getBlockTransactions :: ClientEnv -> BlockId -> IO (Either ServantError (Int64, Timestamp, [OperationTransaction]))
getBlockTransactions clientEnv blockId = do
  eBlock <- runClientM (getBlock ChainIdMain blockId) clientEnv
  case eBlock of
    Left error' -> pure $ Left error'
    Right block' ->
      let level' = (((block' ^. P.field @"metadata") ^. P.field @"level")  ^. P.field @"level") in
      let tstamp = ((block' ^. P.field @"header") ^. P.field @"timestamp") in
      pure $ Right (level', tstamp, (join $ getTransactions <$> join (block' ^. P.field @"operations")))
  where
    getTransactions (OperationAndResult _ _ _ _ contents' _) =
      let getTransaction (OperationContentsAndResultTransaction transaction _) = Just transaction
          getTransaction _ = Nothing
      in catMaybes $ getTransaction <$> contents'

    getTransactions (Operation _ _ _ _ contents' _) =
      let getTransaction (OperationContentsTransaction transaction) = Just transaction
          getTransaction _ = Nothing
      in catMaybes $ getTransaction <$> contents'
    
getContractTransactionsByBlock :: ClientEnv -> ContractId -> BlockId -> IO (Either ServantError (Int64, [OperationTransaction]))
getContractTransactionsByBlock clientEnv contractId blockId = do
  eResult <- getBlockTransactions clientEnv blockId
  case eResult of
    Left error' -> pure $ Left error'
    Right (level',_timestamp, operations') -> pure $ Right (level', filter matchContractId operations')
  where
    matchContractId transaction = contractId == transaction ^. P.field @"destination"
  
getContractTransactions :: ClientEnv -> ContractId -> BlockHash -> IO (Either ServantError [(Int64, [OperationTransaction])])
getContractTransactions clientEnv contractId originationBlockHash = do
  eBlockHashes <- getBlockHashesInRange clientEnv 100 Nothing originationBlockHash
  case eBlockHashes of
    Left error' -> pure . Left $ error'
    Right blockHashes -> do
      let blockIds = BlockId <$> Set.toList blockHashes
      eTransaction <- sequence $ getContractTransactionsByBlock clientEnv contractId <$> blockIds
      pure $ Right $ rights eTransaction

getAccountIO
  :: Connection
  -> ContractId
  -> Map ContractId (SqlEntity AccountId Account)
  -> IO (SqlEntity AccountId Account, Map ContractId (SqlEntity AccountId Account))
getAccountIO conn address accountMap =
  case Map.lookup address accountMap of
    Just account -> pure (account, accountMap)
    Nothing -> do
      mAccount <- getAccount conn address
      case mAccount of
        Just account -> pure (account, Map.insert address account accountMap)
        Nothing -> do
          accountId <- insertAccount conn (Account address)
          let account = SqlEntity accountId (Account address)
          pure (account, Map.insert address account accountMap)

-- | get the latest transactions not in the database from the Tezos network
updateExchangeContractTransactions
  :: Connection
  -> ClientEnv
  -> TVar (Map ContractId (SqlEntity AccountId Account))
  -> SqlEntity ExchangeContractId ExchangeContract
  -> IO ()
updateExchangeContractTransactions conn clientEnv accountMapTVar exchangeContract = do
  mExchangeTransaction <- getLatestExchangeTransaction conn (entityKey exchangeContract)
  
  let latestBlockHash = case mExchangeTransaction of
        Just exchangeTransaction -> trBlockHash . entityVal $ exchangeTransaction
        Nothing                  -> exchangeInitialBlockHash . entityVal $ exchangeContract

  eblockHashesSet <- getBlockHashesInRange clientEnv 100 Nothing latestBlockHash
  case eblockHashesSet of
    Left servantError -> fail $ "getBlockHashesInRange failed for block hash: '" ++ show latestBlockHash ++ "'. " ++ show servantError
    Right blockHashesSet -> do 
      let blockHashes = Set.toList blockHashesSet

      forM_ blockHashes $ \blockHash -> do
        let blockId = BlockId blockHash

        eTransactionResults <- getBlockTransactions clientEnv blockId

        case eTransactionResults of
          Left servantError -> fail $ "getBlockTransactions failed for block id: '" ++ show blockId ++ "'. " ++ show servantError
          Right transactionResults -> do
            let (level', _, operations') = transactionResults

            let exchangeContractOperations =
                  filter (\operation ->
                            (exchangeContractId . entityVal $ exchangeContract) == operation ^. P.field @"destination") operations'

            -- get the set of account ids that initiated the transactions
            let sources = Set.fromList $
                  (^. P.field @"source")
                  <$> exchangeContractOperations

            forM_ sources $ \source' -> do
              accountMap <- readTVarIO accountMapTVar
              (account, accountMap') <- getAccountIO conn source' accountMap
              atomically $ writeTVar accountMapTVar accountMap'
              
              liquidity <-
                getAccountLiquidityBalance
                clientEnv
                blockId
                (exchangeContractId . entityVal $ exchangeContract)
                (accountAddress . entityVal $ account)
              -- let liquidity = PositiveBignum 10000

              eMutez <-
                getContractBalance'
                clientEnv
                blockId
                (accountAddress . entityVal $ account)

              case eMutez of
                Left error' -> fail $ show error'
                Right mutez -> do
                  tokenBalance <-
                    getTokenBalance
                    clientEnv
                    blockId
                    (tokenContractId . entityVal $ exchangeContract)
                    (accountAddress . entityVal $ account)
              
                  let accountBalance =
                        AccountBalance
                          blockHash
                          level'
                          liquidity
                          mutez
                          tokenBalance
                          (entityKey account)
                          (entityKey exchangeContract)

                  void $ insertAccountBalance conn accountBalance

            insertBlockTransactions conn (entityKey exchangeContract) blockHash level' exchangeContractOperations

-- =============================================================================
-- account and dexter exchange queries
-- =============================================================================

-- | Get the total amount of mutez held by an dexter exchange contract or an
-- account.
getExchangeBalance :: ClientEnv -> BlockId -> ContractId -> IO Mutez
getExchangeBalance clientEnv blockId contractId = do  
  eBalance <- runClientM (getContractBalance ChainIdMain blockId contractId) clientEnv
  case eBalance of
    Left servantError -> fail $ "getExchangeBalance: " ++ show servantError
    Right balance -> pure balance

-- { "key": { "int": "1" }, "type": { "prim": "nat" } }
-- | Get the total amount of liquidity in a dexter exchange contract.
getTokenBalance :: ClientEnv -> BlockId -> ContractId -> ContractId -> IO PositiveBignum
getTokenBalance clientEnv blockId contractId address = do
  let bigMap =
        BigMap
          (StringExpression $ unContractId address)
          (ContractExpression (PrimitiveType PTAddress) Nothing Nothing)
  eBigMap <- runClientM (postContractBigMap ChainIdMain blockId contractId bigMap) clientEnv
  case eBigMap of
    Left servantError -> fail $ "getTokenBalance: " ++ show servantError
    Right (Just (ContractExpression
            _
            (Just [IntExpression (Bignum tokens), _])
            _)) -> pure $ PositiveBignum tokens
    _ -> fail $ "getTokenBalance: " ++ show eBigMap

-- =============================================================================
-- dexter exchange only queries
-- =============================================================================

-- | Get the total amount of liquidity in a dexter exchange contract.
getExchangeLiquidity :: ClientEnv -> BlockId -> ContractId -> IO Integer
getExchangeLiquidity clientEnv blockId contractId = do
  eStorage <- runClientM (getContractStorage ChainIdMain blockId contractId) clientEnv
  case eStorage of
    Left servantError -> fail $ "getExchangeLiquidity: " ++ show servantError
    Right (ContractExpression _
           (Just
            [_ ,
             (ContractExpression
              _
              (Just
               [_,
                (ContractExpression _ (Just [IntExpression (Bignum liquidity), _]) _)
               ]
              )
              _
             )
            ]
           )
           _) -> pure liquidity  
    _ -> fail $ "getExchangeLiquidity:" ++ show eStorage

-- =============================================================================
-- account only queries
-- =============================================================================

-- | Get the token balance of an account at a particular block hash.
-- Can be used by accounts and contracts.
getAccountLiquidityBalance :: ClientEnv -> BlockId -> ContractId -> ContractId -> IO PositiveBignum -- (Either ServantError (Maybe Expression))
getAccountLiquidityBalance clientEnv blockId contractId address = do
  let bigMap =
        BigMap
          (StringExpression $ unContractId address)
          (ContractExpression (PrimitiveType PTAddress) Nothing Nothing)
  eBigMap <- runClientM (postContractBigMap ChainIdMain blockId contractId bigMap) clientEnv
  case eBigMap of
    Left servantError -> fail $ "getAccountLiquidityBalance: " ++ show servantError
    Right (Just (IntExpression (Bignum bignum))) -> pure $ PositiveBignum bignum
    _ -> fail ("getAccountLiquidityBalance: " ++ show eBigMap)

getContractBalance' :: ClientEnv -> BlockId -> ContractId -> IO (Either ServantError Mutez)
getContractBalance' clientEnv blockId contractId =
  runClientM (getContractBalance ChainIdMain blockId contractId) clientEnv

-- =============================================================================
-- servant functions
-- =============================================================================

getBlockchainHeads
  :: ChainId
  -> Maybe Int
  -> Maybe BlockHash
  -> Maybe Timestamp
  -> ClientM [[BlockHash]]
getBlockchainHeads = client (Proxy :: Proxy GetBlockchainHeads)

getBlockHeader :: ChainId -> BlockId -> ClientM BlockHeader
getBlockHeader c = client (Proxy :: Proxy GetBlockHeader) c

getBlock :: ChainId -> BlockId -> ClientM Block
getBlock c = client (Proxy :: Proxy GetBlock) c

getBlockOperations :: ChainId -> BlockId -> ClientM [[Operation]]
getBlockOperations c = client (Proxy :: Proxy GetBlockOperations) c

postContractBigMap :: ChainId -> BlockId -> ContractId -> BigMap -> ClientM (Maybe Expression)
postContractBigMap c b cid = client (Proxy :: Proxy PostContractBigMap) c b cid

getContractStorage :: ChainId -> BlockId -> ContractId -> ClientM Expression
getContractStorage c b = client (Proxy :: Proxy GetContractStorage) c b

getContractBalance :: ChainId -> BlockId -> ContractId -> ClientM Mutez
getContractBalance c b = client (Proxy :: Proxy GetBlockContextContractBalance) c b
