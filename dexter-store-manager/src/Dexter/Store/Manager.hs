{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TypeApplications      #-}

module Dexter.Store.Manager where

import           Control.Concurrent.STM.TVar (newTVarIO)
import           Control.Exception (finally)
import           Control.Monad     (mapM, mapM_)

import qualified Data.Map as Map
import qualified Data.Yaml as Yaml

import           Database.PostgreSQL.Simple (ConnectInfo(..), Connection, close, connect)

import           Dexter.Store.Config (DexterStoreManagerConfig, PostgreSQLConfig(..), contracts, database, tezosCli, tezosHost, tezosPort, tezosProtocol)
import           Dexter.Store.Query.DB (getOrInsertExchangeContract)
import           Dexter.Store.Query.Tezos (updateExchangeContractTransactions)

import           Network.HTTP.Client (newManager, defaultManagerSettings)
import           Servant.Client      (BaseUrl(BaseUrl), mkClientEnv)


connectToPostgreSQL :: PostgreSQLConfig -> IO Connection
connectToPostgreSQL (PostgreSQLConfig psqlHost' psqlPort' psqlUser' psqlDBName'
                     psqlPassword') =
  connect $ ConnectInfo psqlHost' psqlPort' psqlUser' psqlDBName' psqlPassword'

-- =============================================================================
-- main function
-- =============================================================================

runDexterStoreManager :: IO ()
runDexterStoreManager = do
  eConfig <- Yaml.decodeFileEither "config.yml" :: IO (Either Yaml.ParseException DexterStoreManagerConfig)
  case eConfig of
    Left  error' -> fail $ show error'
    Right dexterStoreManagerConfig -> do
      putStrLn $ show dexterStoreManagerConfig
      if length (contracts dexterStoreManagerConfig) < 1
        then putStrLn "Expected the number of contracts in 'config.yml' to be greater than zero."
        else do
        conn <- connectToPostgreSQL (database dexterStoreManagerConfig)

        let tezosCli'  = tezosCli dexterStoreManagerConfig
        manager'       <- newManager defaultManagerSettings
        let clientEnv  = mkClientEnv manager' (BaseUrl (tezosProtocol tezosCli') (tezosHost tezosCli') (tezosPort tezosCli') "")

        exchangeContracts <- mapM (getOrInsertExchangeContract conn) (contracts dexterStoreManagerConfig)

        -- keep list of accounts, if not there, get from db, if not there, insert
        accountMapTVar <- newTVarIO Map.empty

        -- run every minute, the average time to originate a new block on tezos
        finally
          (mapM_ (updateExchangeContractTransactions conn clientEnv accountMapTVar) exchangeContracts)
          (close conn)

{- ghci

import qualified Data.Yaml as Yaml
import Tezos.Client.Types (ContractId(..), Unistring(..))
import Network.HTTP.Client (newManager, defaultManagerSettings)
import Servant.Client      (BaseUrl(BaseUrl), mkClientEnv)
import Tezos.Client.Types (BlockId(..), ContractId(..), Unistring(..), Expression(..), PrimitiveInstruction(..), BigMap(..), Primitives(..))

(Right config) <- Yaml.decodeFileEither "config.yml" :: IO (Either Yaml.ParseException DexterStoreManagerConfig)
conn <- connectToPostgreSQL (database config)

let tezosCli'  = tezosCli config
manager'       <- newManager defaultManagerSettings
let clientEnv  = mkClientEnv manager' (BaseUrl (tezosProtocol tezosCli') (tezosHost tezosCli') (tezosPort tezosCli') "")

-- getExchangeContract :: Connection -> ContractId -> IO (Maybe (SqlEntity ExchangeContractId ExchangeContract))
Just exchangeContract <- getExchangeContract conn (ContractId $ UnistringText "KT1G2D45tpJ9f1iGVwQHqvupv2syhvMqeWPe")

getExchangeTransaction conn (entityKey exchangeContract)

getLatestExchangeTransaction conn (entityKey exchangeContract)

-- token contract    KT1WQAW1sRaykMPYEPpqiL4nrYvdnb8SWTV7
-- exchange contract KT1G2D45tpJ9f1iGVwQHqvupv2syhvMqeWPe
getAccountLiquidityBalance clientEnv BlockIdHead (ContractId $ UnistringText "KT1G2D45tpJ9f1iGVwQHqvupv2syhvMqeWPe") (ContractId $ UnistringText "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH")


{ "key": { "int": "1" }, "type": { "prim": "nat" } }
http://localhost:9329/chains/main/blocks/head/context/contracts/KT1G2D45tpJ9f1iGVwQHqvupv2syhvMqeWPe/big_map_get

curl \
-H "Content-Type: application/json" \
-d '{"key":{"string": "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH"}, "type":{"prim": "address"}}' \
-X POST http://localhost:9323/chains/main/blocks/head/context/contracts/KT1G2D45tpJ9f1iGVwQHqvupv2syhvMqeWPe/big_map_get

let address = ContractId $ UnistringText "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH"

:{
let bigMap =
        BigMap
          (StringExpression $ unContractId address)
          (ContractExpression (PrimitiveInstruction PIAddress) Nothing Nothing)
:}
-}
