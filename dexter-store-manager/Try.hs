import Network.HTTP.Client (newManager, defaultManagerSettings)
import Servant.Client (BaseUrl(BaseUrl), Scheme(Http), ClientM, client, mkClientEnv, runClientM)
import Servant.API (NoContent)
import Data.Proxy (Proxy(Proxy))
import Tezos.Client.Protocol.Alpha
import Tezos.Client.Shell
import Tezos.Client.Types
import qualified Data.Text as T
import Dexter.Store.Manager

:{
manager' <- newManager defaultManagerSettings
let clientEnv = (mkClientEnv manager' (BaseUrl Http "127.0.0.1" 9323 ""))

}:

{-
(Just h) <- getHeadBlockHash clientEnv


runClientM (getBlockchainHeads (ChainIdMain) (Just 100) (Just $ BlockHash $ UnistringText "BLHBYHZuBJUbbqKY2Qr2WuYji546SHFnfXYonG5muC2kEHeVpBF") Nothing) clientEnv


getClientIdsInRange clientEnv 25 (Just $ BlockHash $ UnistringText "BLHBYHZuBJUbbqKY2Qr2WuYji546SHFnfXYonG5muC2kEHeVpBF") (BlockHash $ UnistringText "BMEUauJot4efcLmbicepByzdSiprbRmEP4FhQSmu2wJ3X3KVx8D")
-}
-- 100 blocks between these two
-- "BLHBYHZuBJUbbqKY2Qr2WuYji546SHFnfXYonG5muC2kEHeVpBF" 579900
-- "BMEUauJot4efcLmbicepByzdSiprbRmEP4FhQSmu2wJ3X3KVx8D" 579801
