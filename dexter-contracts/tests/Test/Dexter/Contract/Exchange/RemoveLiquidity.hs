{-|
Module      : Test.Dexter.Contract.Exchange.RemoveLiquidity
Copyright   : (c) camlCase, 2019
Maintainer  : james@camlcase.io

Unit and property tests for the remove liquidity entry point in the Dexter exchange contract.

-}

{-# LANGUAGE DerivingStrategies     #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE OverloadedLabels       #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE ViewPatterns           #-}

module Test.Dexter.Contract.Exchange.RemoveLiquidity
  ( testRemoveLiquidity
  ) where

import Dexter.Contract.Exchange (Parameter(..), Storage(..), MutableStorage, exchangeContract)
import Dexter.Contract.Types (Balance, GetBalanceBrokerContractParameter, RemoveLiquidityParams, SimpleTokenContractParameter)

import qualified Data.Map as Map

import Lorentz (compileLorentz)

import Michelson.Interpret (ContractEnv(..))
import Michelson.Test (ContractPropValidator, contractProp, concatTestTrees,
                       failedProp, midTimestamp, dummyContractEnv)
import Michelson.Typed (BigMap, ContractAddr(..), ToT)

import qualified Michelson.Typed as T

import Named (arg)

import Prelude (Bool(..), Either(..), IO, Natural, (.), ($), (*), (<*>), (<$>), (&&),
                (++), (<=), (==), flip, fromIntegral, fst, isLeft, isRight,
                one, pure, show, snd)

import Test.Dexter.Contract.Gen (genAddress, genMidAmount, genSmallAmount, staticImmutableStorage)
import Test.Dexter.Contract.Mock (alice)
import Test.Hspec.Expectations (shouldSatisfy)
import Test.QuickCheck (Gen, Property, choose, counterexample, forAll, withMaxSuccess, vectorOf, (.&&.))
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)
import Test.Tasty.QuickCheck (testProperty)

import Tezos.Address (Address(..))
import Tezos.Core (unsafeMkMutez, timestampPlusSeconds)

import Util.Named ((.!))

testRemoveLiquidity :: IO [TestTree]
testRemoveLiquidity = concatTestTrees
  [ one . testGroup "Dexter.Contract.Exchange removeLiquidity" <$>
    exchangeTest (compileLorentz exchangeContract)
  ]
  where
    exchangeTest contract =
      pure
      [ testCase "Calling removeLiquidity entry path completes" $
          contractProp
            contract        -- Contract
            (flip shouldSatisfy (isRight . fst)) -- ContractPropValidator
            contractEnv     -- ContractEnv
            removeLiquidity -- Parameter
            storage         -- Storage
      , testProperty "Check removeLiquidity properties" $ withMaxSuccess 1000 $
          qcProp contract
      ]

    qcProp contract =
      forAll ((,) <$> genRemoveLiquidityParams <*> genStorage) $
        \(arbParam, arbStorage) ->
          let validate = validateRemoveLiquidity contractEnv arbParam arbStorage
           in contractProp contract validate contractEnv arbParam arbStorage


-- =============================================================================
-- Environment
-- =============================================================================

contractEnv :: ContractEnv
contractEnv =
  dummyContractEnv
    { ceNow     = midTimestamp
    , ceAmount  = unsafeMkMutez 0
    , ceBalance = unsafeMkMutez (1000000 * 100)
    , ceSource  = alice
    , ceSender  = alice
    }

-- =============================================================================
-- Parameter
-- =============================================================================

removeLiquidity :: Parameter
removeLiquidity = RemoveLiquidity
  ( #burnAmount .! 1
  , #minMutez   .! unsafeMkMutez 1000000
  , #minTokens  .! 1
  , #deadline   .! (midTimestamp `timestampPlusSeconds` 1)
  )

-- | Generate RemoveLiquidityParams with a small amount of ...
-- The deadline is set to one second later then
-- the time in the environment.
genRemoveLiquidityParams :: Gen Parameter
genRemoveLiquidityParams = do
  ba <- genSmallAmount
  mm <- genSmallAmount
  mt <- genSmallAmount
  e <- choose (-5,5)
  pure . RemoveLiquidity $
    (#burnAmount .! ba,
     #minMutez   .! (unsafeMkMutez $ fromIntegral mm),
     #minTokens  .! mt,
     #deadline   .! (midTimestamp `timestampPlusSeconds` e)
    )

-- =============================================================================
-- Storage
-- =============================================================================

storage :: Storage
storage =
  Storage
    (T.BigMap $ Map.fromList [(alice, 100)])
    staticImmutableStorage
    ((#totalSupply .! 100), (#continuationStorage .! Map.empty))

genBigMap :: Gen (BigMap Address Balance)
genBigMap = do
  k <- choose (0,2)
  T.BigMap . Map.fromList <$> vectorOf k genPair
  where
    genPair = (,) <$> genAddress <*> genMidAmount
  
genMutableStorage :: Gen Natural -> Gen MutableStorage
genMutableStorage genNatural = (,) <$> ((#totalSupply .!) <$> genNatural) <*> (pure $ #continuationStorage .! Map.empty)

-- | There  is a relation between the accounts (the big map), the totalSupply
-- and the tez amount held by the contract. If one is empty or zero, the rest
-- should be empty or zero. Otherwise they should be non-empty and non-zero.
genStorage :: Gen Storage
genStorage = Storage <$> genBigMap <*> pure staticImmutableStorage <*> genMutableStorage genMidAmount

-- =============================================================================
-- Validation Test
-- =============================================================================

validateRemoveLiquidity
  :: ContractEnv
  -> Parameter
  -> Storage
  -> ContractPropValidator (ToT Storage) Property
validateRemoveLiquidity env param storage' result =
  case param of
    RemoveLiquidity p -> validateRemoveLiquidity' env p storage' result
    _ -> failedProp "Expected RemoveLiquidity"

validateRemoveLiquidity'
  :: ContractEnv
  -> RemoveLiquidityParams
  -> Storage
  -> ContractPropValidator (ToT Storage) Property
validateRemoveLiquidity' env param (Storage _oldBigMap immutableStorage' _mutableStorage) (resE, _interpreterState) =
  -- expected failures
  prop (burnAmount <= 0                ) (counterexample "Expected failure: burnAmount is not greater than zero" $ isLeft resE) $
  prop (deadline   <= ceNow env        ) (counterexample "Expected failure: deadline is less than current time" $ isLeft resE) $
  prop (minMutez   <= (unsafeMkMutez 0)) (counterexample "Expected failure: minMutez is not greater than zero" $ isLeft resE) $
  prop (minTokens  <= 0                ) (counterexample "Expected failure: minTokens is not greater than zero" $ isLeft resE) $

  -- expected successes
  (counterexample ("Should only transfer token to the broker contract from immutable storage:\n " ++ showState) assertTransferContract) .&&.
  (counterexample ("The transfer token parameter should include the sender and the token address:\n " ++ showState) assertTransferParam)  

  where
    showState = "env: " ++ show env ++ "\nparam:" ++ show param ++ "\nresE:" ++ show resE

    prop condition trueFunction falseFunction =
      case condition of
        True -> trueFunction
        False -> falseFunction

    (arg #burnAmount -> burnAmount, arg #minMutez -> minMutez,
     arg #minTokens  -> minTokens,  arg #deadline -> deadline) = param

    brokerContract = arg #brokerAddress . fst $ immutableStorage' :: ContractAddr GetBalanceBrokerContractParameter
    tokenContract  = arg #tokenAddress  . snd $ immutableStorage' :: ContractAddr SimpleTokenContractParameter

    assertTransferContract =
      case resE of
        Right ([T.OpTransferTokens (T.TransferTokens _ _ (T.VContract contract))], _) ->
          contract == unContractAddress brokerContract
        _ -> False

    assertTransferParam =
      case resE of
        Right ([T.OpTransferTokens (T.TransferTokens (T.VOr (Left (T.VPair (T.VC (T.CvAddress tokenAccount), T.VContract tokenAddress)))) _ _)], _) ->
          tokenAccount == ceSender env &&
          tokenAddress == unContractAddress tokenContract
        _ -> False
