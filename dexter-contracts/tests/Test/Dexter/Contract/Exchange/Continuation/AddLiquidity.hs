{-|
Module      : Test.Dexter.Contract.Exchange.Continuation.AddLiquidity
Copyright   : (c) camlCase, 2019
Maintainer  : james@camlcase.io

Add liquidity continuation. Keep in mind that this is only called when
totalSupply is greater than zero, this also means that the balance and the
number of tokens held by the exchange contract are non-zero.

-}

{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE OverloadedLabels       #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE ViewPatterns           #-}

module Test.Dexter.Contract.Exchange.Continuation.AddLiquidity
  ( testAddLiquidityContinuation
  ) where

import qualified Data.Map.Strict as Map

import qualified Data.Text as T

import Dexter.Contract.Exchange (ContinuationStorage, MutableStorage, Parameter(..), Storage(..), exchangeContract)
import Dexter.Contract.Types (AddLiquidityParams, Balance)

import Lorentz (compileLorentz)

import Michelson.Interpret (ContractEnv(..))
import Michelson.Test (ContractPropValidator, contractProp, concatTestTrees, midTimestamp, dummyContractEnv)
import Michelson.Test.Util (failedProp, succeededProp)
import Michelson.Typed (ToT)
import qualified Michelson.Typed as T
import Michelson.Typed.Haskell.Value (BigMap(..))

import Named (arg)

import Test.Dexter.Contract.Gen (staticImmutableStorage)
import Test.Dexter.Contract.Mock (alice, broker, emptyExchangeStorage, setContinuationStorage)
import Test.Hspec.Expectations (shouldSatisfy)
import Test.QuickCheck (Gen, Property, choose, counterexample, forAll, withMaxSuccess)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)
import Test.Tasty.QuickCheck (testProperty)

import Tezos.Address (Address(..))
import Tezos.Core (unsafeMkMutez, timestampPlusSeconds, unMutez)

import Util.Named ((.!))

-- | Generate a small Natural number.
genNatural :: Gen Natural
genNatural = fromInteger <$> choose (10, 100)

-- Environment

-- | Generate a ContractEnv where balance is always greater than or equal to
-- amount. Use the alice contrac as source and the sender is the broker contract.
genEnvAmountAndBalance :: Gen ContractEnv
genEnvAmountAndBalance = do
  amountInt <- choose (10,100)
  let amount = fromInteger . (* 1000000) $ amountInt
  balance <- fromInteger . (* 1000000) <$> choose (amountInt,200)
  pure $
    dummyContractEnv
      { ceNow     = midTimestamp
      , ceAmount  = unsafeMkMutez amount
      , ceBalance = unsafeMkMutez balance
      , ceSource  = alice
      , ceSender  = broker
      }

-- Storage Generation

-- | addLiquidityParamsPass with parameters that should pass.
addLiquidityParamsPass :: AddLiquidityParams
addLiquidityParamsPass =
  ( #minLiquidity .! 1
  , #maxTokens    .! 100000
  , #deadline     .! (midTimestamp `timestampPlusSeconds` 1)
  )

-- | Generate MutableStorage with an empty continuationStorage map.
genMutableStorage :: Gen Natural -> Gen MutableStorage
genMutableStorage genNatural' = (,) <$> ((#totalSupply .!) <$> genNatural') <*> (pure $ #continuationStorage .! Map.empty)

-- | There  is a relation between the accounts (the big map), the totalSupply
-- and the tez amount held by the contract. If one is empty or zero, the rest
-- should be empty or zero. Otherwise they should be non-empty and non-zero.
genStorage :: Gen Storage
genStorage =
  flip setContinuationStorage (Map.fromList [(alice, Left . Left $ addLiquidityParamsPass)])
  <$> Storage (BigMap Map.empty) staticImmutableStorage <$> genMutableStorage genNatural

-- | Test the add liquidity continuation portion of the dexter exchange
-- contract.
testAddLiquidityContinuation :: IO [TestTree]
testAddLiquidityContinuation =
  concatTestTrees
    [ one . testGroup "Dexter.Contract.Exchange addLiquidityContinuation" <$>
      exchangeTest (compileLorentz exchangeContract)
    ]
  where
    exchangeTest contract =
      pure
      [ testCase "Should fail when the sender is not the broker contract" $
          contractProp
            contract     -- Contract
            (flip shouldSatisfy (isLeft . fst)) -- ContractPropValidator
            nonBrokerContractEnv  -- ContractEnv
            paramContinuation -- Parameter
            exchangeStorage -- Storage

      , testCase "Should pass when sender is the broker contract, balance is greater than amount and they are non-zero" $
          contractProp
            contract
            (flip shouldSatisfy (isRight . fst))
            passEnv
            paramContinuation
            passExchangeStorage

      , testProperty "Check addLiquidity continuation properties" $ withMaxSuccess 1000 $
          qcProp contract
      ]

    paramContinuation :: Parameter
    paramContinuation = Continuation 100
  
    nonBrokerContractEnv = dummyContractEnv
            { ceNow     = midTimestamp
            , ceAmount  = unsafeMkMutez 1000000
            , ceBalance = unsafeMkMutez 2000000
            , ceSource  = alice
            }

    passEnv = dummyContractEnv
            { ceNow     = midTimestamp
            , ceAmount  = unsafeMkMutez 1000000
            , ceBalance = unsafeMkMutez 2000000
            , ceSource  = alice
            , ceSender  = broker
            }

    addLiquidityParams =
      ( #minLiquidity .! 1
      , #maxTokens    .! 100
      , #deadline     .! (midTimestamp `timestampPlusSeconds` 1)
      )

    continuationStorage = Map.fromList [(alice, Left . Left $ addLiquidityParams)]
    exchangeStorage = setContinuationStorage emptyExchangeStorage continuationStorage

    passExchangeStorage =
      setContinuationStorage
      emptyExchangeStorage
      (Map.fromList [(alice, Left . Left $ addLiquidityParamsPass)])

    qcProp contract =
      forAll ((,,) <$> genEnvAmountAndBalance <*> (Continuation <$> genNatural) <*> genStorage) $
        \(env, param, storage) ->
          let validate = validateAddLiquidityContinuation env param storage
           in contractProp contract validate env param storage

-- | Errors that can occur during the calculation of add liquidity continuation.
data AddLiquidityError
  = TokenReserveLessThanOne
  | TezosReserveLessThanOne
  | TokenAmountGreaterThanMaxTokens
  | LiquidityMintedLessThanMinLiquidity
  deriving (Eq, Read, Show)

-- | The succesful result of the add liquidity continuation.
data AddLiquidityResult
  = AddLiquidityResult
    { alrAccountBalance         :: Natural
    , alrTotalSupply            :: Natural
    } deriving (Eq, Read, Show)

-- | Emulate the Lorentz addLiquidityContinuation function in pure Haskell.
addLiquidityContinuation
  :: Natural
  -> Natural
  -> Natural
  -> Natural
  -> Natural
  -> Natural
  -> Either AddLiquidityError AddLiquidityResult
addLiquidityContinuation balance amount totalSupply tokenReserveCheck minLiquidity maxTokens = do
  tokenReserve <- if tokenReserveCheck > 0 then pure tokenReserveCheck else Left TokenReserveLessThanOne
  let tezosReserveResult = balance - amount
  tezosReserve <- if tezosReserveResult > 0 then pure tezosReserveResult else Left TezosReserveLessThanOne

  let liquidityMinted = amount * totalSupply `div` tokenReserve
  let tokenAmount     = amount * tokenReserve `div` tezosReserve + 1
  _ <- if maxTokens >= tokenAmount then Left TokenAmountGreaterThanMaxTokens else pure ()
  _ <- if liquidityMinted >= minLiquidity then Left LiquidityMintedLessThanMinLiquidity else pure ()

  pure $ AddLiquidityResult liquidityMinted (totalSupply + liquidityMinted) 

getAddLiquidityCon :: Address -> Storage -> Maybe AddLiquidityParams
getAddLiquidityCon address (Storage _oldBigMap _immutableStorage mutableStorage') =
  let continuationStorage = arg #continuationStorage . snd $ mutableStorage' :: Map.Map Address ContinuationStorage
  in
  case Map.lookup address continuationStorage of
    Nothing -> Nothing
    Just (Left (Left addLiquidityParams')) -> Just addLiquidityParams'
    _ -> Nothing

-- |
assertResult :: Address -> T.Value (ToT Storage) -> AddLiquidityResult -> Bool
assertResult address storage addLiquidityResult =
  case storage of
    T.VPair (T.VBigMap bigMap, T.VPair (_, T.VPair (T.VC (T.CvNat totalSupply) , _))) ->
      case Map.lookup (T.CvAddress address) bigMap of
        Just (T.VC (T.CvNat balance)) ->
          (alrAccountBalance addLiquidityResult) == balance &&
          (alrTotalSupply addLiquidityResult) == totalSupply
        _ -> False

-- | This validator checks the result of contracts/exchange.tz execution.
--
-- It checks following properties:
--
-- * The contract ends with an error if amount is greater than balance.
-- * The contract ends with an error when divide by zero occurs.
-- * The contract storage succesfully adds liquidity to an account and increases
--   the total supply.
--
validateAddLiquidityContinuation
  :: ContractEnv
  -> Parameter
  -> Storage
  -> ContractPropValidator (ToT Storage) Property
validateAddLiquidityContinuation env param storage result =
  case (param, getAddLiquidityCon (ceSource env) storage) of
    (Continuation balance, Just addLiquidityParams') -> validateAddLiquidityContinuation' env balance addLiquidityParams' storage result
    _ -> failedProp "Expected Continuation with AddLiquidityParams stored in the continuationStorage for the source map"

validateAddLiquidityContinuation'
  :: ContractEnv
  -> Balance
  -> AddLiquidityParams
  -> Storage
  -> ContractPropValidator (ToT Storage) Property
validateAddLiquidityContinuation'
  env
  paramBalance
  addLiquidityParams'
  (Storage _oldBigMap _immutableStorage mutableStorage')
  (resE, _interpreterState) = do

  let eExpectedResult =
        (addLiquidityContinuation
          (fromIntegral $ unMutez $ ceBalance env)
          (fromIntegral $ unMutez $ ceAmount env)
          (arg #totalSupply . fst $ mutableStorage')
          paramBalance
          ((\(x,_,_) -> arg #minLiquidity x) addLiquidityParams')
          ((\(_,x,_) -> arg #maxTokens x) addLiquidityParams')
        )

  case eExpectedResult of
    Left err -> counterexample ("Expected failure: " ++ show err) $ isLeft resE
    Right expectedResult ->
  
      case resE of
        Right (_, outputStorage) ->
          if assertResult (ceSource env) outputStorage expectedResult
          then succeededProp
          else failedProp $ T.pack $ "compareOut failed: " ++ showState
        _ -> failedProp $ T.pack $ "Unexpected fail response:\n" ++ showState  
  where
    showState = "env: " ++ show env ++ "\nparam: " ++ show paramBalance ++ "\nresE: " ++ show resE
