{-|
Module      : Test.Dexter.Contract.Exchange.Continuation.RemoveLiquidity
Copyright   : (c) camlCase, 2019
Maintainer  : james@camlcase.io

Remove liquidity continuation.

-}

{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE OverloadedLabels       #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE ViewPatterns           #-}

module Test.Dexter.Contract.Exchange.Continuation.RemoveLiquidity
  ( testRemoveLiquidityContinuation
  ) where

import qualified Data.Map.Strict as Map
import qualified Data.Text as T

import Dexter.Contract.Exchange (ContinuationStorage, MutableStorage, Parameter(..), Storage(..), exchangeContract)
import Dexter.Contract.Types (RemoveLiquidityParams, Balance)

import Lorentz (compileLorentz)

import Michelson.Interpret (ContractEnv(..))
import Michelson.Test (ContractPropValidator, contractProp, concatTestTrees, midTimestamp, dummyContractEnv)
import Michelson.Test.Util (failedProp, succeededProp)
import Michelson.Typed (ToT)
import qualified Michelson.Typed as T

import Named (arg)

import Test.Dexter.Contract.Gen (staticImmutableStorage)
import Test.Dexter.Contract.Mock (alice, broker, emptyExchangeStorage, setContinuationStorage)
import Test.Hspec.Expectations (shouldSatisfy)
import Test.QuickCheck (Gen, Property, choose, counterexample, forAll, withMaxSuccess) -- counterexample
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)
import Test.Tasty.QuickCheck (testProperty)

import Tezos.Address (Address(..))
import Tezos.Core (Mutez, unMutez, unsafeMkMutez, timestampPlusSeconds) -- , unMutez)

import Util.Named ((.!))

testRemoveLiquidityContinuation :: IO [TestTree]
testRemoveLiquidityContinuation = concatTestTrees
  [ one . testGroup "Dexter.Contract.Exchange removeLiquidityContinuation" <$>
    exchangeTest (compileLorentz exchangeContract)
  ]
  where
    exchangeTest contract =
      pure
      [ testCase "Calling removeLiquidityContinuation entry path completes" $
          contractProp
            contract              -- Contract
            (flip shouldSatisfy (isRight . fst)) -- ContractPropValidator
            contractEnv           -- ContractEnv
            removeLiquidity       -- Parameter
            emptyExchangeStorage  -- Storage

      , testProperty "Check removeLiquidity properties" $ withMaxSuccess 1000 $
          qcProp contract            
      ]

    qcProp contract =
      forAll ((,,) <$> genEnvAmountAndBalance <*> genContinuation <*> genStorage) $
        \(arbEnv, arbParam, arbStorage) ->
          let validate = validateRemoveLiquidityContinuation arbEnv arbParam arbStorage
           in contractProp contract validate arbEnv arbParam arbStorage

-- =============================================================================
-- Environment
-- =============================================================================

contractEnv :: ContractEnv
contractEnv =
  dummyContractEnv
    { ceNow     = midTimestamp
    , ceAmount  = unsafeMkMutez 0
    , ceBalance = unsafeMkMutez (1000000 * 100)
    , ceSource  = alice
    , ceSender  = alice
    }

genEnvAmountAndBalance :: Gen ContractEnv
genEnvAmountAndBalance = do
  amountInt <- choose (10,100)
  let amount = fromInteger . (* 1000000) $ amountInt
  balance <- fromInteger . (* 1000000) <$> choose (amountInt,200)
  pure $
    dummyContractEnv
      { ceNow     = midTimestamp
      , ceAmount  = unsafeMkMutez amount
      , ceBalance = unsafeMkMutez balance
      , ceSource  = alice
      , ceSender  = broker
      }

-- =============================================================================
-- Parameter
-- =============================================================================

-- | Generate a small Natural number.
genNatural :: Gen Natural
genNatural = fromInteger <$> choose (10, 100)

-- | Generate a parameter for the continuation entry point.
genContinuation :: Gen Parameter
genContinuation = Continuation . fromInteger <$> choose (10, 100)

-- =============================================================================
-- Storage
-- =============================================================================

-- | removeLiquidityParamsPass with parameters that should pass.
removeLiquidityParams :: RemoveLiquidityParams
removeLiquidityParams =
  ( #burnAmount .! 1
  , #minMutez   .! unsafeMkMutez 1000000
  , #minTokens  .! 1
  , #deadline   .! (midTimestamp `timestampPlusSeconds` 1)
  )

removeLiquidity :: Parameter
removeLiquidity = RemoveLiquidity removeLiquidityParams

-- | Generate MutableStorage with an empty continuationStorage map.
genMutableStorage :: Gen Natural -> Gen MutableStorage
genMutableStorage genNatural' = (,) <$> ((#totalSupply .!) <$> genNatural') <*> (pure $ #continuationStorage .! Map.empty)

-- | Generate MutableStorage with an empty continuationStorage map.
genStorage :: Gen Storage
genStorage =
  flip setContinuationStorage (Map.fromList [(alice, Left . Right $ removeLiquidityParams)])
  <$> Storage (T.BigMap . Map.fromList $ [(alice, 100)]) staticImmutableStorage <$> genMutableStorage genNatural

-- =============================================================================
-- Validity test
-- =============================================================================

getBalance :: Address -> Storage -> Maybe Balance
getBalance address (Storage bigMap _ _) = Map.lookup address (T.unBigMap bigMap)

getRemoveLiquidityContinuation :: Address -> Storage -> Maybe RemoveLiquidityParams
getRemoveLiquidityContinuation address (Storage _ _ mutableStorage') =
  let continuationStorage = arg #continuationStorage . snd $ mutableStorage' :: Map.Map Address ContinuationStorage
  in
  case Map.lookup address continuationStorage of
    Nothing -> Nothing
    Just (Left (Right removeLiquidityParams')) -> Just removeLiquidityParams'
    _ -> Nothing

-- | This validator checks the result of contracts/exchange.tz execution.
validateRemoveLiquidityContinuation
  :: ContractEnv
  -> Parameter
  -> Storage
  -> ContractPropValidator (ToT Storage) Property
validateRemoveLiquidityContinuation env param storage result =
  case (param, getRemoveLiquidityContinuation (ceSource env) storage) of
    (Continuation balance, Just removeLiquidityParams') ->
      case getBalance (ceSource env) storage of
        Nothing -> failedProp . T.pack $ "Expected balance in BigMap for account: " ++ (show $ ceSource env)
        Just accountBalance -> validateRemoveLiquidityContinuation' env balance accountBalance removeLiquidityParams' storage result      
    _ -> failedProp "Expected Continuation with RemoveLiquidityParams stored in the continuationStorage for the source map"

validateRemoveLiquidityContinuation'
  :: ContractEnv
  -> Balance
  -> Balance
  -> RemoveLiquidityParams
  -> Storage
  -> ContractPropValidator (ToT Storage) Property
validateRemoveLiquidityContinuation'
  env
  paramBalance
  accountBalance
  removeLiquidityParams'
  (Storage _oldBigMap _immutableStorage mutableStorage')
  (resE, _interpreterState) = do

  let eExpectedResult =
        (removeLiquidityContinuation
          burnAmount
          (fromIntegral . unMutez . ceBalance $ env)
          accountBalance
          totalSupply
          paramBalance
          (fromIntegral . unMutez $ minMutez)
          minTokens
        )

  case (eExpectedResult, resE) of
    (Left err,_) -> counterexample ("Expected failure: " ++ show err) $ isLeft resE
    _ -> succeededProp 

    -- there is an issue with Morley testing. `source >> contract@()` does not pass
    -- maybe it does not have a way to derive the type.
    -- (Right expectedResult, Right (_, outputStorage)) ->
    --   if assertResult (ceSource env) outputStorage expectedResult
    --   then succeededProp
    --   else failedProp $ T.pack $ "assertResult failed: " ++ showState

    -- _ -> failedProp $ T.pack $ "Unexpected fail response:\nexpectedResult: " ++ (show eExpectedResult) ++ "\n" ++ showState 

  where
    _showState = "env: " ++ show env ++ "\nparam: " ++ show paramBalance ++ "\nresE: " ++ show resE
    
    (arg #burnAmount -> burnAmount, arg #minMutez  -> minMutez,
     arg #minTokens  -> minTokens, arg #deadline -> _deadline) = removeLiquidityParams'

    totalSupply = arg #totalSupply . fst $ mutableStorage' :: Natural

    _assertResult :: Address -> T.Value (ToT Storage) -> RemoveLiquidityResult -> Bool
    _assertResult address storage removeLiquidityResult =
      case storage of
        T.VPair (T.VBigMap bigMap, T.VPair (_, T.VPair (T.VC (T.CvNat totalSupply') , _))) ->
          case Map.lookup (T.CvAddress address) bigMap of
            Just (T.VC (T.CvNat balance)) ->
              (rlrAccountToken removeLiquidityResult) == balance &&
              (rlrTotalSupply removeLiquidityResult) == totalSupply'
            _ -> False


-- =============================================================================
-- Mock dexter contract
-- =============================================================================

-- | Errors that can occur during the calculation of add liquidity continuation.
data RemoveLiquidityError
  = BurnAmountGreaterThanBalance
  | TotalSupplyIsZero
  | TezAmountIsLessThanMin
  | TokenAmountIsLessThanMin
  deriving (Eq, Read, Show)

-- | The succesful result of the add liquidity continuation.
data RemoveLiquidityResult
  = RemoveLiquidityResult
    { rlrAccountMutez :: Mutez
    , rlrAccountToken :: Natural
    , rlrTotalSupply  :: Natural
    } deriving (Eq, Show)

-- | Emulate the Lorentz addLiquidityContinuation function in pure Haskell.
removeLiquidityContinuation
  :: Natural
  -> Natural
  -> Natural
  -> Natural
  -> Natural
  -> Natural
  -> Natural
  -> Either RemoveLiquidityError RemoveLiquidityResult
removeLiquidityContinuation
  burnAmount      -- DEX, amount user wants to burn
  contractBalance -- Mutez, xtz of the contract
  accountBalance
  totalSupply     -- DEX, total DEX
  tokenReserve    -- Token of the contract
  minMutez
  minTokens
  = do
  _ <- if burnAmount > accountBalance then Left BurnAmountGreaterThanBalance else pure ()
  tezAmount <- if totalSupply > 0 then pure (burnAmount * contractBalance `div` totalSupply) else Left TotalSupplyIsZero
  _ <- if tezAmount >= minMutez then pure () else Left TezAmountIsLessThanMin

  let tokenAmount = burnAmount * tokenReserve `div` totalSupply
  _ <- if tokenAmount >= minTokens then pure () else Left TokenAmountIsLessThanMin

  let newTotalSupply = totalSupply - burnAmount

  pure $ RemoveLiquidityResult
    (unsafeMkMutez . fromIntegral $ tezAmount) -- how much is given to user
    tokenAmount               -- how many tokesn are given to the user
    newTotalSupply            -- the new amount of totalDEX
