{-|
Module      : Test.Dexter.Contract.Gen
Copyright   : (c) camlCase, 2019
Maintainer  : james@camlcase.io

Generation functions that producte Test.QuickCheck.Gen values for common types.

-}

{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE OverloadedLabels       #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE ViewPatterns           #-}

module Test.Dexter.Contract.Gen where

import Dexter.Contract.Exchange (ImmutableStorage)

import Michelson.Typed.Haskell.Value (ContractAddr(..))

import Test.Dexter.Contract.Mock (alice, bob, broker, token)
import Test.QuickCheck (Gen, choose, elements)

import Tezos.Address (Address)

import Util.Named ((.!))

genAddress :: Gen Address
genAddress = elements [alice, bob]

genSmallAmount :: Gen Natural
genSmallAmount = fromInteger . ((*) 1000000) <$> choose (0, 10)

genMidAmount :: Gen Natural
genMidAmount = fromInteger . ((*) 1000000) <$> choose (100, 1000)

staticImmutableStorage :: ImmutableStorage
staticImmutableStorage = ((#brokerAddress .! (ContractAddr broker)) , (#tokenAddress .! (ContractAddr token)))
