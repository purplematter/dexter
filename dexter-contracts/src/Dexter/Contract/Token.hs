{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DerivingStrategies     #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE NoApplicativeDo        #-}
{-# LANGUAGE OverloadedLabels       #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE QuasiQuotes            #-}
{-# LANGUAGE RebindableSyntax       #-}
{-# LANGUAGE TemplateHaskell        #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE ViewPatterns           #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}

module Dexter.Contract.Token where

import Lorentz    hiding (createAccount)
import GHC.Generics      (Generic)
import Prelude           (Natural, Text, toStrict)
import Tezos.Core        (unsafeMkMutez)

type Balance     = Natural
type Allowance   = Natural
type Allowances  = Map Address Allowance
type Account     = (Balance, Allowances)

-- parameter
data Parameter
  = InitToken     InitParams
  | Approve       ("spender" :! Address, "tokens"  :! Natural)
  | Transfer      ("dest" :! Address, "tokens" :! Natural)
  | CreateAccount ("dest" :! Address, "tokens" :! Natural)
  | AllowanceOf   ("from" :! Address, "spender" :! Address, "forward" :! ContractAddr (Allowance, Balance))
  | GetBalance    ("spender" :! Address, "forward" :! ContractAddr Balance)
  deriving stock Generic
  deriving anyclass IsoValue

type InitParams = ( "owner"       :! Address
                  , "totalSupply" :! Natural
                  , "decimals"    :! Natural
                  , "name"        :! MText
                  , "symbol"      :! MText
                  )

data TransferParams =
  TransferParams
    { from :: Address
    , dest :: Address
    , tokens :: Natural
    } deriving stock Generic
      deriving anyclass IsoValue

-- type Accounts = BigMap Address Account
-- this should be a BigMap but there is a problem
-- with the way that Lorentz is handling the types, it needs
-- to put it at the top left, but it isn't doing that
type Accounts = Map Address Account

data Storage =
  Storage
    { accounts    :: Accounts
    , version     :: Natural
    , totalSupply :: Natural
    , decimals    :: Natural
    , name        :: MText
    , symbol      :: MText
    , owner       :: Address
    }
    deriving stock Generic
    deriving anyclass IsoValue

tokenContract :: Contract Parameter Storage
tokenContract = do
  unpair
  caseT @Parameter
    ( #cInitToken /-> do
        getField #owner; dip (swap); setField #owner; swap;
        getField #totalSupply; dip (swap); setField #totalSupply; swap;
        getField #decimals; dip (swap); setField #decimals; swap;
        getField #name; dip (swap); setField #name; swap;
        getField #symbol; dip (swap); setField #symbol;
        push (1 :: Natural); setField #version;

        swap; createOwnerAccount;
        
        nil;
        pair;

    , #cApprove /-> do
        -- [ApproveParams, Storage]
        swap;       -- [Storage, ApproveParams]
        sender;     -- [Address, Storage, ApproveParams]
        getAccount; -- [Account, Storage, ApproveParams]

        dip swap # swap; -- [ApproveParams, Account, Storage]
        getField #tokens;
        push (0 :: Natural);
        eq;

        -- [ApproveParams, Account, Storage]
        if_ ( do
                swap; unpair;             -- [Balance, Allowances, ApproveParams, Storage]
                dip ( dip (getField #spender) -- [Balance, Allowances, spender, ApproveParams, Storage]
                    # swap                -- [Balance, spender, Allowances, ApproveParams, Storage]
                    # dip none # update); -- [Balance, Allowances, ApproveParams, Storage]              
                pair;
            )
            ( do
                getField #spender;                -- [spender, ApproveParams, Account, Storage]
                dip (getField #tokens # some)     -- [spender, some tokens, ApproveParams, Account, Storage]
                dip (dip (swap # dup # cdr)) -- [spender, some tokens, Allowances, Account, ApproveParams, Storage]
                update;                    -- [Allowances, Account, ApproveParams, Storage]
                swap; car; pair
            );
        -- [Account, ApproveParams, Storage]
        some
        dip (swap # getField #accounts) -- [Account, Accounts, Storage, ApproveParams]
        sender
        update

        setField #accounts
        dip drop        
        nil; pair;

    , #cTransfer /-> do
        sender
        dip (getField #dest # dip (toField #tokens))
        toTransferParams
        performTransfer
        nil; pair

    , #cCreateAccount /-> createAccount # nil # pair

    , #cAllowanceOf /-> do
        getAllowance
        nil; swap; cons
        pair

    , #cGetBalance /-> do
        -- get the spender's account
        getField #spender -- [spender, Input, Storage]
        dip swap      -- [spender, Storage, Input]
        getAccount    -- [spenderAccount, Storage, Input]

        -- get the spender's balance
        car           -- [spenderBalance, Storage, Input]

        -- get the contract
        dip (swap # toField #forward)

        -- send the amount the user holds back to the provided contract
        dip (push (unsafeMkMutez 0)) -- [spenderBalance, Mutez, ContractAddr, Storage]
        transferTokens    -- [Operations, Storage]
        nil; swap; cons
        pair
    )

createOwnerAccount :: '[InitParams, Storage] :-> '[Storage]
createOwnerAccount = do
  dip (getField #accounts); -- [InputParams, Accounts, Storage]

  getField #totalSupply;
  emptyMap;
  swap;
  pair; 
  some; -- [Option (Balance, Allowances), InputParams, Accounts, Storage]
  
  swap;
  toField #owner; -- [owner, Option (Balance, Allowances), Accounts, Storage]

  update;
  setField #accounts;

assertOwner :: Storage & s :-> Storage & s
assertOwner = do
  getField #owner
  sender
  assertEq [mt|Expected the owner of this contract.|]

createAccount :: ( HasFieldOfType a "dest" Address
                 , HasFieldOfType a "tokens" Natural)
              => a & Storage & s :-> Storage & s
createAccount = do
  -- assert that the sender of createAccount is the owner
  swap; assertOwner -- Storage & CreateAccountParams

  getField #owner -- owner & Storage & CreateAccountParams
  dip ( swap # getField #dest -- owner & dest & CreateAccountParams & Storage 
      # dip (toField #tokens) -- owner & dest & tokens & Storage 
      )

  toTransferParams

  performTransfer

toTransferParams :: Address & Address & Natural & s :-> TransferParams & s
toTransferParams = do
  construct $
    fieldCtor (sender) -- used as a dummy value
    :& fieldCtor (sender) -- used as a dummy value
    :& fieldCtor (push (0 :: Natural))
    :& RNil
  swap # setField #from
  swap # setField #dest
  swap # setField #tokens

getAccount :: Address & Storage & s :-> Account & Storage & s
getAccount = do
  dip (getField #accounts); get; ifNone ( emptyMap # push 0 # pair ) nop

getAccount' :: Address & Accounts & s :-> Account & s
getAccount' = do
  get; ifNone ( emptyMap # push 0 # pair ) nop

performTransfer :: ( HasFieldOfType a "from" Address
                   , HasFieldOfType a "dest" Address
                   , HasFieldOfType a "tokens" Natural
                   )
             => a ': Storage ': s :-> Storage ': s
performTransfer = do
  -- get the account or the from address
  getField #from; dip swap; -- [Address, Storage, Input]
  getAccount;           -- [Account, Storage, Input]
  dup;                  -- [Account, Account, Storage, Input]

  -- get the difference between
  -- the from address account balance
  -- and the amount of tokens they want to transfer

  car;                  -- [Balance, Account, Storage, Input]
  dip (dip swap # swap # getField #tokens); -- [Balance, Tokens, Input, Account, Storage]
  sub;
        
  -- check that from.account.balance - tokens
  -- is greater than zero
  isNat
  ifNone ( push [mt|Not enough tokens for transfer to|] # failWith)
    -- set the new balance of the from account
    ( dip (swap # cdr) -- [b, Input, Account, Storage]
      # pair -- [Account, Input, Storage]
    );

  -- update the accounts with the from account
  some;              -- [Option Account, Input, Storage]
  dip (getField #from); swap -- [Address, Option Account, Input, Storage]
  dip (dip (dip (getField #accounts) # swap)) -- [Address, Option Account, Accounts, Input, Storage]
  update; -- [Accounts, Input, Storage]

  -- get the destination account
  dip (getField #dest) -- [Accounts, Address, Input, Storage]
  dup; (dip swap); swap -- [Address, Accounts, Accounts, Input, Storage]
  getAccount'; -- [Account, Accounts, Input, Storage]

  -- get tokens from the input
  dip (swap # getField #tokens); -- [Account, Input.tokens, Input, Accounts, Storage]

  -- add the tokens to the destination account
  swap; dip (dup # car); add -- [Input.tokens + Account.balance, Account, Input, Accounts, Storage]
  dip cdr; pair              -- [Account, Input, Accounts, Storage] 

  -- update the destination account in accounts
  swap; toField #dest; -- [Address (dest), Account, Accounts, Storage] 
  dip some;            -- [Address (dest), Option Account, Accounts, Storage] 
  update               -- [Accounts, Storage] 

  -- set accounts the storage
  setField #accounts

getAllowance :: ( HasFieldOfType a "from" Address
                , HasFieldOfType a "spender" Address
                , HasFieldOfType a "forward" (ContractAddr (Allowance, Balance)))
             => a & Storage & s :-> Operation & Storage & s
getAllowance = do
  getField #from; dip (swap # getField #accounts); get -- [Maybe account, Storage, a]
  ifNone (push (0 :: Natural) # push (0 :: Natural) # pair) -- [(Allowance, Balance), Storage, a]
         ( do -- got the account [account, Storage, a]]
           unpair # swap -- [allowances, balance, Storage, a]
           dip (dip swap # swap) # swap # getField #spender -- [spender, a, allowances, balance, Storage]
           dip swap # get -- [Maybe allowance, a, balance, Storage]
           ifNone (do
                   swap # (dip swap) -- [balance, Storage, a]
                   push (0 :: Natural) # pair -- [(Allowance, Balance), Storage, a]
                  )
                  ( do -- [Allowance, a, balance, Storage]
                    dip (swap # dip swap) -- [Allowance, balance, Storage, a]
                    pair
                  )
         )
  -- [(Allowance, Balance), Storage, a]
  dip (swap # toField #forward)
  dip (push (unsafeMkMutez 0))
  transferTokens

printTokenContract :: Text
printTokenContract = toStrict $ printLorentzContract False lcwDumb tokenContract
