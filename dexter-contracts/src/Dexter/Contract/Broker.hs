{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DerivingStrategies     #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE NoApplicativeDo        #-}
{-# LANGUAGE OverloadedLabels       #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE QuasiQuotes            #-}
{-# LANGUAGE RebindableSyntax       #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE TemplateHaskell        #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE ViewPatterns           #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}

module Dexter.Contract.Broker where

import Dexter.Contract.Types
import GHC.Generics    (Generic)
import Lorentz
import Michelson.Typed (BigMap, IsoValue)
import Prelude         (Either, Text, toStrict)
import Tezos.Core      (unsafeMkMutez)

data Parameter
  = ExchangeToGetBalance CallerToGetBalanceParams
  | GetBalanceToExchange GetBalanceToCallerParams
  deriving stock Generic
  deriving anyclass IsoValue

type TempStore = (Mutez, Address)

data Storage =
  Storage
    { store :: BigMap Address TempStore -- token address to exchange address
    , dummy :: () -- because BigMap needs to be in a pair
    }
    deriving stock Generic
    deriving anyclass IsoValue

brokerContract :: Contract Parameter Storage
brokerContract = do
  unpair
  caseT @Parameter
    ( #cExchangeToGetBalance /-> exchangeToGetBalance
    , #cGetBalanceToExchange /-> getBalanceToExchange
    )

updateTempStore
  :: ContractAddr SimpleTokenContractParameter & ContractAddr ExchangeContractParameter & Storage & s
  :-> Storage & s
updateTempStore = do
  address
  dip $ do
    address
    amount
    pair

    some
    dip (getField #store)
  
  update
  setField #store
  
exchangeToGetBalance :: '[CallerToGetBalanceParams, Storage] :-> '[([Operation], Storage)]
exchangeToGetBalance = do
  getField #tokenAddress
  dip swap
  dip $ do
    sender
    contract @ExchangeContractParameter
    ifNone (do
               push [mt|addLiquidityToGetBalance: sender contract does not match the expected type.|]
               failWith
           )
           nop

  updateTempStore
  swap
  
  getField #tokenAccount
  right @TransferParams
  
  dip $ do
    toField #tokenAddress
    push @Mutez (unsafeMkMutez 0)

  stackType @[Either TransferParams Address, Mutez, ContractAddr SimpleTokenContractParameter, Storage]
  transferTokens

  nil; swap; cons; pair

getBalanceToExchange :: '[GetBalanceToCallerParams, Storage] :-> '[([Operation], Storage)]
getBalanceToExchange = do
  right >> right >> right
  dip $ do
    sender
    stackType @[Address, Storage]
    dip (getField #store)
    get
    ifNone (push [mt|getBalanceToAddLiquidity: unexpected, exchange not found for existing token contract.|] # failWith) nop
    unpair
    stackType @[Mutez, Address, Storage]

    dip $ do 
      contract @ExchangeContractParameter
      ifNone (do
                 push [mt|getBalanceToAddLiquidity: exchange contract does not match the expected type.|]
                 failWith
             )
             nop

  stackType @[ExchangeContractParameter, Mutez, ContractAddr ExchangeContractParameter, Storage]

  transferTokens

  nil; swap; cons; pair

printBrokerContract :: Text
printBrokerContract = toStrict $ printLorentzContract False lcwDumb brokerContract
