{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DerivingStrategies     #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE NoApplicativeDo        #-}
{-# LANGUAGE OverloadedLabels       #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE QuasiQuotes            #-}
{-# LANGUAGE RebindableSyntax       #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE TemplateHaskell        #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE ViewPatterns           #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}

module Dexter.Contract.SimpleToken where

import Dexter.Contract.Types
import GHC.Generics          (Generic)
import Lorentz
import Michelson.Typed       (IsoValue)
import Prelude               (Text, toStrict)
import Tezos.Core            (unsafeMkMutez)

-- parameter
data Parameter
  = Transfer   TransferParams
  | GetBalance GetBalanceParams
  deriving stock Generic
  deriving anyclass IsoValue


-- because it cannot handle a data type with four or more entries
type MetaData =
  ( "name"   :! MText
  , "symbol" :! MText
  )

data Storage =
  Storage
    { accounts    :: Accounts
    , totalSupply :: Natural
    , metaData    :: MetaData
    }
    deriving stock Generic
    deriving anyclass IsoValue

simpleTokenContract :: Contract Parameter Storage
simpleTokenContract = do
  unpair
  caseT @Parameter
    ( #cTransfer /-> transfer
    , #cGetBalance /-> getBalance
    )

transfer :: '[TransferParams, Storage] :-> '[([Operation], Storage)]
transfer = do
  performTransfer
  nil; pair

getAccountFromStorage :: Address & Storage & s :-> Account & Storage & s
getAccountFromStorage = do
  dip (getField #accounts); getAccountFromAccounts

getAccountFromAccounts :: Address & Accounts & s :-> Account & s
getAccountFromAccounts = do
  get; ifNone ( emptyMap # push 0 # pair ) nop

performTransfer :: forall a s.
  ( HasFieldOfType a "from" Address
  , HasFieldOfType a "dest" Address
  , HasFieldOfType a "tokens" Natural
  )
  => a ': Storage ': s :-> Storage ': s
performTransfer = do
  -- get the account or the from address  
  getField #from; dip swap
  getAccountFromStorage
  dup
  stackType @(Account : Account : Storage : a : s)

  -- from.account.balance - tokens
  -- get the difference between
  -- the from address account balance
  -- and the amount of tokens they want to transfer
  car
  dip (dip swap # swap # getField #tokens)
  sub
  stackType @(Integer : a : Account : Storage : s)

  -- (from.account.balance - tokens) > 0
  isNat
  ifNone (push [mt|performTransfer: not enough tokens for transfer to|] # failWith)
         -- set the new balance of the from account
         ( do
             dip (swap # cdr)
             pair
         )
  stackType @(Account : a : Storage : s)

  -- update the accounts with the from account
  some
  dip (getField #from); swap
  dip (dip (dip (getField #accounts) # swap))
  update
  stackType @(Accounts : a : Storage : s)

  -- get the destination account
  dip (getField #dest)
  dup; (dip swap); swap
  getAccountFromAccounts
  stackType @(Account : Accounts : a : Storage : s)

  -- get tokens from the input
  dip (swap # getField #tokens)
  stackType @(Account : Natural : a : Accounts : Storage : s)

  -- add the tokens to the destination account
  swap; dip (dup # car); add
  dip cdr; pair
  stackType @(Account : a : Accounts : Storage : s) 

  -- update the destination account in accounts
  swap; toField #dest
  dip some
  update
  stackType @(Accounts : Storage : s)

  -- set accounts the storage
  setField #accounts

getBalance :: '[Address, Storage] :-> '[([Operation], Storage)]
getBalance = do
  -- normally you look up the balance with the address
  -- for simplicity we are ignoring it and sending a static value
  dip (getField #accounts)
  get
  ifNone (push @Balance 0) (car)
  right @CallerToGetBalanceParams
  stackType @[GetBalanceBrokerContractParameter, Storage]

  dip $ do
    sender
    contract @GetBalanceBrokerContractParameter
    ifNone (do
               push [mt|GetBalance: sender contract does not match the expected type, first try.|]
               failWith
           )
           nop

    push @Mutez (unsafeMkMutez 0)
  
  stackType @[GetBalanceBrokerContractParameter, Mutez, ContractAddr GetBalanceBrokerContractParameter, Storage]
  
  transferTokens
  nil; swap; cons; pair

printSimpleTokenContract :: Text
printSimpleTokenContract = toStrict $ printLorentzContract False lcwDumb simpleTokenContract
