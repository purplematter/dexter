module Main where

import System.IO (hSetEncoding, utf8)

import Dexter.Contract.Broker      (printBrokerContract)
import Dexter.Contract.Exchange    (printExchangeContract)
import Dexter.Contract.SimpleToken (printSimpleTokenContract)

-- ~/alphanet.sh client typecheck script container:contracts/token.tz
-- ~/alphanet.sh client typecheck script container:contracts/broker.tz
-- ~/alphanet.sh client typecheck script container:contracts/exchange.tz

writeFileUtf8 :: Print text => FilePath -> text -> IO ()
writeFileUtf8 name txt =
  withFile name WriteMode $ \h -> hSetEncoding h utf8 >> hPutStr h txt

main :: IO ()
main = do
  writeFileUtf8 "contracts/broker.tz"    printBrokerContract
  writeFileUtf8 "contracts/token.tz"     printSimpleTokenContract
  writeFileUtf8 "contracts/exchange.tz"  printExchangeContract

{-
Originate the contracts

~/alphanet.sh client originate contract tezosGold for alice \
                                transferring 0 from alice \
                                running container:contracts/token.tz \
                                --init '(Pair {Elt "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH" (Pair 1000000 {})} (Pair 1000000 (Pair "Tezos Gold" "TGD")))' --burn-cap 2 --force

~/alphanet.sh client originate contract broker for alice \
                                transferring 0 from alice \
                                running container:contracts/broker.tz \
                                --init '(Pair {} Unit)' --burn-cap 2 --force

~/alphanet.sh client list known contracts

broker:    KT1MkNg2N6XqZmbcE5kq4iVKhqvvdN84LnH1
tezosGold: KT1WQAW1sRaykMPYEPpqiL4nrYvdnb8SWTV7

~/alphanet.sh client originate contract tezosGoldExchange for alice \
                                transferring 0 from alice \
                                running container:contracts/exchange.tz \
                                --init '(Pair {} (Pair (Pair "KT1MkNg2N6XqZmbcE5kq4iVKhqvvdN84LnH1" "KT1WQAW1sRaykMPYEPpqiL4nrYvdnb8SWTV7") (Pair 0 {})))' --burn-cap 10 --force


-- TransferParams

-- transfer tokens to simon
~/alphanet.sh client transfer 0 from alice to tezosGold --arg '(Left (Pair "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH" (Pair "tz1cSBeSxJf6dK1U8HtMSvPt2hMSYDtbf4JB" 100)))' --burn-cap 1

-- transfer tokens to bob
~/alphanet.sh client transfer 0 from alice to tezosGold --arg '(Left (Pair "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH" (Pair "tz1PYgf9fBGLXvwx8ag8sdwjLJzmyGdNiswM" 100)))' --burn-cap 1


~/alphanet.sh client get big map value for '"tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH"' of type 'address' in tezosGold
~/alphanet.sh client get big map value for '"tz1cSBeSxJf6dK1U8HtMSvPt2hMSYDtbf4JB"' of type 'address' in tezosGold
~/alphanet.sh client get big map value for '"tz1PYgf9fBGLXvwx8ag8sdwjLJzmyGdNiswM"' of type 'address' in tezosGold


~/alphanet.sh client get big map value for '"tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH"' of type 'address' in '"KT1V86wJqJvgJ1FNPYrrdeUdopcmREMJyV9M"'


-- AddLiquidityParams
~/alphanet.sh client transfer 10 from alice to tezosGoldExchange --arg 'Left (Left (Pair 1 (Pair 100 "2020-06-29T18:00:21Z")))' --burn-cap 1

~/alphanet.sh client transfer 11 from bob to tezosGoldExchange --arg 'Left (Left (Pair 1 (Pair 100 "2020-06-29T18:00:21Z")))' --burn-cap 1

~/alphanet.sh client transfer 10 from simon to tezosGoldExchange --arg 'Left (Left (Pair 1 (Pair 100 "2020-06-29T18:00:21Z")))' --burn-cap 1

-- RemoveLiquidityParams
~/alphanet.sh client transfer 0 from alice to tezosGoldExchange --arg 'Left (Right (Pair (Pair 100 1) (Pair 1 "2020-06-29T18:00:21Z")))' --burn-cap 1

-- TezToToken
~/alphanet.sh client transfer 1 from bob to tezosGoldExchange --arg 'Right (Left (Pair 1 "2020-06-29T18:00:21Z"))' --burn-cap 1

-- TokenToTez
~/alphanet.sh client transfer 0 from bob to tezosGoldExchange --arg 'Right (Right (Left (Pair 10 (Pair 1 "2020-06-29T18:00:21Z"))))' --burn-cap 1

-- Get Liquidity
~/alphanet.sh client get script storage for tezosGoldExchange
~/alphanet.sh client get balance for tezosGoldExchange

-- helpful functions
~/alphanet.sh client list known addresses
~/alphanet.sh client list known contracts
~/alphanet.sh client get big map value for '"tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH"' of type 'address' in tezosGoldExchange
~/alphanet.sh client get big map value for '"tz1PYgf9fBGLXvwx8ag8sdwjLJzmyGdNiswM"' of type 'address' in tezosGold
~/alphanet.sh client get script storage for tezosGoldExchange

~/alphanet.sh client get balance for alice
~/alphanet.sh client get balance for bob
~/alphanet.sh client get balance for simon

~/alphanet.sh client transfer 500 from alice to simon --fee 0.5
~/alphanet.sh client gen keys simon

simon: tz1cSBeSxJf6dK1U8HtMSvPt2hMSYDtbf4JB (unencrypted sk known)
bob: tz1PYgf9fBGLXvwx8ag8sdwjLJzmyGdNiswM (unencrypted sk known)
alice: tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH (unencrypted sk known)

-}
