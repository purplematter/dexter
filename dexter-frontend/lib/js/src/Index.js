'use strict';

var React = require("react");
var Dexter = require("./Dexter/Dexter.js");
var ReactDOMRe = require("reason-react/lib/js/src/ReactDOMRe.js");
var Caml_option = require("bs-platform/lib/js/caml_option.js");
var Tezos_ContractId = require("./Tezos/Tezos_ContractId.js");
var Dexter_ContractsMap = require("./Dexter/Dexter_ContractsMap.js");

var tezosGold = Tezos_ContractId.ofString("KT1DmCHxit2bQ2GiHVc24McY6meuJPMTrqD8");

var tezosGoldExchange = Tezos_ContractId.ofString("KT1NNHerEYbKvUMAhMYikRCs2Aoym7HQLqPB");

var tezosSilver = Tezos_ContractId.ofString("KT1GutmbJNisDvN2XiuKLdoRHukSfh5bsUUc");

var tezosSilverExchange = Tezos_ContractId.ofString("KT1AecgjTUtHibSn64MbNQ4VBCNQELJweSZ4");

var tastyTacos = Tezos_ContractId.ofString("KT1AszhGevBf3EFP5EhxaR4i15oXpqzD5kaE");

var tastyTacosExchange = Tezos_ContractId.ofString("KT1Su9h3rAn6K66qucpN8954nrmjvBAoMmhp");

var checkers = Tezos_ContractId.ofString("KT1F3D8aAeiYA6EWu96AJhES1zRYJPm4UpiM");

var checkersExchange = Tezos_ContractId.ofString("KT1Rq4qVCkuDZQAE3YtoMQkk32dhMdZfwUcj");

var wrappedLTC = Tezos_ContractId.ofString("KT1AfhJ6kxvtqYMRJ2uzq3xSiH9cvHBENGSV");

var wrappedLTCExchange = Tezos_ContractId.ofString("KT19Mteifa4XT4PtwRS5AWYMj5fV2sv6getq");

var contractList_000 = /* tuple */[
  "Checkers",
  /* tuple */[
    checkersExchange,
    checkers
  ]
];

var contractList_001 = /* :: */[
  /* tuple */[
    "Wrapped LTC",
    /* tuple */[
      wrappedLTCExchange,
      wrappedLTC
    ]
  ],
  /* :: */[
    /* tuple */[
      "Tasty Tacos",
      /* tuple */[
        tastyTacosExchange,
        tastyTacos
      ]
    ],
    /* :: */[
      /* tuple */[
        "Tezos Gold",
        /* tuple */[
          tezosGoldExchange,
          tezosGold
        ]
      ],
      /* :: */[
        /* tuple */[
          "Tezos Silver",
          /* tuple */[
            tezosSilverExchange,
            tezosSilver
          ]
        ],
        /* [] */0
      ]
    ]
  ]
];

var contractList = /* :: */[
  contractList_000,
  contractList_001
];

var oContractMap = Dexter_ContractsMap.ofList(contractList);

if (oContractMap !== undefined) {
  ReactDOMRe.renderToElementWithId(React.createElement(Dexter.make, {
            contractMap: Caml_option.valFromOption(oContractMap)
          }), "root");
} else {
  console.log("error");
}

exports.tezosGold = tezosGold;
exports.tezosGoldExchange = tezosGoldExchange;
exports.tezosSilver = tezosSilver;
exports.tezosSilverExchange = tezosSilverExchange;
exports.tastyTacos = tastyTacos;
exports.tastyTacosExchange = tastyTacosExchange;
exports.checkers = checkers;
exports.checkersExchange = checkersExchange;
exports.wrappedLTC = wrappedLTC;
exports.wrappedLTCExchange = wrappedLTCExchange;
exports.contractList = contractList;
exports.oContractMap = oContractMap;
/* tezosGold Not a pure module */
