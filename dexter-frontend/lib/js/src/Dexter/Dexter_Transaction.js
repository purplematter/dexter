'use strict';


function toString(t) {
  switch (t) {
    case 0 : 
        return "Tez (XTZ) to Tokens";
    case 1 : 
        return "Tokens to Tez (XTZ)";
    case 2 : 
        return "Add Liquidity";
    case 3 : 
        return "Remove Liquidity";
    
  }
}

exports.toString = toString;
/* No side effect */
