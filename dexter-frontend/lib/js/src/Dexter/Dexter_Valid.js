'use strict';

var Caml_option = require("bs-platform/lib/js/caml_option.js");

function toOption(i) {
  if (typeof i === "number") {
    return undefined;
  } else {
    return Caml_option.some(i[0]);
  }
}

function ofOption(o) {
  if (o !== undefined) {
    return /* Ok */[Caml_option.valFromOption(o)];
  } else {
    return /* Error */1;
  }
}

function notOk(i) {
  if (typeof i === "number") {
    return true;
  } else {
    return false;
  }
}

function toStyle(i) {
  if (typeof i === "number") {
    if (i !== 0) {
      return {
              borderColor: "red"
            };
    } else {
      return { };
    }
  } else {
    return {
            borderColor: "green"
          };
  }
}

exports.toOption = toOption;
exports.ofOption = ofOption;
exports.notOk = notOk;
exports.toStyle = toStyle;
/* No side effect */
