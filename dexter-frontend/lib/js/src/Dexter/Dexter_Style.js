'use strict';

var Css = require("bs-css/lib/js/src/Css.js");

var topSpace = Css.style(/* :: */[
      Css.marginTop(Css.px(20)),
      /* [] */0
    ]);

var signIn = Css.style(/* :: */[
      Css.marginTop(Css.px(5)),
      /* :: */[
        Css.borderRadius(Css.px(4)),
        /* :: */[
          Css.backgroundColor(Css.white),
          /* :: */[
            Css.color(Css.black),
            /* :: */[
              Css.padding2(Css.px(10), Css.px(10)),
              /* :: */[
                Css.border(Css.px(2), Css.solid, Css.hex("C0C0C0")),
                /* [] */0
              ]
            ]
          ]
        ]
      ]
    ]);

var tokenSelector = Css.style(/* :: */[
      Css.marginTop(Css.px(5)),
      /* :: */[
        Css.borderRadius(Css.px(4)),
        /* :: */[
          Css.backgroundColor(Css.white),
          /* :: */[
            Css.color(Css.black),
            /* :: */[
              Css.padding2(Css.px(5), Css.px(5)),
              /* :: */[
                Css.border(Css.px(2), Css.solid, Css.hex("C0C0C0")),
                /* [] */0
              ]
            ]
          ]
        ]
      ]
    ]);

var buttonGroup = Css.style(/* :: */[
      Css.marginTop(Css.px(5)),
      /* :: */[
        Css.display(/* inlineBlock */-147785676),
        /* :: */[
          Css.selector("button", /* :: */[
                Css.backgroundColor(Css.white),
                /* :: */[
                  Css.border(Css.px(1), Css.solid, Css.grey),
                  /* :: */[
                    Css.color(Css.black),
                    /* :: */[
                      Css.padding2(Css.px(10), Css.px(24)),
                      /* :: */[
                        Css.cursor(/* pointer */-786317123),
                        /* :: */[
                          Css.$$float(/* left */-944764921),
                          /* :: */[
                            Css.after(/* :: */[
                                  Css.unsafe("content", "\"\""),
                                  /* :: */[
                                    Css.clear(Css.both),
                                    /* :: */[
                                      Css.display(Css.table),
                                      /* [] */0
                                    ]
                                  ]
                                ]),
                            /* :: */[
                              Css.hover(/* :: */[
                                    Css.backgroundColor(Css.hex("C0C0C0")),
                                    /* [] */0
                                  ]),
                              /* :: */[
                                Css.not__(":last-child", /* :: */[
                                      Css.unsafe("borderRight", "none"),
                                      /* [] */0
                                    ]),
                                /* :: */[
                                  Css.firstChild(/* :: */[
                                        Css.borderTopLeftRadius(Css.px(4)),
                                        /* :: */[
                                          Css.borderBottomLeftRadius(Css.px(4)),
                                          /* [] */0
                                        ]
                                      ]),
                                  /* :: */[
                                    Css.lastChild(/* :: */[
                                          Css.borderTopRightRadius(Css.px(4)),
                                          /* :: */[
                                            Css.borderBottomRightRadius(Css.px(4)),
                                            /* [] */0
                                          ]
                                        ]),
                                    /* [] */0
                                  ]
                                ]
                              ]
                            ]
                          ]
                        ]
                      ]
                    ]
                  ]
                ]
              ]),
          /* [] */0
        ]
      ]
    ]);

function buttonSelected(selected) {
  if (selected) {
    return Css.style(/* :: */[
                Css.border(Css.px(2), Css.solid, Css.blue),
                /* [] */0
              ]);
  } else {
    return Css.style(/* :: */[
                Css.opacity(0.6),
                /* [] */0
              ]);
  }
}

exports.topSpace = topSpace;
exports.signIn = signIn;
exports.tokenSelector = tokenSelector;
exports.buttonGroup = buttonGroup;
exports.buttonSelected = buttonSelected;
/* topSpace Not a pure module */
