'use strict';

var Css = require("bs-css/lib/js/src/Css.js");
var List = require("bs-platform/lib/js/list.js");
var $$Array = require("bs-platform/lib/js/array.js");
var Block = require("bs-platform/lib/js/block.js");
var Curry = require("bs-platform/lib/js/curry.js");
var React = require("react");
var TezBridge = require("../TezBridge/TezBridge.js");
var Belt_Option = require("bs-platform/lib/js/belt_Option.js");
var Dexter_Form = require("./Dexter_Form/Dexter_Form.js");
var Dexter_Style = require("./Dexter_Style.js");
var Belt_MapString = require("bs-platform/lib/js/belt_MapString.js");
var Tezos_ContractId = require("../Tezos/Tezos_ContractId.js");
var CommonUi_DropDown = require("../CommonUi/CommonUi_DropDown.js");
var Dexter_Transaction = require("./Dexter_Transaction.js");

function eventToValue($$event) {
  return $$event.target.value;
}

var tzscanAlphanetUrl = "https://alphanet.tzscan.io/";

function toString(param) {
  return param[0];
}

var ContractDropDown = CommonUi_DropDown.MakeDropDown(/* module */[/* toString */toString]);

function Dexter(Props) {
  var contractMap = Props.contractMap;
  var contractList = Belt_MapString.toList(contractMap);
  var contract = List.hd(contractList);
  var getExchangeContract = function (c) {
    return c[1][0];
  };
  var match = React.useReducer((function (state, action) {
          switch (action.tag | 0) {
            case 0 : 
                return /* record */[
                        /* address */action[0],
                        /* contract */state[/* contract */1],
                        /* dexterTransaction */state[/* dexterTransaction */2],
                        /* transactions */state[/* transactions */3]
                      ];
            case 1 : 
                return /* record */[
                        /* address */state[/* address */0],
                        /* contract */action[0],
                        /* dexterTransaction */state[/* dexterTransaction */2],
                        /* transactions */state[/* transactions */3]
                      ];
            case 2 : 
                return /* record */[
                        /* address */state[/* address */0],
                        /* contract */state[/* contract */1],
                        /* dexterTransaction */action[0],
                        /* transactions */state[/* transactions */3]
                      ];
            case 3 : 
                return /* record */[
                        /* address */state[/* address */0],
                        /* contract */state[/* contract */1],
                        /* dexterTransaction */state[/* dexterTransaction */2],
                        /* transactions */List.concat(/* :: */[
                              /* :: */[
                                action[0],
                                /* [] */0
                              ],
                              /* :: */[
                                state[/* transactions */3],
                                /* [] */0
                              ]
                            ])
                      ];
            
          }
        }), /* record */[
        /* address */undefined,
        /* contract */contract,
        /* dexterTransaction : MutezToTokens */0,
        /* transactions : [] */0
      ]);
  var dispatch = match[1];
  var state = match[0];
  var match$1 = state[/* address */0];
  var tmp;
  if (match$1 !== undefined) {
    var address = match$1;
    tmp = React.createElement("label", undefined, "Your are logged in with: ", React.createElement("a", {
              href: tzscanAlphanetUrl + address,
              target: "_blank"
            }, address));
  } else {
    tmp = React.createElement("label", undefined, "Your Tezos address is needed before you can start trading, get it from tezbridge.");
  }
  return React.createElement("div", undefined, React.createElement("h1", undefined, "Dexter 0.1.0 (alphanet)"), React.createElement("div", {
                  className: Dexter_Style.topSpace
                }, React.createElement("h2", undefined, "How to Use Dexter"), React.createElement("a", {
                      href: "https://gitlab.com/camlcase-dev/dexter/blob/master/docs/dexter-frontend.md",
                      target: "_blank"
                    }, "Dexter Frontend Guide")), React.createElement("div", {
                  className: Dexter_Style.topSpace
                }, tmp), Belt_Option.isNone(state[/* address */0]) ? React.createElement("button", {
                    className: Css.merge(/* :: */[
                          Dexter_Style.topSpace,
                          /* :: */[
                            Dexter_Style.signIn,
                            /* [] */0
                          ]
                        ]),
                    onClick: (function (_event) {
                        Curry._1(TezBridge.getSource, /* () */0).then((function (result) {
                                Curry._1(dispatch, /* UpdateAddress */Block.__(0, [result]));
                                return Promise.resolve(/* () */0);
                              }));
                        return /* () */0;
                      })
                  }, "Get Tezos address from Tezbridge") : null, React.createElement("div", {
                  className: Dexter_Style.topSpace
                }, React.createElement("label", undefined, "Select Token:"), React.createElement("div", undefined, React.createElement(ContractDropDown[/* make */0], {
                          onChange: (function (contract) {
                              return Curry._1(dispatch, /* UpdateContract */Block.__(1, [contract]));
                            }),
                          selectedItem: state[/* contract */1],
                          elements: $$Array.of_list(contractList),
                          className: Dexter_Style.tokenSelector
                        }))), React.createElement("div", {
                  className: Dexter_Style.topSpace
                }, React.createElement("h2", undefined, "Choose Dexter Transaction"), React.createElement("div", {
                      className: Dexter_Style.buttonGroup
                    }, $$Array.of_list(List.mapi((function (i, t) {
                                var i$1 = i;
                                var t$1 = t;
                                return React.createElement("button", {
                                            key: String(i$1),
                                            className: Dexter_Style.buttonSelected(t$1 === state[/* dexterTransaction */2]),
                                            type: "button",
                                            onClick: (function (param) {
                                                return Curry._1(dispatch, /* UpdateDexterTransaction */Block.__(2, [t$1]));
                                              })
                                          }, Dexter_Transaction.toString(t$1));
                              }), /* :: */[
                              /* MutezToTokens */0,
                              /* :: */[
                                /* TokensToMutez */1,
                                /* :: */[
                                  /* AddLiquidity */2,
                                  /* :: */[
                                    /* RemoveLiquidity */3,
                                    /* [] */0
                                  ]
                                ]
                              ]
                            ]))), React.createElement("div", {
                      className: Dexter_Style.topSpace
                    }, React.createElement("h3", undefined, Dexter_Transaction.toString(state[/* dexterTransaction */2]))), React.createElement(Dexter_Form.make, {
                      address: state[/* address */0],
                      dexterContract: getExchangeContract(state[/* contract */1]),
                      dexterTransaction: state[/* dexterTransaction */2],
                      onChange: (function (dexterTransaction, transactionResponse) {
                          return Curry._1(dispatch, /* PrependTransactions */Block.__(3, [/* record */[
                                          /* dexterTransaction */dexterTransaction,
                                          /* transactionResponse */transactionResponse
                                        ]]));
                        })
                    })), React.createElement("div", {
                  className: Dexter_Style.topSpace
                }, React.createElement("h2", undefined, "Transactions"), React.createElement("ul", undefined, $$Array.of_list(List.mapi((function (i, transaction) {
                                return React.createElement("li", {
                                            key: String(i)
                                          }, React.createElement("span", undefined, Dexter_Transaction.toString(transaction[/* dexterTransaction */0])), React.createElement("span", undefined, ": "), React.createElement("a", {
                                                href: tzscanAlphanetUrl + transaction[/* transactionResponse */1].operation_id,
                                                target: "_blank"
                                              }, transaction[/* transactionResponse */1].operation_id));
                              }), state[/* transactions */3])))), React.createElement("div", {
                  className: Dexter_Style.topSpace
                }, React.createElement("h2", undefined, "Contracts"), React.createElement("ul", undefined, $$Array.of_list(List.concat(List.map((function (param) {
                                    var match = param[1];
                                    var token = match[1];
                                    var exchange = match[0];
                                    var title = param[0];
                                    return /* :: */[
                                            React.createElement("li", {
                                                  key: Tezos_ContractId.toString(exchange)
                                                }, React.createElement("a", {
                                                      href: tzscanAlphanetUrl + Tezos_ContractId.toString(exchange),
                                                      target: "_blank"
                                                    }, title + " Dexter Exchange")),
                                            /* :: */[
                                              React.createElement("li", {
                                                    key: Tezos_ContractId.toString(token)
                                                  }, React.createElement("a", {
                                                        href: tzscanAlphanetUrl + Tezos_ContractId.toString(token),
                                                        target: "_blank"
                                                      }, title)),
                                              /* [] */0
                                            ]
                                          ];
                                  }), contractList))))));
}

var make = Dexter;

exports.eventToValue = eventToValue;
exports.tzscanAlphanetUrl = tzscanAlphanetUrl;
exports.ContractDropDown = ContractDropDown;
exports.make = make;
/* ContractDropDown Not a pure module */
