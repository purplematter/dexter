'use strict';

var Block = require("bs-platform/lib/js/block.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.bs.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function encode(primitiveInstruction) {
  switch (primitiveInstruction) {
    case 0 : 
        return "ADD";
    case 1 : 
        return "LE";
    case 2 : 
        return "UNIT";
    case 3 : 
        return "COMPARE";
    case 4 : 
        return "LAMBDA";
    case 5 : 
        return "LOOP";
    case 6 : 
        return "IMPLICIT_ACCOUNT";
    case 7 : 
        return "NONE";
    case 8 : 
        return "BLAKE2B";
    case 9 : 
        return "SHA256";
    case 10 : 
        return "XOR";
    case 11 : 
        return "RENAME";
    case 12 : 
        return "MAP";
    case 13 : 
        return "SET_DELEGATE";
    case 14 : 
        return "DIP";
    case 15 : 
        return "PACK";
    case 16 : 
        return "SIZE";
    case 17 : 
        return "IF_CONS";
    case 18 : 
        return "LSR";
    case 19 : 
        return "TRANSFER_TOKENS";
    case 20 : 
        return "UPDATE";
    case 21 : 
        return "CDR";
    case 22 : 
        return "SWAP";
    case 23 : 
        return "SOME";
    case 24 : 
        return "SHA512";
    case 25 : 
        return "CHECK_SIGNATURE";
    case 26 : 
        return "BALANCE";
    case 27 : 
        return "EMPTY_SET";
    case 28 : 
        return "SUB";
    case 29 : 
        return "MEM";
    case 30 : 
        return "RIGHT";
    case 31 : 
        return "ADDRESS";
    case 32 : 
        return "CONTACT";
    case 33 : 
        return "UNPACK";
    case 34 : 
        return "NOT";
    case 35 : 
        return "LEFT";
    case 36 : 
        return "AMOUNT";
    case 37 : 
        return "DROP";
    case 38 : 
        return "ABS";
    case 39 : 
        return "GE";
    case 40 : 
        return "PUSH";
    case 41 : 
        return "LT";
    case 42 : 
        return "NEQ";
    case 43 : 
        return "NEG";
    case 44 : 
        return "CON";
    case 45 : 
        return "EXEC";
    case 46 : 
        return "NIL";
    case 47 : 
        return "ISNAT";
    case 48 : 
        return "MUL";
    case 49 : 
        return "LOOP_LEFT";
    case 50 : 
        return "EDIV";
    case 51 : 
        return "SLICE";
    case 52 : 
        return "STEPS_TO_QUOTA";
    case 53 : 
        return "INT";
    case 54 : 
        return "SOURCE";
    case 55 : 
        return "CAR";
    case 56 : 
        return "CREATE_ACCOUNT";
    case 57 : 
        return "LSL";
    case 58 : 
        return "OR";
    case 59 : 
        return "IF_NONE";
    case 60 : 
        return "SELF";
    case 61 : 
        return "IF";
    case 62 : 
        return "SENDER";
    case 63 : 
        return "DUP";
    case 64 : 
        return "EQ";
    case 65 : 
        return "NOW";
    case 66 : 
        return "GET";
    case 67 : 
        return "GT";
    case 68 : 
        return "IF_LEFT";
    case 69 : 
        return "FAILWITH";
    case 70 : 
        return "PAIR";
    case 71 : 
        return "ITER";
    case 72 : 
        return "CAST";
    case 73 : 
        return "EMPTY_MAP";
    case 74 : 
        return "CREATE_CONTRACT";
    case 75 : 
        return "HAS_KEY";
    case 76 : 
        return "CONTRACT";
    case 77 : 
        return "AND";
    
  }
}

function decode(json) {
  var exit = 0;
  var str;
  try {
    str = Json_decode.string(json);
    exit = 1;
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn[0] === Json_decode.DecodeError) {
      return /* Error */Block.__(1, ["PrimitiveInstruction.decode: " + exn[1]]);
    } else {
      throw exn;
    }
  }
  if (exit === 1) {
    switch (str) {
      case "ABS" : 
          return /* Ok */Block.__(0, [/* Abs */38]);
      case "ADD" : 
          return /* Ok */Block.__(0, [/* Add */0]);
      case "ADDRESS" : 
          return /* Ok */Block.__(0, [/* Address */31]);
      case "AMOUNT" : 
          return /* Ok */Block.__(0, [/* Amount */36]);
      case "AND" : 
          return /* Ok */Block.__(0, [/* And */77]);
      case "BALANCE" : 
          return /* Ok */Block.__(0, [/* Balance */26]);
      case "BLAKE2B" : 
          return /* Ok */Block.__(0, [/* Blake2B */8]);
      case "CAR" : 
          return /* Ok */Block.__(0, [/* Car */55]);
      case "CAST" : 
          return /* Ok */Block.__(0, [/* Cast */72]);
      case "CDR" : 
          return /* Ok */Block.__(0, [/* Cdr */21]);
      case "CHECK_SIGNATURE" : 
          return /* Ok */Block.__(0, [/* CheckSignature */25]);
      case "COMPARE" : 
          return /* Ok */Block.__(0, [/* Compare */3]);
      case "CON" : 
          return /* Ok */Block.__(0, [/* Con */44]);
      case "CONCAT" : 
          return /* Ok */Block.__(0, [/* Concat */32]);
      case "CONTRACT" : 
          return /* Ok */Block.__(0, [/* Contract */76]);
      case "CREATE_ACCOUNT" : 
          return /* Ok */Block.__(0, [/* CreateAccount */56]);
      case "CREATE_CONTRACT" : 
          return /* Ok */Block.__(0, [/* CreateContract */74]);
      case "DIP" : 
          return /* Ok */Block.__(0, [/* Dip */14]);
      case "DROP" : 
          return /* Ok */Block.__(0, [/* Drop */37]);
      case "DUP" : 
          return /* Ok */Block.__(0, [/* Dup */63]);
      case "EDIV" : 
          return /* Ok */Block.__(0, [/* Ediv */50]);
      case "EMPTY_MAP" : 
          return /* Ok */Block.__(0, [/* EmptyMap */73]);
      case "EMPTY_SET" : 
          return /* Ok */Block.__(0, [/* EmptySet */27]);
      case "EQ" : 
          return /* Ok */Block.__(0, [/* Eq */64]);
      case "EXEC" : 
          return /* Ok */Block.__(0, [/* Exec */45]);
      case "FAILWITH" : 
          return /* Ok */Block.__(0, [/* Failwith */69]);
      case "GET" : 
          return /* Ok */Block.__(0, [/* Get */66]);
      case "GT" : 
          return /* Ok */Block.__(0, [/* Gt */67]);
      case "HAS_KEY" : 
          return /* Ok */Block.__(0, [/* HasKey */75]);
      case "IF" : 
          return /* Ok */Block.__(0, [/* If */61]);
      case "IF_CONS" : 
          return /* Ok */Block.__(0, [/* IfCons */17]);
      case "IF_LEFT" : 
          return /* Ok */Block.__(0, [/* IfLeft */68]);
      case "IF_NONE" : 
          return /* Ok */Block.__(0, [/* IfNone */59]);
      case "IMPLICIT_ACCOUNT" : 
          return /* Ok */Block.__(0, [/* ImplicitAccount */6]);
      case "INT" : 
          return /* Ok */Block.__(0, [/* Int */53]);
      case "ISNAT" : 
          return /* Ok */Block.__(0, [/* Isnat */47]);
      case "ITER" : 
          return /* Ok */Block.__(0, [/* Iter */71]);
      case "LAMBDA" : 
          return /* Ok */Block.__(0, [/* Lambda */4]);
      case "LE" : 
          return /* Ok */Block.__(0, [/* Le */1]);
      case "LEFT" : 
          return /* Ok */Block.__(0, [/* Left */35]);
      case "LOOP" : 
          return /* Ok */Block.__(0, [/* Loop */5]);
      case "LOOP_LEFT" : 
          return /* Ok */Block.__(0, [/* LoopLeft */49]);
      case "LSL" : 
          return /* Ok */Block.__(0, [/* Lsl */57]);
      case "LSR" : 
          return /* Ok */Block.__(0, [/* Lsr */18]);
      case "LT" : 
          return /* Ok */Block.__(0, [/* Lt */41]);
      case "MAP" : 
          return /* Ok */Block.__(0, [/* Map */12]);
      case "MEM" : 
          return /* Ok */Block.__(0, [/* Mem */29]);
      case "MUL" : 
          return /* Ok */Block.__(0, [/* Mul */48]);
      case "NEG" : 
          return /* Ok */Block.__(0, [/* Neg */43]);
      case "NEQ" : 
          return /* Ok */Block.__(0, [/* Neq */42]);
      case "NIL" : 
          return /* Ok */Block.__(0, [/* Nil */46]);
      case "NONE" : 
          return /* Ok */Block.__(0, [/* None */7]);
      case "NOT" : 
          return /* Ok */Block.__(0, [/* Not */34]);
      case "NOW" : 
          return /* Ok */Block.__(0, [/* Now */65]);
      case "OR" : 
          return /* Ok */Block.__(0, [/* Or */58]);
      case "PACK" : 
          return /* Ok */Block.__(0, [/* Pack */15]);
      case "PAIR" : 
          return /* Ok */Block.__(0, [/* Pair */70]);
      case "PUSH" : 
          return /* Ok */Block.__(0, [/* Push */40]);
      case "RENAME" : 
          return /* Ok */Block.__(0, [/* Rename */11]);
      case "RIGHT" : 
          return /* Ok */Block.__(0, [/* Right */30]);
      case "SELF" : 
          return /* Ok */Block.__(0, [/* Self */60]);
      case "SENDER" : 
          return /* Ok */Block.__(0, [/* Sender */62]);
      case "SET_DELEGATE" : 
          return /* Ok */Block.__(0, [/* SetDelegate */13]);
      case "SHA256" : 
          return /* Ok */Block.__(0, [/* Sha256 */9]);
      case "SHA512" : 
          return /* Ok */Block.__(0, [/* Sha512 */24]);
      case "SIZE" : 
          return /* Ok */Block.__(0, [/* Size */16]);
      case "SLICE" : 
          return /* Ok */Block.__(0, [/* Slice */51]);
      case "SOME" : 
          return /* Ok */Block.__(0, [/* Some */23]);
      case "SOURCE" : 
          return /* Ok */Block.__(0, [/* Source */54]);
      case "STEPS_TO_QUOTA" : 
          return /* Ok */Block.__(0, [/* StepsToQuota */52]);
      case "SUB" : 
          return /* Ok */Block.__(0, [/* Sub */28]);
      case "SWAP" : 
          return /* Ok */Block.__(0, [/* Swap */22]);
      case "TE" : 
          return /* Ok */Block.__(0, [/* Ge */39]);
      case "TRANSFER_TOKENS" : 
          return /* Ok */Block.__(0, [/* TransferTokens */19]);
      case "UNIT" : 
          return /* Ok */Block.__(0, [/* Unit */2]);
      case "UNPACK" : 
          return /* Ok */Block.__(0, [/* Unpack */33]);
      case "UPDATE" : 
          return /* Ok */Block.__(0, [/* Update */20]);
      case "XOR" : 
          return /* Ok */Block.__(0, [/* Xor */10]);
      default:
        return /* Error */Block.__(1, ["PrimitiveInstruction.decode: " + str]);
    }
  }
  
}

exports.encode = encode;
exports.decode = decode;
/* No side effect */
