'use strict';

var Block = require("bs-platform/lib/js/block.js");
var Moment = require("moment");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.bs.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function encode(t) {
  return t[0].toJSON();
}

function decode(json) {
  var exit = 0;
  var v;
  try {
    v = Json_decode.string(json);
    exit = 1;
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn[0] === Json_decode.DecodeError) {
      return /* Error */Block.__(1, [exn[1]]);
    } else {
      throw exn;
    }
  }
  if (exit === 1) {
    return /* Ok */Block.__(0, [/* Timestamp */[Moment(v)]]);
  }
  
}

function toString(t) {
  return t[0].toJSON();
}

function ofString(str) {
  return /* Timestamp */[Moment(str)];
}

function mk(moment) {
  return /* Timestamp */[moment];
}

exports.encode = encode;
exports.decode = decode;
exports.toString = toString;
exports.ofString = ofString;
exports.mk = mk;
/* moment Not a pure module */
