'use strict';

var Block = require("bs-platform/lib/js/block.js");
var Belt_Id = require("bs-platform/lib/js/belt_Id.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.bs.js");
var Caml_primitive = require("bs-platform/lib/js/caml_primitive.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function encode(t) {
  return t[0];
}

function decode(json) {
  var exit = 0;
  var v;
  try {
    v = Json_decode.string(json);
    exit = 1;
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn[0] === Json_decode.DecodeError) {
      return /* Error */Block.__(1, ["ContractId.decode failed: " + exn[1]]);
    } else {
      throw exn;
    }
  }
  if (exit === 1) {
    return /* Ok */Block.__(0, [/* ContractId */[v]]);
  }
  
}

function ofString(candidate) {
  if (candidate.startsWith("KT1")) {
    return /* Ok */Block.__(0, [/* ContractId */[candidate]]);
  } else {
    return /* Error */Block.__(1, ["ContractId.mk: unexpected candidate string: " + candidate]);
  }
}

function toString(t) {
  return t[0];
}

function cmp(c0, c1) {
  return Caml_primitive.caml_string_compare(c0[0], c1[0]);
}

var Comparable = Belt_Id.MakeComparable(/* module */[/* cmp */cmp]);

exports.encode = encode;
exports.decode = decode;
exports.ofString = ofString;
exports.toString = toString;
exports.Comparable = Comparable;
/* Comparable Not a pure module */
