'use strict';

var Block = require("bs-platform/lib/js/block.js");
var Int64 = require("bs-platform/lib/js/int64.js");
var Caml_obj = require("bs-platform/lib/js/caml_obj.js");
var Caml_int64 = require("bs-platform/lib/js/caml_int64.js");
var Tezos_Util = require("./Tezos_Util.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.bs.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function encode(t) {
  return Int64.to_string(t[0]);
}

function decode(json) {
  var exit = 0;
  var v;
  try {
    v = Json_decode.string(json);
    exit = 1;
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn[0] === Json_decode.DecodeError) {
      return /* Error */Block.__(1, ["Mutez.decode failed: " + exn[1]]);
    } else {
      throw exn;
    }
  }
  if (exit === 1) {
    var match = Tezos_Util.int64OfString(v);
    if (match !== undefined) {
      var $$int = match;
      if (Int64.compare($$int, Int64.zero) >= 0) {
        return /* Ok */Block.__(0, [/* Mutez */[$$int]]);
      } else {
        return /* Error */Block.__(1, ["Mutez.decode failed: " + v]);
      }
    } else {
      return /* Error */Block.__(1, ["Mutez.decode failed: " + v]);
    }
  }
  
}

function ofInt64($$int) {
  if (Int64.compare($$int, Int64.zero) >= 0) {
    return /* Ok */Block.__(0, [/* Mutez */[$$int]]);
  } else {
    return /* Error */Block.__(1, ["Mutez.mk"]);
  }
}

function ofInt($$int) {
  if ($$int >= 0) {
    return ofInt64(Caml_int64.of_int32($$int));
  } else {
    return /* Error */Block.__(1, ["ofInt: Expected non-negative int."]);
  }
}

function ofString(string) {
  var match = Tezos_Util.int64OfString(string);
  if (match !== undefined) {
    var match$1 = ofInt64(match);
    if (match$1.tag) {
      return /* Error */Block.__(1, ["ofString: expected a non-negative int64 value."]);
    } else {
      return /* Ok */Block.__(0, [match$1[0]]);
    }
  } else {
    return /* Error */Block.__(1, ["ofString: expected a non-negative int64 value encoded as a string."]);
  }
}

function ofTezString(string) {
  var match = Tezos_Util.floatOfString(string);
  if (match !== undefined) {
    var pos = Tezos_Util.getPositionOfLastDecimalDigit(string);
    if (Caml_obj.caml_lessequal(pos, 6)) {
      var mutezFloat = match * 1000000;
      var int64 = Caml_int64.of_float(mutezFloat);
      var match$1 = ofInt64(int64);
      if (match$1.tag) {
        return /* Error */Block.__(1, ["ofString: expected a non-negative int64 value."]);
      } else {
        return /* Ok */Block.__(0, [match$1[0]]);
      }
    } else {
      return /* Error */Block.__(1, ["ofTezString: expected a non-negative float with precision up to 10^-6, received: " + string]);
    }
  } else {
    return /* Error */Block.__(1, ["ofTezString: expected a non-negative float with precision up to 10^-6"]);
  }
}

function toInt64(t) {
  return t[0];
}

function toString(t) {
  return Int64.to_string(t[0]);
}

var zero = /* Mutez */[Int64.zero];

var one = /* Mutez */[Int64.one];

function add(x, y) {
  return /* Mutez */[Caml_int64.add(x[0], y[0])];
}

function sub(x, y) {
  return /* Mutez */[Caml_int64.sub(x[0], y[0])];
}

function mul(x, y) {
  return /* Mutez */[Caml_int64.mul(x[0], y[0])];
}

var oneTez = /* Mutez */[/* int64 */[
    /* hi */0,
    /* lo */1000000
  ]];

exports.encode = encode;
exports.decode = decode;
exports.ofInt64 = ofInt64;
exports.ofInt = ofInt;
exports.ofString = ofString;
exports.ofTezString = ofTezString;
exports.toInt64 = toInt64;
exports.toString = toString;
exports.zero = zero;
exports.one = one;
exports.oneTez = oneTez;
exports.add = add;
exports.sub = sub;
exports.mul = mul;
/* No side effect */
