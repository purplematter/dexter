'use strict';


var Timestamp = 0;

var ContractId = 0;

var Mutez = 0;

var Token = 0;

var PrimitiveInstruction = 0;

var PrimitiveData = 0;

var PrimitiveType = 0;

var Primitives = 0;

var Expression = 0;

var Code = 0;

exports.Timestamp = Timestamp;
exports.ContractId = ContractId;
exports.Mutez = Mutez;
exports.Token = Token;
exports.PrimitiveInstruction = PrimitiveInstruction;
exports.PrimitiveData = PrimitiveData;
exports.PrimitiveType = PrimitiveType;
exports.Primitives = Primitives;
exports.Expression = Expression;
exports.Code = Code;
/* No side effect */
