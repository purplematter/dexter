'use strict';

var Block = require("bs-platform/lib/js/block.js");
var Int64 = require("bs-platform/lib/js/int64.js");
var Caml_int64 = require("bs-platform/lib/js/caml_int64.js");
var Tezos_Util = require("./Tezos_Util.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.bs.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function encode(t) {
  return Int64.to_string(t[0]);
}

function decode(json) {
  var exit = 0;
  var v;
  try {
    v = Json_decode.string(json);
    exit = 1;
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn[0] === Json_decode.DecodeError) {
      return /* Error */Block.__(1, ["Token.decode failed: " + exn[1]]);
    } else {
      throw exn;
    }
  }
  if (exit === 1) {
    var match = Tezos_Util.int64OfString(v);
    if (match !== undefined) {
      var $$int = match;
      if (Int64.compare($$int, Int64.zero) >= 0) {
        return /* Ok */Block.__(0, [/* Token */[$$int]]);
      } else {
        return /* Error */Block.__(1, ["Token.decode failed: " + v]);
      }
    } else {
      return /* Error */Block.__(1, ["Token.decode failed: " + v]);
    }
  }
  
}

function ofInt64($$int) {
  if (Int64.compare($$int, Int64.zero) >= 0) {
    return /* Ok */Block.__(0, [/* Token */[$$int]]);
  } else {
    return /* Error */Block.__(1, ["ofInt64 expected a non-negative int64 value."]);
  }
}

function ofString(string) {
  var match = Tezos_Util.int64OfString(string);
  if (match !== undefined) {
    var match$1 = ofInt64(match);
    if (match$1.tag) {
      return /* Error */Block.__(1, ["ofString expected a non-negative int64 value."]);
    } else {
      return /* Ok */Block.__(0, [match$1[0]]);
    }
  } else {
    return /* Error */Block.__(1, ["ofString expected a non-negative int64 encoded string."]);
  }
}

function toString(t) {
  return Int64.to_string(t[0]);
}

function toInt64(t) {
  return t[0];
}

var zero = /* Token */[Int64.zero];

var one = /* Token */[Int64.one];

function add(x, y) {
  return /* Token */[Caml_int64.add(x[0], y[0])];
}

function sub(x, y) {
  return /* Token */[Caml_int64.sub(x[0], y[0])];
}

function mul(x, y) {
  return /* Token */[Caml_int64.mul(x[0], y[0])];
}

exports.encode = encode;
exports.decode = decode;
exports.ofInt64 = ofInt64;
exports.ofString = ofString;
exports.toString = toString;
exports.toInt64 = toInt64;
exports.zero = zero;
exports.one = one;
exports.add = add;
exports.sub = sub;
exports.mul = mul;
/* No side effect */
