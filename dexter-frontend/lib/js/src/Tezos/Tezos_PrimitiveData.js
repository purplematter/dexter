'use strict';

var Block = require("bs-platform/lib/js/block.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.bs.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function encode(primitiveData) {
  switch (primitiveData) {
    case 0 : 
        return "Elt";
    case 1 : 
        return "Right";
    case 2 : 
        return "False";
    case 3 : 
        return "Unit";
    case 4 : 
        return "Some";
    case 5 : 
        return "None";
    case 6 : 
        return "Left";
    case 7 : 
        return "True";
    case 8 : 
        return "Pair";
    
  }
}

function decode(json) {
  var exit = 0;
  var str;
  try {
    str = Json_decode.string(json);
    exit = 1;
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn[0] === Json_decode.DecodeError) {
      return /* Error */Block.__(1, ["PrimitiveData.decode: " + exn[1]]);
    } else {
      throw exn;
    }
  }
  if (exit === 1) {
    switch (str) {
      case "Elt" : 
          return /* Ok */Block.__(0, [/* Elt */0]);
      case "False" : 
          return /* Ok */Block.__(0, [/* False */2]);
      case "Left" : 
          return /* Ok */Block.__(0, [/* Left */6]);
      case "None" : 
          return /* Ok */Block.__(0, [/* None */5]);
      case "Pair" : 
          return /* Ok */Block.__(0, [/* Pair */8]);
      case "Right" : 
          return /* Ok */Block.__(0, [/* Right */1]);
      case "Some" : 
          return /* Ok */Block.__(0, [/* Some */4]);
      case "True" : 
          return /* Ok */Block.__(0, [/* True */7]);
      case "Unit" : 
          return /* Ok */Block.__(0, [/* Unit */3]);
      default:
        return /* Error */Block.__(1, ["PrimitiveData.decode: " + str]);
    }
  }
  
}

exports.encode = encode;
exports.decode = decode;
/* No side effect */
