'use strict';

var Block = require("bs-platform/lib/js/block.js");
var Belt_List = require("bs-platform/lib/js/belt_List.js");
var Tezos_Util = require("./Tezos_Util.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.bs.js");
var Json_encode = require("@glennsl/bs-json/lib/js/src/Json_encode.bs.js");
var Tezos_Expression = require("./Tezos_Expression.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function encode(contract) {
  var parameter_000 = /* PrimitiveType */Block.__(2, [/* Parameter */23]);
  var parameter_001 = /* :: */[
    contract[/* parameter */0],
    /* [] */0
  ];
  var parameter = /* ContractExpression */Block.__(4, [
      parameter_000,
      parameter_001,
      undefined
    ]);
  var storage_000 = /* PrimitiveType */Block.__(2, [/* Storage */8]);
  var storage_001 = /* :: */[
    contract[/* storage */1],
    /* [] */0
  ];
  var storage = /* ContractExpression */Block.__(4, [
      storage_000,
      storage_001,
      undefined
    ]);
  var code_000 = /* PrimitiveType */Block.__(2, [/* Code */24]);
  var code_001 = /* :: */[
    contract[/* code */2],
    /* [] */0
  ];
  var code = /* ContractExpression */Block.__(4, [
      code_000,
      code_001,
      undefined
    ]);
  return Json_encode.list(Tezos_Expression.encode, /* :: */[
              parameter,
              /* :: */[
                storage,
                /* :: */[
                  code,
                  /* [] */0
                ]
              ]
            ]);
}

function isParameter(expression) {
  if (expression.tag === 4) {
    var prim = expression[0];
    switch (prim.tag | 0) {
      case 0 : 
      case 1 : 
          return false;
      case 2 : 
          return prim[0] === 23;
      
    }
  } else {
    return false;
  }
}

function isStorage(expression) {
  if (expression.tag === 4) {
    var prim = expression[0];
    switch (prim.tag | 0) {
      case 0 : 
      case 1 : 
          return false;
      case 2 : 
          return prim[0] === 8;
      
    }
  } else {
    return false;
  }
}

function isCode(expression) {
  if (expression.tag === 4) {
    var prim = expression[0];
    switch (prim.tag | 0) {
      case 0 : 
      case 1 : 
          return false;
      case 2 : 
          return prim[0] >= 24;
      
    }
  } else {
    return false;
  }
}

function getFirstArg(expression) {
  if (expression.tag === 4) {
    var oArgs = expression[1];
    if (oArgs !== undefined) {
      var match = Belt_List.head(oArgs);
      if (match !== undefined) {
        return /* Ok */Block.__(0, [match]);
      } else {
        return /* Error */Block.__(1, ["getFirstArg: args is unexpectedly an empty list. It should have one element."]);
      }
    } else {
      return /* Error */Block.__(1, ["getFirstArg: args is unexpectedly None. It should be a list of one element."]);
    }
  } else {
    return /* Error */Block.__(1, ["getFirstArg: expected a ContractExpression"]);
  }
}

function getParameter(expression) {
  if (isParameter(expression)) {
    return getFirstArg(expression);
  } else {
    return /* Error */Block.__(1, ["getParameter: "]);
  }
}

function getStorage(expression) {
  if (isStorage(expression)) {
    return getFirstArg(expression);
  } else {
    return /* Error */Block.__(1, ["getStorage: "]);
  }
}

function getCode(expression) {
  if (isCode(expression)) {
    return getFirstArg(expression);
  } else {
    return /* Error */Block.__(1, ["getCode: "]);
  }
}

function decode(json) {
  var exit = 0;
  var val;
  try {
    val = Json_decode.tuple3((function (a) {
            return Tezos_Util.unwrapResult(Tezos_Expression.decode(a));
          }), (function (a) {
            return Tezos_Util.unwrapResult(Tezos_Expression.decode(a));
          }), (function (a) {
            return Tezos_Util.unwrapResult(Tezos_Expression.decode(a));
          }), json);
    exit = 1;
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn[0] === Json_decode.DecodeError) {
      return /* Error */Block.__(1, [exn[1]]);
    } else {
      throw exn;
    }
  }
  if (exit === 1) {
    var match = getParameter(val[0]);
    var match$1 = getStorage(val[1]);
    var match$2 = getCode(val[2]);
    if (match.tag || match$1.tag || match$2.tag) {
      return /* Error */Block.__(1, ["unmatched code expressions"]);
    } else {
      return /* Ok */Block.__(0, [/* record */[
                  /* parameter */match[0],
                  /* storage */match$1[0],
                  /* code */match$2[0]
                ]]);
    }
  }
  
}

exports.encode = encode;
exports.isParameter = isParameter;
exports.isStorage = isStorage;
exports.isCode = isCode;
exports.getFirstArg = getFirstArg;
exports.getParameter = getParameter;
exports.getStorage = getStorage;
exports.getCode = getCode;
exports.decode = decode;
/* No side effect */
