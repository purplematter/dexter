'use strict';

var Fs = require("fs");
var Jest = require("@glennsl/bs-jest/lib/js/src/jest.js");
var Block = require("bs-platform/lib/js/block.js");
var Curry = require("bs-platform/lib/js/curry.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.bs.js");
var Json_encode = require("@glennsl/bs-json/lib/js/src/Json_encode.bs.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function resultMap(f, r) {
  if (r.tag) {
    return /* Error */Block.__(1, [r[0]]);
  } else {
    return /* Ok */Block.__(0, [Curry._1(f, r[0])]);
  }
}

function compareJsonDecodeAndEncode(decode, encode, json) {
  var rDecoded = Curry._1(decode, json);
  return Jest.Expect[/* toEqual */12](/* Ok */Block.__(0, [json]), Jest.Expect[/* expect */0](resultMap(encode, rDecoded)));
}

function jsonRoundtripSpecFile(decode, encode, filePath) {
  var json = JSON.parse(Fs.readFileSync(filePath, "utf8"));
  return Jest.test("decode then encode: " + JSON.stringify(json), (function (param) {
                return compareJsonDecodeAndEncode(decode, encode, json);
              }));
}

function encode(t) {
  return Json_encode.object_(/* :: */[
              /* tuple */[
                "name",
                t[/* name */0]
              ],
              /* :: */[
                /* tuple */[
                  "age",
                  t[/* age */1]
                ],
                /* [] */0
              ]
            ]);
}

function decode(json) {
  var exit = 0;
  var v;
  try {
    v = /* record */[
      /* name */Json_decode.field("name", Json_decode.string, json),
      /* age */Json_decode.field("age", Json_decode.$$int, json)
    ];
    exit = 1;
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn[0] === Json_decode.DecodeError) {
      return /* Error */Block.__(1, [exn[1]]);
    } else {
      throw exn;
    }
  }
  if (exit === 1) {
    return /* Ok */Block.__(0, [v]);
  }
  
}

var Person = /* module */[
  /* encode */encode,
  /* decode */decode
];

jsonRoundtripSpecFile(decode, encode, "__tests__/golden/person.json");

exports.resultMap = resultMap;
exports.compareJsonDecodeAndEncode = compareJsonDecodeAndEncode;
exports.jsonRoundtripSpecFile = jsonRoundtripSpecFile;
exports.Person = Person;
/*  Not a pure module */
