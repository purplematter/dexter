let fp = "__tests__/golden/";

JsonTest.jsonRoundtripSpecFile(
  Tezos.Timestamp.decode,
  Tezos.Timestamp.encode,
  fp ++ "timestamp.json",
);

JsonTest.jsonRoundtripSpecFile(
  Tezos.PrimitiveInstruction.decode,
  Tezos.PrimitiveInstruction.encode,
  fp ++ "primitive_instruction.json",
);

JsonTest.jsonRoundtripSpecFile(
  Tezos.Code.decode,
  Tezos.Code.encode,
  fp ++ "code.json",
);

JsonTest.jsonRoundtripSpecFile(
  Tezos.Expression.decode,
  Tezos.Expression.encode,
  fp ++ "expression.json",
);