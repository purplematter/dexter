Jest.test("", _ =>
  Jest.Expect.expect(Tezos_Util.getPositionOfSmallestNonZero(0.000001))
  |> Jest.Expect.toEqual(6)
);

Jest.test("", _ =>
  Jest.Expect.expect(Tezos_Util.getPositionOfSmallestNonZero(0.1))
  |> Jest.Expect.toEqual(1)
);

Jest.test("", _ =>
  Jest.Expect.expect(Tezos_Util.getPositionOfSmallestNonZero(0.100000))
  |> Jest.Expect.toEqual(1)
);

Jest.test("", _ =>
  Jest.Expect.expect(Tezos_Util.getPositionOfSmallestNonZero(1.0))
  |> Jest.Expect.toEqual(0)
);

Jest.test("", _ =>
  Jest.Expect.expect(Tezos_Util.getPositionOfSmallestNonZero(1.030))
  |> Jest.Expect.toEqual(2)
);

Jest.test("", _ =>
  Jest.Expect.expect(Tezos_Util.getPositionOfLastDecimalDigit("1.030"))
  |> Jest.Expect.toEqual(Some(3))
);

Jest.test("", _ =>
  Jest.Expect.expect(Tezos_Util.getPositionOfLastDecimalDigit("1.0"))
  |> Jest.Expect.toEqual(Some(1))
);

Jest.test("", _ =>
  Jest.Expect.expect(Tezos_Util.getPositionOfLastDecimalDigit("1."))
  |> Jest.Expect.toEqual(Some(0))
);

Jest.test("", _ =>
  Jest.Expect.expect(Tezos_Util.getPositionOfLastDecimalDigit("1.0301"))
  |> Jest.Expect.toEqual(Some(4))
);
/*
  Jest.test("", _ =>
    Jest.Expect.expect(Tezos_Util.getPositionOfSmallestNonZero(" 1.0 "))
    |> Jest.Expect.toEqual(None)
  );
 */