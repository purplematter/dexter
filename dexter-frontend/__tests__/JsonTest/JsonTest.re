let resultMap = (f, r) =>
  switch (r) {
  | Belt.Result.Ok(a) => Belt.Result.Ok(f(a))
  | Belt.Result.Error(b) => Belt.Result.Error(b)
  };

let compareJsonDecodeAndEncode = (decode, encode, json) => {
  let rDecoded = decode(json);
  Jest.Expect.expect(resultMap(encode, rDecoded))
  |> Jest.Expect.toEqual(Belt.Result.Ok(json));
};

let jsonRoundtripSpecFile = (decode, encode, filePath) => {
  let json = Js.Json.parseExn(Node.Fs.readFileAsUtf8Sync(filePath));
  Jest.test("decode then encode: " ++ Js.Json.stringify(json), _ =>
    compareJsonDecodeAndEncode(decode, encode, json)
  );
};

/** test the test functions */
module Person = {
  type t = {
    name: string,
    age: int,
  };

  let encode = (t: t) : Js.Json.t =>
    Json.Encode.(
      object_([("name", string(t.name)), ("age", int(t.age))])
    );

  let decode = (json: Js.Json.t) : Belt.Result.t(t, string) =>
    switch (
      Json.Decode.{
        age: field("age", int, json),
        name: field("name", string, json),
      }
    ) {
    | v => Belt.Result.Ok(v)
    | exception (Json.Decode.DecodeError(error)) => Belt.Result.Error(error)
    };
};

jsonRoundtripSpecFile(
  Person.decode,
  Person.encode,
  "__tests__/golden/person.json",
);