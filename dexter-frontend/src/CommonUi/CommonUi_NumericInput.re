let eventToValue = event => ReactEvent.Form.target(event)##value;

module type NumericInputMode = {
  type t;
  let ofString: string => option(t);
  let toString: t => string;
};

module NumericInputToken = {
  type t = Tezos.Token.t;
  let ofString = x =>
    switch (Tezos.Token.ofString(x)) {
    | Belt.Result.Ok(y) => Some(y)
    | Belt.Result.Error(_error) => None
    };
  let toString = Tezos.Token.toString;
};

module NumericInputMutez = {
  type t = Tezos.Mutez.t;
  let ofString = x =>
    switch (Tezos.Mutez.ofTezString(x)) {
    | Belt.Result.Ok(y) => Some(y)
    | Belt.Result.Error(_error) => None
    };
  let toString = Tezos.Mutez.toString;
};

module MakeNumericInput = (NumericInputConfig: NumericInputMode) => {
  type state = {
    value: string,
    valid: bool,
  };

  [@react.component]
  let make =
      (
        ~onChange,
        ~onEmpty=ignore,
        ~onError=ignore,
        ~numValue: option(NumericInputConfig.t),
        ~initialValue: option(string),
        ~style=ReactDOMRe.Style.make(),
        ~disabled=false,
        ~className="",
      ) => {
    let (state: state, dispatch) =
      React.useReducer(
        (_state, value) =>
          switch (value) {
          | "" =>
            onEmpty();
            {value, valid: true};

          | _ =>
            switch (NumericInputConfig.ofString(value)) {
            | Some(i) =>
              onChange(i);
              {value, valid: true};

            | None =>
              onError();
              {value, valid: false};

            | exception _ =>
              onError();
              {value, valid: false};
            }
          },
        {
          let (value, valid) =
            switch (initialValue) {
            | Some(initialValue) => (initialValue, true)
            | _ =>
              switch (numValue) {
              | Some(numValue) => (
                  NumericInputConfig.toString(numValue),
                  true,
                )
              | None => ("", false)
              }
            };
          {value, valid};
        },
      );
    <span className=(state.valid ? "" : "has-error")>
      <input
        className
        disabled
        type_="text"
        onChange=(ev => dispatch(eventToValue(ev)))
        value=state.value
        style
      />
    </span>;
  };
};

module TokenInput = MakeNumericInput(NumericInputToken);
module MutezInput = MakeNumericInput(NumericInputMutez);