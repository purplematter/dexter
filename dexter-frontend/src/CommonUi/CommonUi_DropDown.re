module type DropDown = {type t; let toString: t => string;};

/* Js.Array.indexOf is *not* the same as this implementation since this one
 * compares items with Caml_obj.caml_equal, not JS' ===
 */
let indexOf = (arr, el) => {
  let rec indexOf = n =>
    if (arr[n] == el) {
      n;
    } else {
      indexOf(n + 1);
    };
  switch (indexOf(0)) {
  | n => Some(n)
  | exception _ => None
  };
};

let eventToValue = event => ReactEvent.Form.target(event)##value;

module MakeDropDown = (DropDownConfig: DropDown) => {
  [@react.component]
  let make =
      (
        ~onChange: DropDownConfig.t => unit,
        ~selectedItem: DropDownConfig.t,
        ~elements: array(DropDownConfig.t),
        ~disabled: bool=false,
        ~className: string="form-control",
      ) => {
    let handleChange = evt => {
      let pos = evt |> eventToValue |> int_of_string;
      switch (elements[pos]) {
      | value => onChange(value)
      | exception _ => ()
      };
    };
    <select
      className
      value=(
        Belt.Option.mapWithDefault(
          indexOf(elements, selectedItem),
          "-1",
          string_of_int,
        )
      )
      disabled
      onChange=handleChange>
      (
        elements
        |> Array.mapi((ix, v) => {
             let s = DropDownConfig.toString(v);
             <option key=s value=(string_of_int(ix))>
               (ReasonReact.string(s))
             </option>;
           })
        |> ReasonReact.array
      )
    </select>;
  };
};