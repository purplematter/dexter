/**
 * Include Tez
 * minLiquidity
 * maxTokensDeposited
 * deadline
 * Left (Left (Pair 1 (Pair 100 "2020-06-29T18:00:21Z")))
*/

let encodeAddLiquidity =
    (
      minLiquidityMinted: Tezos.Token.t,
      maxTokensDeposited: Tezos.Token.t,
      deadline: Tezos.Timestamp.t,
    ) =>
  Tezos.(
    Expression.ContractExpression(
      Primitives.PrimitiveData(PrimitiveData.Left),
      Some([
        Expression.ContractExpression(
          Primitives.PrimitiveData(PrimitiveData.Left),
          Some([
            Expression.ContractExpression(
              Primitives.PrimitiveData(PrimitiveData.Pair),
              Some([
                Expression.IntExpression(
                  Tezos.Token.toInt64(minLiquidityMinted),
                ),
                Expression.ContractExpression(
                  Primitives.PrimitiveData(PrimitiveData.Pair),
                  Some([
                    Expression.IntExpression(
                      Tezos.Token.toInt64(maxTokensDeposited),
                    ),
                    Expression.StringExpression(
                      Tezos.Timestamp.toString(deadline),
                    ),
                  ]),
                  None,
                ),
              ]),
              None,
            ),
          ]),
          None,
        ),
      ]),
      None,
    )
  );

/** encodeRemoveLiquidity
 *
 * liquidityBurned
 * minMutezWithdrawn
 * minTokenWithdrawn
 * Left (Right (Pair (Pair 100 1) (Pair 1 "2020-06-29T18:00:21Z")))
 */
/** encodeTezToToken */
/** encodeTokenToTez */
let encodeRemoveLiquidity =
    (
      liquidityBurned: Tezos.Token.t,
      minMutezWithdrawn: Tezos.Mutez.t,
      minTokenWithdrawn: Tezos.Token.t,
      deadline: Tezos.Timestamp.t,
    ) =>
  Tezos.(
    Expression.ContractExpression(
      Primitives.PrimitiveData(PrimitiveData.Left),
      Some([
        Expression.ContractExpression(
          Primitives.PrimitiveData(PrimitiveData.Right),
          Some([
            Expression.ContractExpression(
              Primitives.PrimitiveData(PrimitiveData.Pair),
              Some([
                Expression.ContractExpression(
                  Primitives.PrimitiveData(PrimitiveData.Pair),
                  Some([
                    Expression.IntExpression(
                      Tezos.Token.toInt64(liquidityBurned),
                    ),
                    Expression.IntExpression(
                      Tezos.Mutez.toInt64(minMutezWithdrawn),
                    ),
                  ]),
                  None,
                ),
                Expression.ContractExpression(
                  Primitives.PrimitiveData(PrimitiveData.Pair),
                  Some([
                    Expression.IntExpression(
                      Tezos.Token.toInt64(minTokenWithdrawn),
                    ),
                    Expression.StringExpression(
                      Tezos.Timestamp.toString(deadline),
                    ),
                  ]),
                  None,
                ),
              ]),
              None,
            ),
          ]),
          None,
        ),
      ]),
      None,
    )
  );

/**
 * minTokensRequired
 * deadline
 * Right (Left (Pair <min-tokens-required> "<deadline>"))
 * tezToToken
*/

let encodeMutezToTokens =
    (minTokensRequired: Tezos.Token.t, deadline: Tezos.Timestamp.t) =>
  Tezos.(
    Expression.ContractExpression(
      Primitives.PrimitiveData(PrimitiveData.Right),
      Some([
        Expression.ContractExpression(
          Primitives.PrimitiveData(PrimitiveData.Left),
          Some([
            Expression.ContractExpression(
              Primitives.PrimitiveData(PrimitiveData.Pair),
              Some([
                Expression.IntExpression(
                  Tezos.Token.toInt64(minTokensRequired),
                ),
                Expression.StringExpression(
                  Tezos.Timestamp.toString(deadline),
                ),
              ]),
              None,
            ),
          ]),
          None,
        ),
      ]),
      None,
    )
  );

/**
 * tokenToTez
 * Right (Right (Left (Pair <tokens-sold> (Pair <min-tez-required> "<deadline>"))))
 */

let encodeTokensToMutez =
    (
      tokensSold: Tezos.Token.t,
      minMutezRequired: Tezos.Mutez.t,
      deadline: Tezos.Timestamp.t,
    ) =>
  Tezos.(
    Expression.ContractExpression(
      Primitives.PrimitiveData(PrimitiveData.Right),
      Some([
        Expression.ContractExpression(
          Primitives.PrimitiveData(PrimitiveData.Right),
          Some([
            Expression.ContractExpression(
              Primitives.PrimitiveData(PrimitiveData.Left),
              Some([
                Expression.ContractExpression(
                  Primitives.PrimitiveData(PrimitiveData.Pair),
                  Some([
                    Expression.IntExpression(
                      Tezos.Token.toInt64(tokensSold),
                    ),
                    Expression.ContractExpression(
                      Primitives.PrimitiveData(PrimitiveData.Pair),
                      Some([
                        Expression.IntExpression(
                          Tezos.Mutez.toInt64(minMutezRequired),
                        ),
                        Expression.StringExpression(
                          Tezos.Timestamp.toString(deadline),
                        ),
                      ]),
                      None,
                    ),
                  ]),
                  None,
                ),
              ]),
              None,
            ),
          ]),
          None,
        ),
      ]),
      None,
    )
  );

let addLiquidity =
    (
      contract: Tezos.ContractId.t,
      mutez: Tezos.Mutez.t,
      minLiquidityMinted: Tezos.Token.t,
      maxTokensDeposited: Tezos.Token.t,
      deadline: Tezos.Timestamp.t,
    )
    : Js.Promise.t(TezBridge.transactionResponse) =>
  TezBridge.postTransaction(
    contract,
    mutez,
    encodeAddLiquidity(minLiquidityMinted, maxTokensDeposited, deadline),
  );

let removeLiquidity =
    (
      contract: Tezos.ContractId.t,
      liquidityBurned: Tezos.Token.t,
      minMutezWithdrawn: Tezos.Mutez.t,
      minTokenWithdrawn: Tezos.Token.t,
      deadline: Tezos.Timestamp.t,
    )
    : Js.Promise.t(TezBridge.transactionResponse) =>
  TezBridge.postTransaction(
    contract,
    Tezos.Mutez.zero,
    encodeRemoveLiquidity(
      liquidityBurned,
      minMutezWithdrawn,
      minTokenWithdrawn,
      deadline,
    ),
  );

let mutezToTokens =
    (
      contract: Tezos.ContractId.t,
      mutez: Tezos.Mutez.t,
      minTokensRequired: Tezos.Token.t,
      deadline: Tezos.Timestamp.t,
    )
    : Js.Promise.t(TezBridge.transactionResponse) =>
  TezBridge.postTransaction(
    contract,
    mutez,
    encodeMutezToTokens(minTokensRequired, deadline),
  );

let tokensToMutez =
    (
      contract: Tezos.ContractId.t,
      tokensSold: Tezos.Token.t,
      minMutezRequired: Tezos.Mutez.t,
      deadline: Tezos.Timestamp.t,
    )
    : Js.Promise.t(TezBridge.transactionResponse) =>
  TezBridge.postTransaction(
    contract,
    Tezos.Mutez.zero,
    encodeTokensToMutez(tokensSold, minMutezRequired, deadline),
  );