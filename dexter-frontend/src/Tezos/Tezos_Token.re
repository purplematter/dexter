type t =
  | Token(Int64.t);

let encode = (t: t) : Js.Json.t =>
  switch (t) {
  | Token(m) => Json.Encode.string(Int64.to_string(m))
  };

let decode = (json: Js.Json.t) =>
  switch (Json.Decode.string(json)) {
  | v =>
    switch (Tezos_Util.int64OfString(v)) {
    | Some(int) =>
      if (Int64.compare(int, Int64.zero) >= 0) {
        Belt.Result.Ok(Token(int));
      } else {
        Belt.Result.Error("Token.decode failed: " ++ v);
      }
    | None => Belt.Result.Error("Token.decode failed: " ++ v)
    }
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Token.decode failed: " ++ error)
  };

let ofInt64 = (int: Int64.t) : Belt.Result.t(t, string) =>
  if (Int64.compare(int, Int64.zero) >= 0) {
    Belt.Result.Ok(Token(int));
  } else {
    Belt.Result.Error("ofInt64 expected a non-negative int64 value.");
  };

let ofString = (string: string) : Belt.Result.t(t, string) =>
switch (Tezos_Util.int64OfString(string)) {
| Some(int64) => switch (ofInt64(int64)) {
| Belt.Result.Ok(token) => Belt.Result.Ok(token)
| Belt.Result.Error(_error) => Belt.Result.Error("ofString expected a non-negative int64 value.")
}

| None => Belt.Result.Error("ofString expected a non-negative int64 encoded string.");
};
  


let toString = (t: t) : string =>
  switch (t) {
  | Token(int) => Int64.to_string(int)
  };

let toInt64 = (t: t) : Int64.t =>
  switch (t) {
  | Token(int) => int
  };

let zero = Token(Int64.zero);
let one = Token(Int64.one);

let add = (x, y) =>
  switch (x, y) {
  | (Token(x), Token(y)) => Token(Int64.add(x, y))
  };

let sub = (x, y) =>
  switch (x, y) {
  | (Token(x), Token(y)) => Token(Int64.sub(x, y))
  };

let mul = (x, y) =>
  switch (x, y) {
  | (Token(x), Token(y)) => Token(Int64.mul(x, y))
  };