type t =
  | Mutez(Int64.t);

let encode = (t: t) : Js.Json.t =>
  switch (t) {
  | Mutez(m) => Json.Encode.string(Int64.to_string(m))
  };

let decode = (json: Js.Json.t) =>
  switch (Json.Decode.string(json)) {
  | v =>
    switch (Tezos_Util.int64OfString(v)) {
    | Some(int) =>
      if (Int64.compare(int, Int64.zero) >= 0) {
        Belt.Result.Ok(Mutez(int));
      } else {
        Belt.Result.Error("Mutez.decode failed: " ++ v);
      }
    | None => Belt.Result.Error("Mutez.decode failed: " ++ v)
    }
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Mutez.decode failed: " ++ error)
  };

let ofInt64 = (int: Int64.t) : Belt.Result.t(t, string) =>
  if (Int64.compare(int, Int64.zero) >= 0) {
    Belt.Result.Ok(Mutez(int));
  } else {
    Belt.Result.Error("Mutez.mk");
  };

let ofInt = (int: int) : Belt.Result.t(t, string) =>
  if (int >= 0) {
    ofInt64(Int64.of_int(int))
  } else {
    Belt.Result.Error("ofInt: Expected non-negative int.");
  };

let ofString = (string: string) : Belt.Result.t(t, string) =>
  switch (Tezos_Util.int64OfString(string)) {
  | Some(int64) =>
    switch (ofInt64(int64)) {
    | Belt.Result.Ok(mutez) => Belt.Result.Ok(mutez)
    | Belt.Result.Error(_error) =>
      Belt.Result.Error("ofString: expected a non-negative int64 value.")
    }
  | None =>
    Belt.Result.Error(
      "ofString: expected a non-negative int64 value encoded as a string.",
    )
  };

let ofTezString = (string: string) : Belt.Result.t(t, string) => 
  switch (Tezos_Util.floatOfString(string)) {
  | Some(float) => {
    let pos = Tezos_Util.getPositionOfLastDecimalDigit(string);
    if (pos <= Some(6)) {
    let mutezFloat = float *. 1000000.;
    
    let int64 = Int64.of_float(mutezFloat);
    switch (ofInt64(int64)){
      | Belt.Result.Ok(mutez) => Belt.Result.Ok(mutez)
      | Belt.Result.Error(_error) =>
        Belt.Result.Error("ofString: expected a non-negative int64 value.")
      } 
    } else {
      Belt.Result.Error("ofTezString: expected a non-negative float with precision up to 10^-6, received: " ++ string);
    }
  }
  | None => Belt.Result.Error("ofTezString: expected a non-negative float with precision up to 10^-6")
  };

let toInt64 = (t: t) : Int64.t =>
  switch (t) {
  | Mutez(int) => int
  };

let toString = (t: t) : string =>
  switch (t) {
  | Mutez(int) => Int64.to_string(int)
  };
let zero = Mutez(Int64.zero);
let one = Mutez(Int64.one);

let oneTez = Mutez(1000000L);

let add = (x, y) =>
  switch (x, y) {
  | (Mutez(x), Mutez(y)) => Mutez(Int64.add(x, y))
  };

let sub = (x, y) =>
  switch (x, y) {
  | (Mutez(x), Mutez(y)) => Mutez(Int64.sub(x, y))
  };

let mul = (x, y) =>
  switch (x, y) {
  | (Mutez(x), Mutez(y)) => Mutez(Int64.mul(x, y))
  };