type t = {
  parameter: Tezos_Expression.t,
  storage: Tezos_Expression.t,
  code: Tezos_Expression.t,
};

let encode = (contract: t) : Js.Json.t => {
  let parameter =
    Tezos_Expression.ContractExpression(
      Tezos_Primitives.PrimitiveType(Tezos_PrimitiveType.Parameter),
      Some([contract.parameter]),
      None,
    );
  let storage =
    Tezos_Expression.ContractExpression(
      Tezos_Primitives.PrimitiveType(Tezos_PrimitiveType.Storage),
      Some([contract.storage]),
      None,
    );
  let code =
    Tezos_Expression.ContractExpression(
      Tezos_Primitives.PrimitiveType(Tezos_PrimitiveType.Code),
      Some([contract.code]),
      None,
    );
  Json.Encode.list(Tezos_Expression.encode, [parameter, storage, code]);
};

let isParameter = (expression: Tezos_Expression.t) : bool =>
  switch (expression) {
  | Tezos_Expression.ContractExpression(prim, _, _) =>
    switch (prim) {
    | Tezos_Primitives.PrimitiveType(Tezos_PrimitiveType.Parameter) => true
    | _ => false
    }
  | _ => false
  };

let isStorage = (expression: Tezos_Expression.t) : bool =>
  switch (expression) {
  | Tezos_Expression.ContractExpression(prim, _, _) =>
    switch (prim) {
    | Tezos_Primitives.PrimitiveType(Tezos_PrimitiveType.Storage) => true
    | _ => false
    }
  | _ => false
  };

let isCode = (expression: Tezos_Expression.t) : bool =>
  switch (expression) {
  | Tezos_Expression.ContractExpression(prim, _, _) =>
    switch (prim) {
    | Tezos_Primitives.PrimitiveType(Tezos_PrimitiveType.Code) => true
    | _ => false
    }
  | _ => false
  };

let getFirstArg =
    (expression: Tezos_Expression.t)
    : Belt.Result.t(Tezos_Expression.t, string) =>
  switch (expression) {
  | Tezos_Expression.ContractExpression(_, oArgs, _) =>
    switch (oArgs) {
    | Some(args) =>
      switch (Belt.List.head(args)) {
      | Some(arg) => Belt.Result.Ok(arg)
      | None =>
        Belt.Result.Error(
          "getFirstArg: args is unexpectedly an empty list. It should have one element.",
        )
      }
    | None =>
      Belt.Result.Error(
        "getFirstArg: args is unexpectedly None. It should be a list of one element.",
      )
    }
  | _ => Belt.Result.Error("getFirstArg: expected a ContractExpression")
  };

let getParameter =
    (expression: Tezos_Expression.t)
    : Belt.Result.t(Tezos_Expression.t, string) =>
  if (isParameter(expression)) {
    getFirstArg(expression);
  } else {
    Belt.Result.Error("getParameter: ");
  };

let getStorage =
    (expression: Tezos_Expression.t)
    : Belt.Result.t(Tezos_Expression.t, string) =>
  if (isStorage(expression)) {
    getFirstArg(expression);
  } else {
    Belt.Result.Error("getStorage: ");
  };

let getCode =
    (expression: Tezos_Expression.t)
    : Belt.Result.t(Tezos_Expression.t, string) =>
  if (isCode(expression)) {
    getFirstArg(expression);
  } else {
    Belt.Result.Error("getCode: ");
  };

let decode = (json: Js.Json.t) : Belt.Result.t(t, string) =>
  switch (
    Json.Decode.tuple3(
      a => Tezos_Util.unwrapResult(Tezos_Expression.decode(a)),
      a => Tezos_Util.unwrapResult(Tezos_Expression.decode(a)),
      a => Tezos_Util.unwrapResult(Tezos_Expression.decode(a)),
      json,
    )
  ) {
  | (a, b, c) =>
    switch (getParameter(a), getStorage(b), getCode(c)) {
    | (
        Belt.Result.Ok(parameter),
        Belt.Result.Ok(storage),
        Belt.Result.Ok(code),
      ) =>
      Belt.Result.Ok({parameter, storage, code})
    | _ => Belt.Result.Error("unmatched code expressions")
    }
  | exception (Json.Decode.DecodeError(error)) => Belt.Result.Error(error)
  };