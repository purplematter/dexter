let unwrapResult = r =>
  switch (r) {
  | Belt.Result.Ok(v) => v
  | Belt.Result.Error(message) => raise @@ Json.Decode.DecodeError(message)
  };

let floatOfString = (x: string) : option(float) =>
  try (Some(float_of_string(x))) {
  | Failure(_) => None
  };

let int64OfString = (x: string) : option(Int64.t) =>
  try (Some(Int64.of_string(x))) {
  | Failure(_) => None
  };

let explode = s => {
  let rec exp = (i, l) =>
    if (i < 0) {
      l;
    } else {
      exp(i - 1, [s.[i], ...l]);
    };
  exp(String.length(s) - 1, []);
};

let getPositionOfLastDecimalDigit = (floatString: string) : option(int) =>
  switch (floatOfString(floatString)) {
  | None => None

  | Some(_float) =>
    let split = Js.String.split(".", floatString);
    if (Array.length(split) == 0) {
      None;
    } else if (Array.length(split) == 1) {
      Some(0);
    } else if (Array.length(split) == 2) {
      let length = Js.String.length(split[1]);      
      Some(length);
    } else {
      None;
    };
  };

let getPositionOfSmallestNonZero = (float: float) : int => {
  let floatString = Js.Float.toString(float);
  let split = Js.String.split(".", floatString);
  if (Array.length(split) == 1) {
    0
  } else if (Array.length(split) == 2) {
    let str = split[1];
    let chars = explode(str);
    let length = List.length(chars);
    let charsRev = List.rev(chars);

    let (i, _b) =
      List.fold_left(
        ((l, finished), char) =>
          if (finished) {
            (l, finished);
          } else if (char == '0') {
            (l - 1, false);
          } else {
            (l, true);
          },
        (length, false),
        charsRev,
      );
    i
  } else {
    0
  };
}