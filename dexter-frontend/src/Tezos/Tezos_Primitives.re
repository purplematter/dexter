type t =
  | PrimitiveInstruction(Tezos_PrimitiveInstruction.t)
  | PrimitiveData(Tezos_PrimitiveData.t)
  | PrimitiveType(Tezos_PrimitiveType.t);

let encode = (primitives: t) : Js.Json.t =>
  switch (primitives) {
  | PrimitiveInstruction(primitiveInstruction) =>
    Tezos_PrimitiveInstruction.encode(primitiveInstruction)
  | PrimitiveData(primitiveData) => Tezos_PrimitiveData.encode(primitiveData)
  | PrimitiveType(primitiveType) => Tezos_PrimitiveType.encode(primitiveType)
  };

let decode = (json: Js.Json.t) : Belt.Result.t(t, string) =>
  switch (Tezos_PrimitiveInstruction.decode(json)) {
  | Belt.Result.Ok(primitiveInstruction) =>
    Belt.Result.Ok(PrimitiveInstruction(primitiveInstruction))
  | Belt.Result.Error(_) =>
    switch (Tezos_PrimitiveData.decode(json)) {
    | Belt.Result.Ok(primitiveData) =>
      Belt.Result.Ok(PrimitiveData(primitiveData))
    | Belt.Result.Error(_) =>
      switch (Tezos_PrimitiveType.decode(json)) {
      | Belt.Result.Ok(primitiveType) =>
        Belt.Result.Ok(PrimitiveType(primitiveType))
      | Belt.Result.Error(error) =>
        Belt.Result.Error("Primitives.decode: " ++ error)
      | exception (Json.Decode.DecodeError(error)) =>
        Belt.Result.Error("Primitives.decode: " ++ error)
      }
    | exception (Json.Decode.DecodeError(error)) =>
      Belt.Result.Error("Primitives.decode: " ++ error)
    }
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Primitives.decode: " ++ error)
  };