/* module type Serializable = {
     let encode: t => Js.Json.t;
     let decode: Js.Json.t => Belt.Result.t(t, string);
   }; */

module Timestamp = Tezos_Timestamp;

module ContractId = Tezos_ContractId;

module Mutez = Tezos_Mutez;

module Token = Tezos_Token;

module PrimitiveInstruction = Tezos_PrimitiveInstruction;

module PrimitiveData = Tezos_PrimitiveData;

module PrimitiveType = Tezos_PrimitiveType;

module Primitives = Tezos_Primitives;

module Expression = Tezos_Expression;

module Code = Tezos_Code;