type t =
  | IntExpression(int64)
  | StringExpression(string)
  | BytesExpression(string)
  | CodeExpression(list(t))
  | ContractExpression(
      Tezos_Primitives.t /*** prim */,
      option(list(t)) /*** args */,
      option(list(string)) /*** annots */,
    );

let rec encode = (expression: t) : Js.Json.t =>
  switch (expression) {
  | IntExpression(i) =>
    Json.Encode.(object_([("int", string(Int64.to_string(i)))]))
  | StringExpression(str) =>
    Json.Encode.(object_([("string", string(str))]))
  | BytesExpression(bytes) =>
    Json.Encode.(object_([("bytes", string(bytes))]))
  | CodeExpression(expressions) => Json.Encode.list(encode, expressions)
  | ContractExpression(prim, args, annots) =>
    let argsEncoded =
      switch (args) {
      | Some(args) => [("args", Json.Encode.list(encode, args))]
      | None => []
      };
    let annotsEncoded =
      switch (annots) {
      | Some(annots) => [
          ("annots", Json.Encode.list(Json.Encode.string, annots)),
        ]
      | None => []
      };
    Json.Encode.(
      object_(
        List.concat([
          [("prim", Tezos_Primitives.encode(prim))],
          argsEncoded,
          annotsEncoded,
        ]),
      )
    );
  };

let intOfStringOpt = x =>
  try (Some(int_of_string(x))) {
  | Failure(_) => None
  };

let int64OfStringOpt = x =>
  try (Some(Int64.of_string(x))) {
  | Failure(_) => None
  };

let rec decode = (json: Js.Json.t) : Belt.Result.t(t, string) =>
  Json.Decode.(
    switch (list(a => Tezos_Util.unwrapResult(decode(a)), json)) {
    | v => Belt.Result.Ok(CodeExpression(v))
    | exception (DecodeError(_error)) =>
      switch (field("int", string, json)) {
      | v =>
        switch (int64OfStringOpt(v)) {
        | Some(i) => Belt.Result.Ok(IntExpression(i))
        | None => Belt.Result.Error("Expected string encoded int.")
        }
      | exception (DecodeError(_error)) =>
        switch (field("string", string, json)) {
        | v => Belt.Result.Ok(StringExpression(v))
        | exception (DecodeError(_error)) =>
          switch (field("bytes", string, json)) {
          | v => Belt.Result.Ok(BytesExpression(v))
          | exception (DecodeError(_error)) =>
            switch (
              field(
                "prim",
                a => Tezos_Util.unwrapResult(Tezos_Primitives.decode(a)),
                json,
              ),
              optional(
                field(
                  "args",
                  list(a => Tezos_Util.unwrapResult(decode(a))),
                ),
                json,
              ),
              optional(field("annots", list(string)), json),
            ) {
            | (prim, args, annots) =>
              Belt.Result.Ok(ContractExpression(prim, args, annots))
            | exception (DecodeError(error)) => Belt.Result.Error(error)
            }
          }
        }
      }
    }
  );