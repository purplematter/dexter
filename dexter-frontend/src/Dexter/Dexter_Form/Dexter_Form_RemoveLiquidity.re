type state = {
  liquidityBurned: Dexter_Valid.t(Tezos.Token.t),
  minMutezWithdrawn: Dexter_Valid.t(Tezos.Mutez.t),
  minTokensWithdrawn: Dexter_Valid.t(Tezos.Token.t),
};

type action =
  | Update(state);

[@react.component]
let make =
    (
      ~address,
      ~dexterContract,
      ~onChange: (Dexter_Transaction.t, TezBridge.transactionResponse) => unit,
    ) => {
  let (state, dispatch) =
    React.useReducer(
      (_state, action) =>
        switch (action) {
        | Update(state) => state
        },
      {
        liquidityBurned: Dexter_Valid.Empty,
        minMutezWithdrawn: Dexter_Valid.Empty,
        minTokensWithdrawn: Dexter_Valid.Empty,
      },
    );

  <form>
    <p>
      <label>
        (ReasonReact.string("Amount of Liquidity Tokens You Want to Burn"))
      </label>
      <br />
      <CommonUi.NumericInput.TokenInput
        initialValue=None
        numValue=(Dexter_Valid.toOption(state.liquidityBurned))
        onChange=(
          token =>
            dispatch(
              Update({...state, liquidityBurned: Dexter_Valid.Ok(token)}),
            )
        )
        onError=(
          () =>
            dispatch(Update({...state, liquidityBurned: Dexter_Valid.Error}))
        )
        onEmpty=(
          () =>
            dispatch(Update({...state, liquidityBurned: Dexter_Valid.Empty}))
        )
        style=(Dexter_Valid.toStyle(state.liquidityBurned))
      />
    </p>
    <p>
      <label>
        (
          ReasonReact.string(
            "Minimum Amount of Liquidity You Want to Withdraw",
          )
        )
      </label>
      <br />
      <CommonUi.NumericInput.MutezInput
        initialValue=None
        numValue=(Dexter_Valid.toOption(state.minMutezWithdrawn))
        onChange=(
          mutez =>
            dispatch(
              Update({...state, minMutezWithdrawn: Dexter_Valid.Ok(mutez)}),
            )
        )
        onError=(
          () =>
            dispatch(
              Update({...state, minMutezWithdrawn: Dexter_Valid.Error}),
            )
        )
        onEmpty=(
          () =>
            dispatch(
              Update({...state, minMutezWithdrawn: Dexter_Valid.Empty}),
            )
        )
        style=(Dexter_Valid.toStyle(state.minMutezWithdrawn))
      />
    </p>
    <p>
      <label>
        (ReasonReact.string("Minimum Amount of Tokens You Want to Withdraw"))
      </label>
      <br />
      <CommonUi.NumericInput.TokenInput
        initialValue=None
        numValue=(Dexter_Valid.toOption(state.minTokensWithdrawn))
        onChange=(
          token =>
            dispatch(
              Update({...state, minTokensWithdrawn: Dexter_Valid.Ok(token)}),
            )
        )
        onError=(
          () =>
            dispatch(
              Update({...state, minTokensWithdrawn: Dexter_Valid.Error}),
            )
        )
        onEmpty=(
          () =>
            dispatch(
              Update({...state, minTokensWithdrawn: Dexter_Valid.Empty}),
            )
        )
        style=(Dexter_Valid.toStyle(state.minTokensWithdrawn))
      />
    </p>
    <p>
      <button
        type_="button"
        disabled=(
          Belt.Option.isNone(address)
          || Dexter_Valid.notOk(state.liquidityBurned)
          || Dexter_Valid.notOk(state.minMutezWithdrawn)
          || Dexter_Valid.notOk(state.minTokensWithdrawn)
        )
        onClick=(
          _event =>
            switch (
              state.liquidityBurned,
              state.minMutezWithdrawn,
              state.minTokensWithdrawn,
            ) {
            | (
                Dexter_Valid.Ok(liquidityBurned),
                Dexter_Valid.Ok(minMutezWithdrawn),
                Dexter_Valid.Ok(minTokensWithdrawn),
              ) =>
              DexterContract.removeLiquidity(
                dexterContract,
                liquidityBurned,
                minMutezWithdrawn,
                minTokensWithdrawn,
                Tezos.Timestamp.ofString("2020-06-29T18:00:21Z"),
              )
              |> Js.Promise.then_((res: TezBridge.transactionResponse) => {
                   onChange(RemoveLiquidity, res);
                   Js.Console.log("removeLiquidity: ");
                   Js.Console.log(res);
                   Js.Promise.resolve();
                 })
              |> ignore

            | _ => ()
            }
        )>
        (ReasonReact.string("Remove Liquidity"))
      </button>
    </p>
  </form>;
};