type state = {
  mutez: Dexter_Valid.t(Tezos.Mutez.t),
  minTokensRequired: Dexter_Valid.t(Tezos.Token.t),
};

type action =
  | Update(state);

[@react.component]
let make =
    (
      ~address,
      ~dexterContract,
      ~onChange: (Dexter_Transaction.t, TezBridge.transactionResponse) => unit,
    ) => {
  let (state, dispatch) =
    React.useReducer(
      (_state, action) =>
        switch (action) {
        | Update(state) => state
        },
      {mutez: Dexter_Valid.Empty, minTokensRequired: Dexter_Valid.Empty},
    );
  <form>
    <p>
      <label> {ReasonReact.string("Amount of Tez You Want to Sell")} </label>
      <br />
      <CommonUi.NumericInput.MutezInput
        initialValue=None
        numValue={Dexter_Valid.toOption(state.mutez)}
        onChange={mutez =>
          dispatch(Update({...state, mutez: Dexter_Valid.Ok(mutez)}))
        }
        onError={() =>
          dispatch(Update({...state, mutez: Dexter_Valid.Error}))
        }
        onEmpty={() =>
          dispatch(Update({...state, mutez: Dexter_Valid.Empty}))
        }
        style={Dexter_Valid.toStyle(state.mutez)}
      />
    </p>
    <p>
      <label>
        {ReasonReact.string("Minimum Amount of Tokens You Want to Buy")}
      </label>
      <br />
      <CommonUi.NumericInput.TokenInput
        initialValue=None
        numValue={Dexter_Valid.toOption(state.minTokensRequired)}
        onChange={token =>
          dispatch(
            Update({...state, minTokensRequired: Dexter_Valid.Ok(token)}),
          )
        }
        onError={() =>
          dispatch(Update({...state, minTokensRequired: Dexter_Valid.Error}))
        }
        onEmpty={() =>
          dispatch(Update({...state, minTokensRequired: Dexter_Valid.Empty}))
        }
        style={Dexter_Valid.toStyle(state.minTokensRequired)}
      />
    </p>
    <p>
      <button
        type_="button"
        disabled={
          Belt.Option.isNone(address)
          || Dexter_Valid.notOk(state.mutez)
          || Dexter_Valid.notOk(state.minTokensRequired)
        }
        onClick={_event =>
          switch (state.mutez, state.minTokensRequired) {
          | (Dexter_Valid.Ok(mutez), Dexter_Valid.Ok(tokens)) =>
            DexterContract.mutezToTokens(
              dexterContract,
              mutez,
              tokens,
              Tezos.Timestamp.ofString("2020-06-29T18:00:21Z"),
            )
            |> Js.Promise.then_((res: TezBridge.transactionResponse) => {
                 onChange(MutezToTokens, res);
                 Js.Console.log("mutezToTokens: ");
                 Js.Console.log(res);
                 Js.Promise.resolve();
               })
            |> ignore

          | _ => ()
          }
        }>
        {ReasonReact.string("Buy Tokens with Tez")}
      </button>
    </p>
  </form>;
};