type state = {
  mutez: Dexter_Valid.t(Tezos.Mutez.t),
  minLiquidityMinted: Dexter_Valid.t(Tezos.Token.t),
  maxTokensDeposited: Dexter_Valid.t(Tezos.Token.t),
};

type action =
  | Update(state);

[@react.component]
let make =
    (
      ~address,
      ~dexterContract,
      ~onChange: (Dexter_Transaction.t, TezBridge.transactionResponse) => unit,
    ) => {
  let (state, dispatch) =
    React.useReducer(
      (_state, action) =>
        switch (action) {
        | Update(state) => state
        },
      {
        mutez: Dexter_Valid.Empty,
        minLiquidityMinted: Dexter_Valid.Empty,
        maxTokensDeposited: Dexter_Valid.Empty,
      },
    );

  <form>
    <p>
      <label>
        {ReasonReact.string("Amount of Tez (XTZ) You Want to Deposit")}
      </label>
      <br />
      <CommonUi.NumericInput.MutezInput
        initialValue=None
        numValue={Dexter_Valid.toOption(state.mutez)}
        onChange={mutez =>
          dispatch(Update({...state, mutez: Dexter_Valid.Ok(mutez)}))
        }
        onError={() =>
          dispatch(Update({...state, mutez: Dexter_Valid.Error}))
        }
        onEmpty={() =>
          dispatch(Update({...state, mutez: Dexter_Valid.Empty}))
        }
        style={Dexter_Valid.toStyle(state.mutez)}
      />
    </p>
    <p>
      <label>
        {ReasonReact.string("Minimum Amount of Liquidity You Want to Mint")}
      </label>
      <br />
      <CommonUi.NumericInput.TokenInput
        initialValue=None
        numValue={Dexter_Valid.toOption(state.minLiquidityMinted)}
        onChange={token =>
          dispatch(
            Update({...state, minLiquidityMinted: Dexter_Valid.Ok(token)}),
          )
        }
        onError={() =>
          dispatch(
            Update({...state, minLiquidityMinted: Dexter_Valid.Error}),
          )
        }
        onEmpty={() =>
          dispatch(
            Update({...state, minLiquidityMinted: Dexter_Valid.Empty}),
          )
        }
        style={Dexter_Valid.toStyle(state.minLiquidityMinted)}
      />
    </p>
    <p>
      <label>
        {ReasonReact.string("Maximum Amount of Tokens You Want to Deposit")}
      </label>
      <br />
      <CommonUi.NumericInput.TokenInput
        initialValue=None
        numValue={Dexter_Valid.toOption(state.maxTokensDeposited)}
        onChange={token =>
          dispatch(
            Update({...state, maxTokensDeposited: Dexter_Valid.Ok(token)}),
          )
        }
        onError={() =>
          dispatch(
            Update({...state, maxTokensDeposited: Dexter_Valid.Error}),
          )
        }
        onEmpty={() =>
          dispatch(
            Update({...state, maxTokensDeposited: Dexter_Valid.Empty}),
          )
        }
        style={Dexter_Valid.toStyle(state.maxTokensDeposited)}
      />
    </p>
    <p>
      <button
        type_="button"
        disabled={
          Belt.Option.isNone(address)
          || Dexter_Valid.notOk(state.mutez)
          || Dexter_Valid.notOk(state.minLiquidityMinted)
          || Dexter_Valid.notOk(state.maxTokensDeposited)
        }
        onClick={_event =>
          switch (
            state.mutez,
            state.minLiquidityMinted,
            state.maxTokensDeposited,
          ) {
          | (
              Dexter_Valid.Ok(mutez),
              Dexter_Valid.Ok(minLiquidityMinted),
              Dexter_Valid.Ok(maxTokensDeposited),
            ) =>
            DexterContract.addLiquidity(
              dexterContract,
              mutez,
              minLiquidityMinted,
              maxTokensDeposited,
              Tezos.Timestamp.ofString("2020-06-29T18:00:21Z"),
            )
            |> Js.Promise.then_((res: TezBridge.transactionResponse) => {
                 onChange(AddLiquidity, res);
                 Js.Console.log("addLiquidity: ");
                 Js.Console.log(res);
                 Js.Promise.resolve();
               })
            |> ignore

          | _ => ()
          }
        }>
        {ReasonReact.string("Add Liquidity")}
      </button>
    </p>
  </form>;
};