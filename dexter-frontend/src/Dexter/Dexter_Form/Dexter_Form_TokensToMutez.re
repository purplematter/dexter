type state = {
  tokensSold: Dexter_Valid.t(Tezos.Token.t),
  minMutezRequired: Dexter_Valid.t(Tezos.Mutez.t),
};

type action =
  | Update(state);

[@react.component]
let make =
    (
      ~address,
      ~dexterContract,
      ~onChange: (Dexter_Transaction.t, TezBridge.transactionResponse) => unit,
    ) => {
  let (state: state, dispatch) =
    React.useReducer(
      (_state, action) =>
        switch (action) {
        | Update(state) => state
        },
      {tokensSold: Dexter_Valid.Empty, minMutezRequired: Dexter_Valid.Empty},
    );
  <form>
    <p>
      <label>
        {ReasonReact.string("Amount of Tokens You Want to Sell")}
      </label>
      <br />
      <CommonUi.NumericInput.TokenInput
        initialValue=None
        numValue={Dexter_Valid.toOption(state.tokensSold)}
        onChange={token =>
          dispatch(Update({...state, tokensSold: Dexter_Valid.Ok(token)}))
        }
        onError={() =>
          dispatch(Update({...state, tokensSold: Dexter_Valid.Error}))
        }
        onEmpty={() =>
          dispatch(Update({...state, tokensSold: Dexter_Valid.Empty}))
        }
        style={Dexter_Valid.toStyle(state.tokensSold)}
      />
    </p>
    <p>
      <label>
        {ReasonReact.string("Minimum Amount of Tez You Want to Buy")}
      </label>
      <br />
      <CommonUi.NumericInput.MutezInput
        initialValue=None
        numValue={Dexter_Valid.toOption(state.minMutezRequired)}
        onChange={mutez =>
          dispatch(
            Update({...state, minMutezRequired: Dexter_Valid.Ok(mutez)}),
          )
        }
        onError={() =>
          dispatch(Update({...state, minMutezRequired: Dexter_Valid.Empty}))
        }
      />
    </p>
    <p>
      <button
        type_="button"
        disabled={
          Belt.Option.isNone(address)
          || Dexter_Valid.notOk(state.tokensSold)
          || Dexter_Valid.notOk(state.minMutezRequired)
        }
        onClick={_event =>
          switch (state.tokensSold, state.minMutezRequired) {
          | (Dexter_Valid.Ok(tokensSold), Dexter_Valid.Ok(minMutezRequired)) =>
            DexterContract.tokensToMutez(
              dexterContract,
              tokensSold,
              minMutezRequired,
              Tezos.Timestamp.ofString("2020-06-29T18:00:21Z"),
            )
            |> Js.Promise.then_((res: TezBridge.transactionResponse) => {
                 onChange(TokensToMutez, res);
                 Js.Console.log("tokensToMutez: ");
                 Js.Console.log(res);
                 Js.Promise.resolve();
               })
            |> ignore

          | _ => ()
          }
        }>
        {ReasonReact.string("Buy Tez with Tokens")}
      </button>
    </p>
  </form>;
};