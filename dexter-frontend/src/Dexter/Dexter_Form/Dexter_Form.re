[@react.component]
let make =
    (
      ~address,
      ~dexterContract,
      ~dexterTransaction: Dexter_Transaction.t,
      ~onChange: (Dexter_Transaction.t, TezBridge.transactionResponse) => unit,
    ) =>
  switch (dexterTransaction) {
  | MutezToTokens =>
    <Dexter_Form_MutezToTokens address dexterContract onChange />

  | TokensToMutez =>
    <Dexter_Form_TokensToMutez address dexterContract onChange />

  | AddLiquidity =>
    <Dexter_Form_AddLiquidity address dexterContract onChange />

  | RemoveLiquidity =>
    <Dexter_Form_RemoveLiquidity address dexterContract onChange />
  };