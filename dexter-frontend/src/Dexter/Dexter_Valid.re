/** validation state of an input box */
type t('a) =
  | Empty
  | Error
  | Ok('a);

let toOption = (i: t('a)) : option('a) =>
  switch (i) {
  | Empty => None
  | Error => None
  | Ok(a) => Some(a)
  };

let ofOption = (o: option('a)) : t('a) =>
  switch (o) {
  | None => Error
  | Some(a) => Ok(a)
  };

let notOk = (i: t('a)) : bool =>
  switch (i) {
  | Empty => true
  | Error => true
  | Ok(_) => false
  };

let toStyle = (i: t('a)) : ReactDOMRe.Style.t =>
  switch (i) {
  | Empty => ReactDOMRe.Style.make()
  | Error => ReactDOMRe.Style.make(~borderColor="red", ())
  | Ok(_) => ReactDOMRe.Style.make(~borderColor="green", ())
  };