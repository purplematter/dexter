let eventToValue = event => ReactEvent.Form.target(event)##value;

let tzscanAlphanetUrl = "https://alphanet.tzscan.io/";

module ContractDropDown =
  CommonUi.DropDown.MakeDropDown({
    type t = (string, (Tezos.ContractId.t, Tezos.ContractId.t));
    let toString = ((title, _)) => title;
  });

type transaction = {
  dexterTransaction: Dexter_Transaction.t,
  transactionResponse: TezBridge.transactionResponse,
};

/* State declaration */
type state = {
  address: option(string),
  contract: (string, (Tezos.ContractId.t, Tezos.ContractId.t)),
  dexterTransaction: Dexter_Transaction.t,
  transactions: list(transaction),
};

/* Action declaration */
type action =
  | UpdateAddress(option(string))
  | UpdateContract((string, (Tezos.ContractId.t, Tezos.ContractId.t)))
  | UpdateDexterTransaction(Dexter_Transaction.t)
  | PrependTransactions(transaction);

[@react.component]
let make = (~contractMap: Dexter_ContractsMap.t) => {
  let contractList = Belt.Map.String.toList(contractMap);
  let contract = List.hd(contractList);
  let getExchangeContract = c => {
    let (_contractName, (contract, _)) = c;
    contract;
  };
  let (state: state, dispatch) =
    React.useReducer(
      (state: state, action) =>
        switch (action) {
        | UpdateAddress(address) => {...state, address}
        | UpdateContract(contract) => {...state, contract}
        | UpdateDexterTransaction(dexterTransaction) => {
            ...state,
            dexterTransaction,
          }
        | PrependTransactions(transaction) => {
            ...state,
            transactions: List.concat([[transaction], state.transactions]),
          }
        },
      {
        address: None,
        contract,
        dexterTransaction: Dexter_Transaction.MutezToTokens,
        transactions: [],
      },
    );

  let mkSelectButton = (i: int, t: Dexter_Transaction.t) =>
    <button
      className={Dexter_Style.buttonSelected(t == state.dexterTransaction)}
      key={string_of_int(i)}
      type_="button"
      onClick={_ => dispatch(UpdateDexterTransaction(t))}>
      {ReasonReact.string(Dexter_Transaction.toString(t))}
    </button>;
  <div>
    <h1> {ReasonReact.string("Dexter 0.1.0 (alphanet)")} </h1>
    <div className=Dexter_Style.topSpace>
      <h2> {ReasonReact.string("How to Use Dexter")} </h2>
      <a
        href="https://gitlab.com/camlcase-dev/dexter/blob/master/docs/dexter-frontend.md"
        target="_blank">
        {ReasonReact.string("Dexter Frontend Guide")}
      </a>
    </div>    
    <div className=Dexter_Style.topSpace>
      {switch (state.address) {
       | Some(address) =>
         <label>
           {ReasonReact.string("Your are logged in with: ")}
           <a href={tzscanAlphanetUrl ++ address} target="_blank">
             {ReasonReact.string(address)}
           </a>
         </label>
       | None =>
         <label>
           {ReasonReact.string(
              "Your Tezos address is needed before you can start trading, get it from tezbridge.",
            )}
         </label>
       }}
    </div>
    {if (Belt.Option.isNone(state.address)) {
       <button
         className={Css.merge([Dexter_Style.topSpace, Dexter_Style.signIn])}
         onClick={_event =>
           TezBridge.getSource()
           |> Js.Promise.then_(result => {
                dispatch(UpdateAddress(Some(result)));
                Js.Promise.resolve();
              })
           |> ignore
         }>
         {ReasonReact.string("Get Tezos address from Tezbridge")}
       </button>;
     } else {
       ReasonReact.null;
     }}
    <div className=Dexter_Style.topSpace>
      <label> {ReasonReact.string("Select Token:")} </label>
      <div>
        <ContractDropDown
          className=Dexter_Style.tokenSelector
          onChange={contract => dispatch(UpdateContract(contract))}
          selectedItem={state.contract}
          elements={Array.of_list(contractList)}
        />
      </div>
    </div>
    <div className=Dexter_Style.topSpace>
      <h2> {ReasonReact.string("Choose Dexter Transaction")} </h2>
      <div className=Dexter_Style.buttonGroup>
        {List.mapi(
           (i, t) => mkSelectButton(i, t),
           Dexter_Transaction.[
             MutezToTokens,
             TokensToMutez,
             AddLiquidity,
             RemoveLiquidity,
           ],
         )
         |> Array.of_list
         |> ReasonReact.array}
      </div>
      <div className=Dexter_Style.topSpace>
        <h3>
          {ReasonReact.string(
             Dexter_Transaction.toString(state.dexterTransaction),
           )}
        </h3>
      </div>
      <Dexter_Form
        address={state.address}
        dexterContract={getExchangeContract(state.contract)}
        dexterTransaction={state.dexterTransaction}
        onChange={(dexterTransaction, transactionResponse) =>
          dispatch(
            PrependTransactions({dexterTransaction, transactionResponse}),
          )
        }
      />
    </div>
    <div className=Dexter_Style.topSpace>
      <h2> {ReasonReact.string("Transactions")} </h2>
      <ul>
        {List.mapi(
           (i, transaction: transaction) =>
             <li key={string_of_int(i)}>
               <span>
                 {ReasonReact.string(
                    Dexter_Transaction.toString(
                      transaction.dexterTransaction,
                    ),
                  )}
               </span>
               <span> {ReasonReact.string(": ")} </span>
               <a
                 href={
                   tzscanAlphanetUrl
                   ++ TezBridge.operationId(transaction.transactionResponse)
                 }
                 target="_blank">
                 {ReasonReact.string(
                    TezBridge.operationId(transaction.transactionResponse),
                  )}
               </a>
             </li>,
           state.transactions,
         )
         |> Array.of_list
         |> ReasonReact.array}
      </ul>
    </div>
    <div className=Dexter_Style.topSpace>
      <h2> {ReasonReact.string("Contracts")} </h2>
      <ul>
        {List.map(
           ((title, (exchange, token))) =>
             [
               <li key={Tezos.ContractId.toString(exchange)}>
                 <a
                   href={
                     tzscanAlphanetUrl ++ Tezos.ContractId.toString(exchange)
                   }
                   target="_blank">
                   {ReasonReact.string(title ++ " Dexter Exchange")}
                 </a>
               </li>,
               <li key={Tezos.ContractId.toString(token)}>
                 <a
                   href={
                     tzscanAlphanetUrl ++ Tezos.ContractId.toString(token)
                   }
                   target="_blank">
                   {ReasonReact.string(title)}
                 </a>
               </li>,
             ],
           contractList,
         )
         |> List.concat
         |> Array.of_list
         |> ReasonReact.array}
      </ul>
    </div>
  </div>;
};

/**
 *

 tezosGoldExchange: KT1G2D45tpJ9f1iGVwQHqvupv2syhvMqeWPe

 ~/alphanet.sh client get script storage for tezosGoldExchange
 ~/alphanet.sh client get script storage for tezosGold
 ~/alphanet.sh client get big map value for '"tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH"' of type 'address' in tezosGoldExchange
 ~/alphanet.sh client get big map value for '"tz1PYgf9fBGLXvwx8ag8sdwjLJzmyGdNiswM"' of type 'address' in tezosGold

 // get alice's balance
~/alphanet.sh client get big map value for '"tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH"' of type 'address' in tezosGold

 https://alphanet.tzscan.io/KT1G2D45tpJ9f1iGVwQHqvupv2syhvMqeWPe

 https://alphanet.tzscan.io/tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH

~/alphanet.sh client get balance for alice
*/;
