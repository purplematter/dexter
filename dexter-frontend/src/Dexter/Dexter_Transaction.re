type t =
  | MutezToTokens
  | TokensToMutez
  | AddLiquidity
  | RemoveLiquidity;

let toString = t =>
  switch (t) {
  | MutezToTokens => "Tez (XTZ) to Tokens"
  | TokensToMutez => "Tokens to Tez (XTZ)"
  | AddLiquidity => "Add Liquidity"
  | RemoveLiquidity => "Remove Liquidity"
  };