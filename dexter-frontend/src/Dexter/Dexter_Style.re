// https://www.npmjs.com/package/bs-css
// https://github.com/SentiaAnalytics/bs-css/blob/HEAD/src/Css.rei
/* Open the Css module, so we can access the style properties below without prefixing them with Css. */
open Css;

let topSpace = style([marginTop(px(20))]);

let signIn =
  style([
    marginTop(px(5)),
    borderRadius(px(4)),
    backgroundColor(white),
    color(black),
    padding2(~v=px(10), ~h=px(10)),
    border(px(2), solid, hex("C0C0C0")),
  ]);

let tokenSelector =
  style([
    marginTop(px(5)),
    borderRadius(px(4)),
    backgroundColor(white),
    color(black),
    padding2(~v=px(5), ~h=px(5)),
    border(px(2), solid, hex("C0C0C0")),
  ]);

let buttonGroup =
  style([
    marginTop(px(5)),
    display(`inlineBlock),
    selector(
      "button",
      [
        backgroundColor(white),
        border(px(1), solid, grey),
        color(black),
        padding2(~v=px(10), ~h=px(24)),
        cursor(`pointer),
        Css.float(`left),
        /** pseudo class */
        after([unsafe("content", "\"\""), clear(both), display(table)]),
        hover([backgroundColor(hex("C0C0C0"))]),
        not_(":last-child", [unsafe("borderRight", "none")]),
        firstChild([
          borderTopLeftRadius(px(4)),
          borderBottomLeftRadius(px(4)),
        ]),
        lastChild([
          borderTopRightRadius(px(4)),
          borderBottomRightRadius(px(4)),
        ]),
      ],
    ),
  ]);

let buttonSelected = selected =>
  if (selected) {
    style([border(px(2), solid, blue)]);
  } else {
    style([opacity(0.6)]);
  };