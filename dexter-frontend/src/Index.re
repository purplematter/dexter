let tezosGold =
  Tezos.ContractId.ofString("KT1DmCHxit2bQ2GiHVc24McY6meuJPMTrqD8");
let tezosGoldExchange =
  Tezos.ContractId.ofString("KT1NNHerEYbKvUMAhMYikRCs2Aoym7HQLqPB");

let tezosSilver =
  Tezos.ContractId.ofString("KT1GutmbJNisDvN2XiuKLdoRHukSfh5bsUUc");
let tezosSilverExchange =
  Tezos.ContractId.ofString("KT1AecgjTUtHibSn64MbNQ4VBCNQELJweSZ4");

let tastyTacos =
  Tezos.ContractId.ofString("KT1AszhGevBf3EFP5EhxaR4i15oXpqzD5kaE");
let tastyTacosExchange =
  Tezos.ContractId.ofString("KT1Su9h3rAn6K66qucpN8954nrmjvBAoMmhp");

let checkers =
  Tezos.ContractId.ofString("KT1F3D8aAeiYA6EWu96AJhES1zRYJPm4UpiM");
let checkersExchange =
  Tezos.ContractId.ofString("KT1Rq4qVCkuDZQAE3YtoMQkk32dhMdZfwUcj");

let wrappedLTC =
  Tezos.ContractId.ofString("KT1AfhJ6kxvtqYMRJ2uzq3xSiH9cvHBENGSV");
let wrappedLTCExchange =
  Tezos.ContractId.ofString("KT19Mteifa4XT4PtwRS5AWYMj5fV2sv6getq");

let contractList = [
  ("Checkers", (checkersExchange, checkers)),
  ("Wrapped LTC", (wrappedLTCExchange, wrappedLTC)),
  ("Tasty Tacos", (tastyTacosExchange, tastyTacos)),
  ("Tezos Gold", (tezosGoldExchange, tezosGold)),
  ("Tezos Silver", (tezosSilverExchange, tezosSilver)),
];

let oContractMap = Dexter_ContractsMap.ofList(contractList);

switch (oContractMap) {
| Some((contractMap: Dexter_ContractsMap.t)) =>
  ReactDOMRe.renderToElementWithId(<Dexter contractMap />, "root")
| None => Js.Console.log("error")
};
