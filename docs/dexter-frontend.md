# Dexter Frontend

Dexter frontend is a web frontend that allows users to transactions on Dexter exchanges
(decentralized exchanges on Tezos) via [TezBridge](https://www.tezbridge.com/), 
a web wallet for Tezos that  stores data in the user's browser and allows for 
interaction with Tezos contracts.

This project is still in development. Only use this the zeronet or 
alphanet. Do not try this on the mainnet yet.

The alpha version of Dexter is available on the [camlCase website](https://camlcase.io/dexter.html).

# Overview

- Create an account on the alphanet
- Add alphanet account to TezBridge
- How to use Dexter

## Create an account on the alphanet

(You may skip this section if you are using an alphanet account on a Ledger Nano S.)

Before using the Dexter frontend, create a TezBridge account directly with a json file from 
the Tezos faucet or associate an alphanet account with TezBridge.

### Create an account directly from Tezos faucet

1. Go to https://faucet.tzalpha.net and get a faucet JSON file.
2. Copy the contents of the JSON file.
3. Open https://www.tezbridge.com.
4. Click Import Key and paste the entire JSON file into the text box. Enter
   a Manager name (TezBridge account name stored in your browser) and a 
   password, then press confirm.

Details from the TezBridge website: [Setup an alphanet account with Tezos faucet](https://docs.tezbridge.com/sample.html#step1-preparation).

### Add alphanet account

This option is very insecure and should never be performed on the mainnet.
For alphanet and zeronet the tez stored has no value and is for development purposes only.
Follow our tutorial: [Make an alphanet account](https://gitlab.com/camlcase-dev/michelson-tutorial/tree/master/02).

Now that you have an alphanet account, get its secret key.

```bash
$ ~/alphanet.sh client show address alice -S
```

You should see something like this:

```bash
Secret Key: unencrypted:edskzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
```

Copy the text after `unencrypted:`. Go to the 
[TezBridge](https://www.tezbridge.com) website. Click on `Import key`, paste 
your secret key in the box labeled 
`Secret key / Words / Encrypted key / Faucet`. If your secret key is correct, 
it should display a Public key hash and give you the option to create a 
`Manager name` and `Locking password`, do so, then click confirm.

Now you should see the manager name you just created under `Local managers`. 
Click on the name and you should see the associated Public key hash and XTZ
balance.

Here is a [screen animation](https://docs.tezbridge.com/user.html#importing-secret-key) 
that shows the process on TezBridge.

## Using the Dexter frontend

Click `Get Tezos address from TezBridge`. This will open and redirect you to 
a new tab with TezBridge. 

If you created an alphanet account or used a Tezos faucet directly in the browser:
Click on `Choose signer`, then `Local managers`, select the `manager`
you want, enter the password and click enter, then select the public key hash and
click `Use as signer`. 

If you have an alphanet account on a Ledger Nano S:
Click on `Hardware signer`, then `Ledger`. 

Now go back to the Dexter tab. You should see the public key hash of the signed
account.

Select one of the four Dexter transactions. Insert the values and if they are 
valid values, you can submit the transaction. Click the button below the form,
it will take you back to TezBridge. Click `DApp requests` and then click
`Approve`. You should see a spinning circle, when it it finished, click 
responses. You will see either `approved` or an error. 

Now go back to the Dexter tab. If the transaction was approved, you will
see it under transactions. Click on the operation link. If it has been
included on the blockchain, you should see the details, if it has not,
the page will appear invalid and you may need to wait a few minutes until it has
been included.

You can also confirm the validity of the action you just performed via Dexter by 
looking at the accounts and contracts in the command line (as per [dexter-cli.md](./dexter-cli.md))
or by following the tzscan links in to the accounts and contracts in Dexter.


# Run Dexter Locally

## Set Dexter contracts

In the editor of your choice, go to 
[dexter-frontend/src/Index.re](../dexter-frontend/src/Index.re). You should see
some variables like `tezosGold` and `tezosGoldExchange`. The first one is a
token contract and the second one is a Dexter exchange contract. Change the
values to match the ids of the contracts you originated according to the 
[dexter-cli.md](./dexter-cli.md) document. You can add as many token contracts 
and exchanges as you like.

In `contractList` you might want to update the contract names, these will be
displayed in the Dexter frontend.

## Compile and run Dexter frontend

### Compile

```bash
cd dexter-frontend
yarn
yarn build
```

### Run

```bash
PORT=8040 yarn server
```

Change the port number to whatever you like.
