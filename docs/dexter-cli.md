Dexter is a Tezos on-chain, decentralized exchange for the native Tezos token
`tez` and assets built on the Tezos block chain.

# Originating the Dexter Contracts from the Command Line

The Dexter contracts are written in Haskell using the `morley` library. There is
a `dexter-contract` executable that outputs the contracts in Michelson. We have 
committed the latest Michelson versions of the Dexter contracts to this 
repository so you do not need to compile and execute the Haskell code. The 
Michelson contracts are located at `dexter-contracts/contracts`.

We are going to discuss how to originate the contracts on the Tezos alphanet. 
This tutorial assumes you are familiar with the Tezos client. If you are not,
please read our 
[first](https://gitlab.com/camlcase-dev/michelson-tutorial/tree/master/01) 
and 
[second](https://gitlab.com/camlcase-dev/michelson-tutorial/tree/master/02) 
Michelson tutorials. They show you how to download the Tezos client, create 
accounts on the Alphanet, transfer tokens and originate contracts.

Dexter is currently composed of three contracts:

- A sample token contract.
- A broker contract for interaction between the main Dexter exchange contract and the sample token contract.
- The main Dexter exchange contract.

Clone the dexter repository and move to the main directory:

```bash
git clone git@gitlab.com:camlcase-dev/dexter.git
cd dexter/dexter-contracts
```

## Originating the Sample Token Contract

A standard Tezos token contract is still under development. The proposal can be
found [here](https://gitlab.com/tzip/tzip/blob/master/A/FA1.2.md). In order to 
create a proof of concept of the Dexter exchange, we have developed a simplified 
version of [ERC-20](https://en.wikipedia.org/wiki/ERC-20) for Tezos. This is 
not intended for real world use, just for development. When the standard is 
ready, Dexter will be updated to support it.

Now we are going to originate the token contract. First, lookup the address of
the account with which you want to originate the token and give ownership of 
the coins to.

```bash
$ ~/alphanet.sh client list known addresses

simon: tz1cSBeSxJf6dK1U8HtMSvPt2hMSYDtbf4JB (unencrypted sk known)
bob: tz1PYgf9fBGLXvwx8ag8sdwjLJzmyGdNiswM (unencrypted sk known)
alice: tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH (unencrypted sk known)
```

I have three accounts on the Alphanet. I am going to originate a token called 
`Tezos Gold` with a symbol `TGD` and 1,000,000 tokens. I will place all of them 
in Alice's custody. Copy the command below and replace `alice` with the name of your
account and `"tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH"` with your account's 
address. You can change the name, symbol and amount of tokens to whatever number
you like. Make sure the two numbers match or there will be untransferrable 
tokens in the contract.

```bash
$ ~/alphanet.sh client originate contract tokenTezosGold for alice \
                                 transferring 0 from alice \
                                 running container:contracts/token.tz \
                                 --init '(Pair {Elt "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH" (Pair 1000000 {})} (Pair 1000000 (Pair "Tezos Gold" "TGD")))' --burn-cap 2
```

The name `tokenTezosGold` is a unique symbol that will be stored are your 
device. If you want to originate another contract with the same symbol, you
can overwrite it by adding `--force` to the end of the command. Just remember
that it will disassociate the old contract address from the symbol, but that
contract will still exist on the Tezos blockchain.

Now Alice can transfer Tezos Gold tokens to another account. In the command 
below, the first account address is the sender, Alice and the second one is the 
receiver, Simon. The numerical value is the number of Tezos Gold tokens that 
Alice will send to Simon.

```bash
~/alphanet.sh client transfer 0 from alice \
              to tokenTezosGold \ 
              --arg '(Left (Pair "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH" (Pair "tz1cSBeSxJf6dK1U8HtMSvPt2hMSYDtbf4JB" 100)))' --burn-cap 1
```

Finally, you can check the Tezos Gold balance of each account.

```bash
$ ~/alphanet.sh client get big map value \ 
                for '"tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH"' \
                of type 'address' in tokenTezosGold

$ ~/alphanet.sh client get big map value \ 
                for '"tz1cSBeSxJf6dK1U8HtMSvPt2hMSYDtbf4JB"' \
                of type 'address' in tokenTezosGold
```

You can create multiple token contracts if you like. Just copy the first command
and replace the values as you like.

## Originating the Broker Contract

The Dexter exchange contract needs to get data from the token contract to check 
the balance of a particular account in order to calculate the exchange rate and 
make sure there are enough funds in the account. In the [third](https://gitlab.com/camlcase-dev/michelson-tutorial/tree/master/03)
and [fifth](https://gitlab.com/camlcase-dev/michelson-tutorial/tree/master/05) 
Michelson tutorials, we discuss how to use `TRANSFER_TOKENS` to transfer 
`tez` and call other contracts.

In order to get the balance from the token contract into the exchange
contract on-chain, we use a contract callback technique where contract A calls
contract B, and then contract B calls contract A with the data A wants from
B. 

However, there is challenge. When using `TRANSFER_TOKENS`, you must match
exactly the parameter type signature of the contract you are calling. In the 
case where we write both contract A and B, this is not a problem. We are in 
control of the type signature of contract A and B, but we are not in control of
the token standard. The token standard will not know our contract's parameter 
type signature.

This forces us to use another work around, write a broker contract that 
matches the parameter type signature that the token contract uses when it calls 
`TRANSFER_TOKENS` to pass data. Then the broker contracts will call
the exchange contract with the data from the token contract and to the correct
entrance path. This means that the type signature that the token standard 
assumes when it calls `TRANSFER_TOKENS` must be written in a generic way.

Now we will originate the broker contract to allow the Dexter exchange to
retrieve an account's token balance from the token contract. Remeber to replace
`alice` with the name of your account.

```bash
~/alphanet.sh client originate contract broker for alice \
                               transferring 0 from alice \
                               running container:contracts/broker.tz \
                               --init '(Pair {} Unit)' --burn-cap 2
```

## Originate the Dexter Exchange Contract

When we originate the Dexter exchange contract, we need to include the 
addresses of the contracts we just originated. We can look up the addresses
in the command line.

```bash
$ ~/alphanet.sh client list known contracts

broker:    KT1MkNg2N6XqZmbcE5kq4iVKhqvvdN84LnH1
tezosGold: KT1WQAW1sRaykMPYEPpqiL4nrYvdnb8SWTV7
```

Now we can originate the exchange contract. Replace `alice` with the name of 
your account, the first address is the broker contract address and the second
address is the token contract address.

```
$ ~/alphanet.sh client originate contract tezosGoldExchange for alice \
                                 transferring 0 from alice \
                                 running container:contracts/exchange.tz \
                                 --init '(Pair {} (Pair (Pair "KT1MkNg2N6XqZmbcE5kq4iVKhqvvdN84LnH1" "KT1WQAW1sRaykMPYEPpqiL4nrYvdnb8SWTV7") (Pair 0 {})))' --burn-cap 10
```

Let's add some liquidity. In the command below, `transfer 10` means Alice is 
adding 10 tez to the liquidity pool. The next number is the minimum amount of 
liquidity you want to mint (if that number is not reached it will fail and 
return the tez to you). The liquidity is an internal number, referred to as DEX 
in the contract, that the exchange tracks. Then the next number is the maximum 
number of Tezos Gold tokens you want to add to the liquidity pool. When the 
liquidity pool is empty it will add the maximum amount if the account has that 
many tokens. And finally the date is the deadline for which you would like the 
transaction to occur by. 

```bash
$ ~/alphanet.sh client transfer 10 from alice to tezosGoldExchange \ 
                --arg 'Left (Left (Pair 1 (Pair 100 "2020-06-29T18:00:21Z")))' --burn-cap 1
```

In this transaction, Alice has initiated the liquidity pool with 10 tez and 100 
Tezos Gold tokens meaning that she has valued Tezos Gold at 10-1.

With another account you can try buying some tokens from the exchange. 
`transfer 1` means Bob is willing to spend one tez. The `5` means he
wants to receive at least 5 Tezos Gold for his one tez and the date is the 
deadline for the transaction to occur. If the deadline has passed or Bob's
minimum request has not been met, the transaction will fail.

```bash
$ ~/alphanet.sh client transfer 1 from bob to tezosGoldExchange \ 
                --arg 'Right (Left (Pair 5 "2020-06-29T18:00:21Z"))' --burn-cap 1
```

We can also purchase tez with tokens. Make sure `tranfer` is set to `0` or you
are giving free tez to the exchange. The first number after `transfer 0` is the 
number of tokens you want to sell, then the minimum amount of mutez you want to 
receive (one tez is 1,000,000 mutez), and finally the deadline by which the 
transfer should occur.

```bash
$ ~/alphanet.sh client transfer 0 from bob to tezosGoldExchange \
                --arg 'Right (Right (Left (Pair 10 (Pair 1 "2020-06-29T18:00:21Z"))))' --burn-cap 1
```

The last thing we can do is remove liquidity from the exchange. 
The first number is the amount of DEX (the internal exchange token for keeping
track of liquidity) you want to get rid of, the second number is the minimum 
amount of mutez you want to withdraw (one tez is 1,000,000 mutez) and the third 
number is the minimum amount of tokens you with to withdraw. If the deadline
is passed of if any of the minimums are not met, then the transaction will fail.

```bash
$ ~/alphanet.sh client transfer 0 from alice to tezosGoldExchange \ 
                --arg 'Left (Right (Pair (Pair 100 1) (Pair 1 "2020-06-29T18:00:21Z")))' --burn-cap 1
```

Finally, you can make multiple tokens and multiple exchanges. Here is a
quick example for reference. Notice that the addresses for the broker
contract is the same. It is reusable. The last address is the token 
contract address.

```bash
$ ~/alphanet.sh client originate contract tezosSilver for alice \
                                transferring 0 from alice \
                                running container:contracts/token.tz \
                                --init '(Pair {Elt "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH" (Pair 1000000 {})} (Pair 1000000 (Pair "Tezos Silver" "TSV")))' --burn-cap 2

$ ~/alphanet.sh client list known contracts

tezosSilver: KT1R4ga92SLKCwrJEcE6KteU5BCFAH7gnTnP
                                
$ ~/alphanet.sh client originate contract tezosSilverExchange for alice \
                                transferring 0 from alice \
                                running container:contracts/exchange.tz \
                                --init '(Pair {} (Pair (Pair "KT1MkNg2N6XqZmbcE5kq4iVKhqvvdN84LnH1" "KT1R4ga92SLKCwrJEcE6KteU5BCFAH7gnTnP") (Pair 0 {})))' --burn-cap 10

```

# Appendix A: Sample Token Contract Commands

## Originate a Sample Token Contract

Reference:

```bash
$ ~/alphanet.sh client originate contract <token-contract-name> for <contract-owner> \
                                 transferring 0 from <contract-owner> \
                                 running container:contracts/token.tz \
                                 --init '(Pair {Elt "<contract-owner-address>" (Pair <token-total-amount> {})} (Pair <token-total-amount> (Pair "<token-name>" "<token-symbol>")))' --burn-cap 2
```

Example:

```bash
$ ~/alphanet.sh client originate contract tokenTezosGold for alice \
                                 transferring 0 from alice \
                                 running container:contracts/token.tz \
                                 --init '(Pair {Elt "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH" (Pair 1000000 {})} (Pair 1000000 (Pair "Tezos Gold" "TGD")))' --burn-cap 2
```

## Transfer Tokens

Reference:

```bash
$ ~/alphanet.sh client transfer 0 from <token-sender> to <token-contract-name> --arg '(Left (Pair "<token-sender-address>" (Pair "<token-receiver-address>" <number of tokens>)))' --burn-cap 1
```

Example:

```bash
$ ~/alphanet.sh client transfer 0 from alice to tezosGold \
                --arg '(Left (Pair "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH" (Pair "tz1cSBeSxJf6dK1U8HtMSvPt2hMSYDtbf4JB" 100)))' \ 
                --burn-cap 1
```

## Check Account Balance

Reference:

```bash
$ ~/alphanet.sh client get big map value for '"<account-address>"' \
                       of type 'address' in <token-contract-name>
```

Example:

```bash
$ ~/alphanet.sh client get big map value \
                for '"tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH"' \ 
                of type 'address' in tezosGold
```

# Appendix B: Dexter Exchange

## Originate a Dexter Exchange

Reference:
```bash
~/alphanet.sh client originate contract <exchange-contract-name> for <contract-owner> \
                                transferring 0 from <contract-owner> \
                                running container:contracts/exchange.tz \
                                --init '(Pair {} (Pair (Pair "<broker-contract-address>" "<token-contract-address>") (Pair 0 {})))' \ 
                                --burn-cap 10
```

Example:
```bash
~/alphanet.sh client originate contract tezosGoldExchange for alice \
                               transferring 0 from alice \
                               running container:contracts/exchange.tz \
                               --init '(Pair {} (Pair (Pair "KT1MkNg2N6XqZmbcE5kq4iVKhqvvdN84LnH1" "KT1WQAW1sRaykMPYEPpqiL4nrYvdnb8SWTV7") (Pair 0 {})))' \ 
                               --burn-cap 10
```

## Add Liquidity

Reference:
```bash
$ ~/alphanet.sh client transfer <tez> from <liquidity-minter> \ 
                       to <exchange-contract-name> \
                       --arg 'Left (Left (Pair <min-liquidity-minted> (Pair <max-tokens-deposited> "<deadline>")))' \ 
                       --burn-cap 1
```

Example:
```bash
$ ~/alphanet.sh client transfer 10 from alice to tezosGoldExchange --arg 'Left (Left (Pair 1 (Pair 100 "2020-06-29T18:00:21Z")))' --burn-cap 1
```

## Remove Liquidity

Reference:
```bash
$ ~/alphanet.sh client transfer <tez> from <liquidity-remover> to <exchange-contract-name> --arg 'Left (Right (Pair (Pair <liquidity-burned> <min-mutez-withdrawn>) (Pair <min-token-withdrawn> "<deadline>")))' --burn-cap 1
```

Example:
```bash
$ ~/alphanet.sh client transfer 0 from alice to tezosGoldExchange \ 
                       --arg 'Left (Right (Pair (Pair 100 1) (Pair 1 "2020-06-29T18:00:21Z")))' \
                       --burn-cap 1
```

## Trade Tez to Token

Reference:
```bash
$ ~/alphanet.sh client transfer <tez> from <buyer> to <exchange-contract-name> \
                       --arg 'Right (Left (Pair <min-tokens-required> "<deadline>"))' \
                       --burn-cap 1
```

Example:
```bash
$ ~/alphanet.sh client transfer 1 from bob to tezosGoldExchange \ 
                       --arg 'Right (Left (Pair 1 "2020-06-29T18:00:21Z"))' \
                       --burn-cap 1
```

## Trade Token to Tez

Reference:
```bash
$ ~/alphanet.sh client transfer 0 from <buyer> to <exchange-contract-name> \
                       --arg 'Right (Right (Left (Pair <tokens-sold> (Pair <min-tez-required> "<deadline>"))))' \
                       --burn-cap 1
```

Example:
```bash
$ ~/alphanet.sh client transfer 0 from bob to tezosGoldExchange \
                       --arg 'Right (Right (Left (Pair 10 (Pair 1 "2020-06-29T18:00:21Z"))))' \
                       --burn-cap 1
```

## Get Exchange Total Liquidity

Reference:
```bash
$ ~/alphanet.sh client get script storage for <exchange-contract-name>
```

Example:
```bash
$ ~/alphanet.sh client get script storage for tezosGoldExchange
```

## Get Exchange Total Tez

Reference:
```bash
$ ~/alphanet.sh client get balance for <exchange-contract-name>
```

Example:
```bash
$ ~/alphanet.sh client get balance for tezosGoldExchange
```

## Get Exchange Total Tokens

Reference:
```bash
$ ~/alphanet.sh client get big map value for '"<exchange-contract-address>"' \
                       of type 'address' in tezosGold
```

Example:
```bash
$ ~/alphanet.sh client get big map value \
                       for '"KT1G2D45tpJ9f1iGVwQHqvupv2syhvMqeWPe"' \
                       of type 'address' in tezosGold
```
